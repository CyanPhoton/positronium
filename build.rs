use shaderc::{OptimizationLevel, ShaderKind};

const SHADERS_DIR: &str = "res/shaders";

fn main() {
    let mut compiler = shaderc::Compiler::new().unwrap();
    let mut options = shaderc::CompileOptions::new().unwrap();
    options.set_optimization_level(OptimizationLevel::Performance);

    let shaders_dir_abs = std::path::PathBuf::from(SHADERS_DIR).canonicalize().unwrap().to_string_lossy().to_string();
    println!("cargo:rustc-env=SHADERS_DIR={}", shaders_dir_abs);

    if let Ok(shaders_dir) = std::fs::read_dir(SHADERS_DIR) {
        for shader_dir in shaders_dir
            .filter_map(|d| d.ok())
            .filter(|d| d.file_type().map_or(false, |t| t.is_dir())) {


            if let Ok(shaders) = std::fs::read_dir(shader_dir.path()) {
                for shader in shaders
                    .filter_map(|f| f.ok())
                    .filter(|d| d.file_type().map_or(false, |t| t.is_file())) {

                    let shader_kind = match shader.path().extension().map(|e| e.to_str()).flatten() {
                        None => None,
                        Some("frag") => Some((ShaderKind::Fragment, "frag")),
                        Some("vert") => Some((ShaderKind::Vertex, "vert")),
                        _ => None
                    };

                    if let Some((shader_kind, e)) = shader_kind {

                        let src = std::fs::read_to_string(shader.path()).unwrap();
                        let name = shader.path().to_string_lossy().to_string();
                        println!("cargo:rerun-if-changed={}", name);

                        let output = compiler.compile_into_spirv(src.as_str(), shader_kind, name.as_str(), "main", Some(&options))
                            .expect("Failed to compile shader");

                        let output_file = shader.path().with_file_name(e).with_extension("out");
                        println!("cargo:rerun-if-changed={}", output_file.to_string_lossy().to_string());

                        let output = "[".to_string() + &output.as_binary().iter().map(|o| o.to_string()).collect::<Vec<_>>().join(", ") + "]";

                        std::fs::write(output_file, output).unwrap();
                    }
                }
            }
        }
    }
}