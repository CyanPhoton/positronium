#![windows_subsystem = "windows"]

use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;
use rand::distributions::Distribution;
use positronium::{Application, ApplicationSettings, Positronium, renderer::Renderer, TickInfo, window_manager::WindowManager};
use positronium::input_manager::{ButtonBinding, ButtonBindingMode, CharStreamID, InputManager, ModifiersMode};
use positronium::renderer::{BasicShader, RenderTarget, RenderTargetSettings, SwapchainSettings};
use positronium::window_manager::WindowRef;
use positronium::winit::event::VirtualKeyCode;
use positronium_maths as psm;
use positronium_maths::Vector2f;
use psm::vector;
use winit::event::ModifiersState;
use positronium::renderer::resources::{IndexData, ModelData};

pub struct Sandbox {
    main_window: WindowRef,
    second_window: Option<WindowRef>,
    char_stream: (positronium::flume::Receiver<Vec<char>>, CharStreamID),
    tick: bool,
}

pub struct SandboxData {
    main_render_target: RenderTarget,
    second_render_target: Option<RenderTarget>,
}

impl Application for Sandbox {
    type Data = SandboxData;

    fn new(window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>) -> (Self, ApplicationSettings) {
        let main_window = window_manager.create_window("Test 1", (800, 400));
        // let second_window = Some(window_manager.create_window("Test 2", (600, 300)));
        let char_stream = input_manager.register_input_char_stream(&main_window);

        let test_binding = ButtonBinding {
            mode: ButtonBindingMode::VirtualKey(VirtualKeyCode::Escape),
            modifiers: ModifiersMode::Any
        };

        // input_manager.register_on_button_pressed(second_window.as_ref().unwrap(), test_binding, |app, info| {
        //     if !info.is_synthetic && !info.is_repeat {
        //         app.second_window.take().map(|w| w.close());
        //     }
        // });

        input_manager.register_on_button_pressed(&main_window, test_binding, |app, info| {
            if !info.is_synthetic && !info.is_repeat {
                info.window_manager.request_close(&app.main_window);
            }
        });

        let sandbox = Sandbox {
            main_window,
            // second_window,
            second_window: None,
            char_stream,
            tick: true
        };

        (sandbox, ApplicationSettings::default())
    }

    fn setup(&mut self, window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>, renderer: &mut Renderer) -> SandboxData {
        let settings = RenderTargetSettings {
            swapchain_settings: SwapchainSettings {
                max_frames_in_flight: 0,
                requested_additional_images: 0
            }
        };

        let mut main_render_target = renderer.create_render_target(window_manager, &self.main_window, settings);
        // let mut second_render_target = self.second_window.as_ref().map(|second_window| renderer.create_render_target(window_manager, second_window, settings));

        let rect = gen_rect(vector![-0.5, -0.5], vector![1.0, 1.0]);
        let rect = rect.load_to_device(&mut main_render_target);

        let mut main_scene = main_render_target.create_scene_data();
        main_scene.add_model::<BasicShader>(rect);
        main_render_target.set_scene_data(main_scene);

        // let second_scene = positronium::renderer::SceneData {
        //     hello_world: true,
        //     resource_ref: Default::default(),
        //     basic_meshes: Arc::new(vec![])
        // };
        // second_render_target.as_mut().unwrap().set_scene_data(window_manager, self.second_window.as_ref().unwrap(), second_scene, false);

        SandboxData {
            main_render_target,
            // second_render_target
            second_render_target: None
        }
    }

    fn tick(&mut self, data: &mut SandboxData, window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>, renderer: &mut Renderer, tick_info: &TickInfo) -> bool {
        sleep(Duration::from_millis(10));
        // if self.tick {
        //     println!("Tick");
        // } else {
        //     println!("Toc");
        // }
        // self.tick = !self.tick;

        let s= self.char_stream.0.try_iter().map(|c| c.into_iter()).flatten().collect::<String>();
        if !s.is_empty() {
            dbg!(s);
        }

        let was_r_pressed = input_manager.was_button_pressed_count(&self.main_window, ButtonBinding {
            mode: ButtonBindingMode::VirtualKey(VirtualKeyCode::R),
            modifiers: ModifiersMode::Any
        }, false, false) > 0;

        let is_r_shift_held = input_manager.is_button_pressed(&self.main_window, ButtonBinding {
            mode: ButtonBindingMode::VirtualKey(VirtualKeyCode::R),
            modifiers: ModifiersMode::Exactly(ModifiersState::SHIFT)
        });

        if was_r_pressed || is_r_shift_held {

            let pos_range = rand::distributions::Uniform::new(-0.9, 0.8);
            let size_range = rand::distributions::Uniform::new(0.05, 0.1);

            let mut rng = rand::thread_rng();

            let rect = gen_rect(vector![pos_range.sample(&mut rng), pos_range.sample(&mut rng)], vector![size_range.sample(&mut rng), size_range.sample(&mut rng)]);
            let rect = rect.load_to_device(&mut data.main_render_target);

            data.main_render_target.update_scene_data(|mut scene_data| {
                scene_data.add_model::<BasicShader>(rect);
                // println!("mesh count {}", scene_data.basic_meshes.len());
                scene_data
            })

            // let mut main_scene = data.main_render_target.get_scene_data();
            // main_scene.basic_meshes.push(rect);
            //
            // data.main_render_target.set_scene_data(window_manager, &self.main_window, main_scene, input_manager.get_current_modifiers().shift());
        }

        // if input_manager.was_button_pressed_count(&self.main_window, ButtonBinding {
        //     mode: ButtonBindingMode::VirtualKey(VirtualKeyCode::A),
        //     modifiers: ModifiersMode::None
        // }, false, false) > 0 {
        //     // data.main_render_target.get_scene_mut().add_basic_mesh(gen_rect(vector![-0.5, -0.5], vector![1.0, 1.0]));
        // }

        data.main_render_target.render(window_manager, &self.main_window, None);
        data.second_render_target.as_mut().zip(self.second_window.as_ref()).map(|(r, w)| r.render(window_manager, w, None));

        if let Some(second_window) = self.second_window.take() {
            if window_manager.is_close_requested(&second_window) {
                second_window.close();
            } else {
                self.second_window = Some(second_window);
            }
        }

        !window_manager.is_close_requested(&self.main_window)
    }

    fn cleanup(self, data: SandboxData) {

    }
}

fn gen_rect(top_left: Vector2f, size: Vector2f) -> ModelData<positronium::renderer::BasicVertex> {
    let verts = vec![
        positronium::renderer::BasicVertex { pos: vector![top_left.x, top_left.y], colour: vector![1.0, 0.0, 0.0] }, // TL
        positronium::renderer::BasicVertex { pos: vector![top_left.x, top_left.y + size.y], colour: vector![0.0, 0.0, 1.0] }, // BL
        positronium::renderer::BasicVertex { pos: vector![top_left.x + size.x, top_left.y + size.y], colour: vector![0.0, 1.0, 0.0] }, // BR
        positronium::renderer::BasicVertex { pos: vector![top_left.x + size.x, top_left.y], colour: vector![1.0, 1.0, 1.0] }, // TR
    ];

    let indices = IndexData::U16(vec![0, 1, 2, 2, 3, 0]);

    ModelData::new(verts, indices)
}

fn main() {
    Positronium::run::<Sandbox>()
}