use std::cmp::Ordering;
use std::collections::Bound;
use std::ops::{RangeBounds, Sub};

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum LowerBound<T> where T: Ord {
    Unbounded,
    Inclusive(T),
    Exclusive(T),
}

impl<T: Ord> PartialOrd for LowerBound<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: Ord> Ord for LowerBound<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (LowerBound::Unbounded, LowerBound::Unbounded) => Ordering::Equal,
            (LowerBound::Unbounded, LowerBound::Inclusive(_)) => Ordering::Less, (LowerBound::Inclusive(_), LowerBound::Unbounded) => Ordering::Greater,
            (LowerBound::Unbounded, LowerBound::Exclusive(_)) => Ordering::Less, (LowerBound::Exclusive(_), LowerBound::Unbounded) => Ordering::Greater,
            (LowerBound::Inclusive(l), LowerBound::Inclusive(r)) => l.cmp(r),
            (LowerBound::Exclusive(l), LowerBound::Exclusive(r)) => l.cmp(r),
            (LowerBound::Inclusive(l), LowerBound::Exclusive(r)) => if l <= r { Ordering::Less } else { Ordering::Greater },
            (LowerBound::Exclusive(l), LowerBound::Inclusive(r)) => if l < r { Ordering::Less } else { Ordering::Greater },
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum UpperBound<T> where T: Ord {
    Unbounded,
    Inclusive(T),
    Exclusive(T),
}

impl<T: Ord> PartialOrd for UpperBound<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: Ord> Ord for UpperBound<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (UpperBound::Unbounded, UpperBound::Unbounded) => Ordering::Equal,
            (UpperBound::Unbounded, UpperBound::Inclusive(_)) => Ordering::Greater, (UpperBound::Inclusive(_), UpperBound::Unbounded) => Ordering::Less,
            (UpperBound::Unbounded, UpperBound::Exclusive(_)) => Ordering::Greater, (UpperBound::Exclusive(_), UpperBound::Unbounded) => Ordering::Less,
            (UpperBound::Inclusive(l), UpperBound::Inclusive(r)) => l.cmp(r),
            (UpperBound::Exclusive(l), UpperBound::Exclusive(r)) => l.cmp(r),
            (UpperBound::Inclusive(l), UpperBound::Exclusive(r)) => if l < r { Ordering::Less } else { Ordering::Greater },
            (UpperBound::Exclusive(l), UpperBound::Inclusive(r)) => if l <= r { Ordering::Less } else { Ordering::Greater },
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Range<T> where T: Ord {
    pub lower: LowerBound<T>,
    pub upper: UpperBound<T>,
}

impl<T: Ord> PartialEq<UpperBound<T>> for LowerBound<T> {
    fn eq(&self, other: &UpperBound<T>) -> bool {
        match (self, other) {
            (LowerBound::Unbounded, UpperBound::Unbounded) => false,
            (LowerBound::Unbounded, UpperBound::Inclusive(_)) => false,
            (LowerBound::Unbounded, UpperBound::Exclusive(_)) => false,
            (LowerBound::Inclusive(_), UpperBound::Unbounded) => false,
            (LowerBound::Exclusive(_), UpperBound::Unbounded) => false,
            (LowerBound::Exclusive(_), UpperBound::Inclusive(_)) => false,
            (LowerBound::Inclusive(_), UpperBound::Exclusive(_)) => false,
            (LowerBound::Inclusive(l), UpperBound::Inclusive(r)) => l == r,
            (LowerBound::Exclusive(l), UpperBound::Exclusive(r)) => l == r,
        }
    }
}

impl<T: Ord> PartialOrd<UpperBound<T>> for LowerBound<T> {
    fn partial_cmp(&self, other: &UpperBound<T>) -> Option<Ordering> {
        Some(match (self, other) {
            (LowerBound::Unbounded, UpperBound::Unbounded) => Ordering::Less,
            (LowerBound::Unbounded, UpperBound::Inclusive(_)) => Ordering::Less,
            (LowerBound::Unbounded, UpperBound::Exclusive(_)) => Ordering::Less,
            (LowerBound::Inclusive(_), UpperBound::Unbounded) => Ordering::Less,
            (LowerBound::Exclusive(_), UpperBound::Unbounded) => Ordering::Less,
            (LowerBound::Inclusive(l), UpperBound::Inclusive(r)) => l.cmp(r),
            (LowerBound::Exclusive(l), UpperBound::Exclusive(r)) => l.cmp(r),
            (LowerBound::Inclusive(l), UpperBound::Exclusive(r)) => if l < r { Ordering::Less } else { Ordering::Greater },
            (LowerBound::Exclusive(l), UpperBound::Inclusive(r)) => if l < r { Ordering::Less } else { Ordering::Greater },
        })
    }
}

impl<T: Ord> PartialEq<LowerBound<T>> for UpperBound<T> {
    fn eq(&self, other: &LowerBound<T>) -> bool {
        other.eq(self)
    }
}

impl<T: Ord> PartialOrd<LowerBound<T>> for UpperBound<T> {
    fn partial_cmp(&self, other: &LowerBound<T>) -> Option<Ordering> {
        other.partial_cmp(self).map(|o| o.reverse())
    }
}

impl<T: Ord> LowerBound<T> {
    pub fn map<V: Ord, F: FnOnce(T) -> V>(self, map: F) -> LowerBound<V> {
        match self {
            LowerBound::Unbounded => LowerBound::Unbounded,
            LowerBound::Inclusive(v) => LowerBound::Inclusive(map(v)),
            LowerBound::Exclusive(v) => LowerBound::Exclusive(map(v)),
        }
    }

    pub fn flip(self) -> UpperBound<T> {
        match self {
            LowerBound::Unbounded => UpperBound::Unbounded,
            LowerBound::Inclusive(v) => UpperBound::Inclusive(v),
            LowerBound::Exclusive(v) => UpperBound::Exclusive(v),
        }
    }

    // Returns non if self is Unbounded
    pub fn complement(self) -> Option<UpperBound<T>> {
        match self {
            LowerBound::Unbounded => None,
            LowerBound::Inclusive(v) => Some(UpperBound::Exclusive(v)),
            LowerBound::Exclusive(v) => Some(UpperBound::Inclusive(v)),
        }
    }

    pub fn is_adjacent(&self, other: &UpperBound<T>) -> bool where T: AdjacentValueCheck {
        other.is_adjacent(self)
    }
}

impl<T: Ord> UpperBound<T> {
    pub fn map<V: Ord, F: FnOnce(T) -> V>(self, map: F) -> UpperBound<V> {
        match self {
            UpperBound::Unbounded => UpperBound::Unbounded,
            UpperBound::Inclusive(v) => UpperBound::Inclusive(map(v)),
            UpperBound::Exclusive(v) => UpperBound::Exclusive(map(v)),
        }
    }

    pub fn flip(self) -> LowerBound<T> {
        match self {
            UpperBound::Unbounded => LowerBound::Unbounded,
            UpperBound::Inclusive(v) => LowerBound::Inclusive(v),
            UpperBound::Exclusive(v) => LowerBound::Exclusive(v),
        }
    }

    // Returns non if self is Unbounded
    pub fn complement(self) -> Option<LowerBound<T>> {
        match self {
            UpperBound::Unbounded => None,
            UpperBound::Inclusive(v) => Some(LowerBound::Exclusive(v)),
            UpperBound::Exclusive(v) => Some(LowerBound::Inclusive(v)),
        }
    }

    pub fn is_adjacent(&self, other: &LowerBound<T>) -> bool where T: AdjacentValueCheck {
        match (self, other) {
            (UpperBound::Unbounded, LowerBound::Unbounded) => false,
            (UpperBound::Unbounded, LowerBound::Inclusive(_)) => false,
            (UpperBound::Unbounded, LowerBound::Exclusive(_)) => false,
            (UpperBound::Inclusive(_), LowerBound::Unbounded) => false,
            (UpperBound::Exclusive(_), LowerBound::Unbounded) => false,
            (UpperBound::Inclusive(l), LowerBound::Inclusive(r)) => l.is_adjacent(r),
            (UpperBound::Inclusive(l), LowerBound::Exclusive(r)) => l.eq(r),
            (UpperBound::Exclusive(l), LowerBound::Inclusive(r)) => l.eq(r),
            // [0.0, 1.0) adj (1.0, 2.0] is false
            // [0, 1) adj (1, 2] is false
            // [0, 2) (1, 3] is true
            // [0, 2) (3, 4] is false
            // [0.0, 2.0) (1.999...99, 3] is true
            // [0.0, 2.0) (2.00...01, 3] is false
            // So, they need to be adjacent and l > r
            (UpperBound::Exclusive(l), LowerBound::Exclusive(r)) => l > r && l.is_adjacent(r),
        }
    }
}

impl<T: Ord> Range<T> {
    pub fn new<R: RangeBounds<T>>(range: R) -> Range<T> where T: Clone {
        let lower = match range.start_bound() {
            Bound::Included(v) => LowerBound::Inclusive(v.clone()),
            Bound::Excluded(v) => LowerBound::Exclusive(v.clone()),
            Bound::Unbounded => LowerBound::Unbounded
        };
        let upper = match range.end_bound() {
            Bound::Included(v) => UpperBound::Inclusive(v.clone()),
            Bound::Excluded(v) => UpperBound::Exclusive(v.clone()),
            Bound::Unbounded => UpperBound::Unbounded
        };
        assert!(upper >= lower);

        Range {
            lower,
            upper
        }
    }

    pub fn new_wrap<V, R: RangeBounds<V>, F: Fn(V) -> T>(range: R, wrap: F) -> Range<T> where V: Clone {
        let lower = match range.start_bound() {
            Bound::Included(v) => LowerBound::Inclusive(wrap(v.clone())),
            Bound::Excluded(v) => LowerBound::Exclusive(wrap(v.clone())),
            Bound::Unbounded => LowerBound::Unbounded
        };
        let upper = match range.end_bound() {
            Bound::Included(v) => UpperBound::Inclusive(wrap(v.clone())),
            Bound::Excluded(v) => UpperBound::Exclusive(wrap(v.clone())),
            Bound::Unbounded => UpperBound::Unbounded
        };
        assert!(upper >= lower);

        Range {
            lower,
            upper
        }
    }

    pub fn overlap(self, other: Range<T>) -> Option<Range<T>> {
        let lower = self.lower.max(other.lower);
        let upper = self.upper.min(other.upper);
        (upper >= lower).then(|| Range { lower, upper })
    }
}

impl<T: Ord> RangeBounds<T> for Range<T> {
    fn start_bound(&self) -> Bound<&T> {
        match &self.lower {
            LowerBound::Unbounded => Bound::Unbounded,
            LowerBound::Inclusive(v) => Bound::Included(v),
            LowerBound::Exclusive(v) => Bound::Excluded(v),
        }
    }

    fn end_bound(&self) -> Bound<&T> {
        match &self.upper {
            UpperBound::Unbounded => Bound::Unbounded,
            UpperBound::Inclusive(v) => Bound::Included(v),
            UpperBound::Exclusive(v) => Bound::Excluded(v),
        }
    }
}

pub trait AdjacentValueCheck {
    /// The definition of two values being adjacent is:
    ///
    /// for all x, y, such that x < y
    /// x and y are adjacent iff,
    /// there does not exist a z, such that x < z < y
    ///
    /// This must be symmetric, so if adj(x, y) then adj(y, x)
    /// This must not be reflexive, so adj(x, x) is always false
    /// This will always be anti-transitivity, so if adj(x, y) && adj(y, z), then adj(x, z) is always false
    fn is_adjacent(&self, other: &Self) -> bool;
}

macro_rules! int_adj_check {
    ($($int:ty),*) => {
        $(
        impl AdjacentValueCheck for $int {
            fn is_adjacent(&self, other: &Self) -> bool {
                self.max(other) - self.min(other) == 1
            }
        }
        )*
    };
}

int_adj_check!(u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize);

impl AdjacentValueCheck for f32 {
    fn is_adjacent(&self, other: &Self) -> bool {
        // See f32::total_cmp from std to see why this works
        let mut left = self.to_bits() as i32;
        let mut right = other.to_bits() as i32;
        left ^= (((left >> 31) as u32) >> 1) as i32;
        right ^= (((right >> 31) as u32) >> 1) as i32;

        left.is_adjacent(&right)
    }
}

impl AdjacentValueCheck for f64 {
    fn is_adjacent(&self, other: &Self) -> bool {
        // See f64::total_cmp from std to see why this works
        let mut left = self.to_bits() as i64;
        let mut right = other.to_bits() as i64;
        left ^= (((left >> 63) as u64) >> 1) as i64;
        right ^= (((right >> 63) as u64) >> 1) as i64;

        left.is_adjacent(&right)
    }
}

pub trait BoundedRange {
    fn offset(&self) -> usize;
    fn len(&self) -> usize;
}

impl BoundedRange for std::ops::Range<usize> {
    fn offset(&self) -> usize {
        self.start
    }

    fn len(&self) -> usize {
        self.end - self.start
    }
}

impl BoundedRange for std::ops::RangeInclusive<usize> {
    fn offset(&self) -> usize {
        *self.start()
    }

    fn len(&self) -> usize {
        *self.end() - *self.start() + 1
    }
}