use std::collections::BTreeMap;
use std::ops::{Bound, RangeBounds, RangeInclusive};
use crate::utility::bounds::{AdjacentValueCheck, Range};
use crate::utility::region_map::{OverlapDescription, RegionMap, TotalOrd};

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct RegionSet<T> where T: TotalOrd + AdjacentValueCheck + Copy {
    region_map: RegionMap<T, ()>
}

impl<T: TotalOrd + AdjacentValueCheck + Copy> Default for RegionSet<T> {
    fn default() -> Self {
        RegionSet {
            region_map: RegionMap::default()
        }
    }
}

impl<T: TotalOrd + AdjacentValueCheck + Copy> RegionSet<T> {
    pub fn new() -> RegionSet<T> {
        RegionSet::default()
    }

    pub fn insert<R: RangeBounds<T> + Clone>(&mut self, range: R) {
        self.region_map.insert(range, (), |_, _| ())
    }

    pub fn remove<R: RangeBounds<T>>(&mut self, range: R) {
        self.region_map.remove(range);
    }

    pub fn contains(&self, value: T) -> bool {
        self.region_map.contains(value)
    }

    pub fn get_overlap<'a, R: 'a + RangeBounds<T>>(&'a self, range: R) -> impl Iterator<Item=OverlapDescription<T>> + '_ {
        self.region_map.get_overlap(range).map(move |(r, v)| r)
    }
}