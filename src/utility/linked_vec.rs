use std::iter::FusedIterator;
use std::num::NonZeroUsize;
use std::ops::{Deref, DerefMut, Index, IndexMut};
use crate::utility;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(transparent)]
struct OffsetIndex {
    index: NonZeroUsize
}

impl OffsetIndex {
    pub fn new(i: usize) -> OffsetIndex {
        OffsetIndex {
            index: NonZeroUsize::new(i + 1).unwrap()
        }
    }

    pub fn to_index(self) -> usize {
        self.index.get() - 1
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
enum IndexRef {
    None,
    Index(OffsetIndex)
}

impl IndexRef {
    pub fn to_index(self) -> Option<Id> {
        match self {
            IndexRef::None => None,
            IndexRef::Index(i) => Some(Id(i.to_index())),
        }
    }
}

#[derive(Debug)]
pub struct Element<T: Sized> {
    previous: IndexRef,
    pub element: T,
    next: IndexRef
}

impl<T: Sized> Element<T> {
    pub fn previous(&self) -> Option<Id> {
        self.previous.to_index()
    }

    pub fn next(&self) -> Option<Id> {
        self.next.to_index()
    }
}

impl<T: Sized> Deref for Element<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.element
    }
}

impl<T: Sized> DerefMut for Element<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.element
    }
}

#[derive(Debug)]
enum ElementType<T: Sized> {
    Used(Element<T>),
    Unused { next_unused: IndexRef }
}

impl<T: Sized> ElementType<T> {
    pub fn assume_used(&self) -> &Element<T> {
        match self {
            ElementType::Used(e) => e,
            ElementType::Unused { .. } => panic!()
        }
    }

    pub fn assume_used_mut(&mut self) -> &mut Element<T> {
        match self {
            ElementType::Used(e) => e,
            ElementType::Unused { .. } => panic!()
        }
    }

    pub fn into_used(self) -> Element<T> {
        match self {
            ElementType::Used(e) => e,
            ElementType::Unused { .. } => panic!()
        }
    }
}

#[derive(Debug)]
pub struct LinkedVec<T: Sized> {
    data: Vec<ElementType<T>>,
    first: IndexRef,
    last: IndexRef,
    next_unused: IndexRef,
    unused_count: usize,
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash)]
pub struct Id(pub usize);

impl<T: Sized> LinkedVec<T>  {
    pub fn new() -> LinkedVec<T> {
        LinkedVec {
            data: vec![],
            first: IndexRef::None,
            last: IndexRef::None,
            next_unused: IndexRef::None,
            unused_count: 0
        }
    }

    pub fn with_capacity(capacity: usize) -> LinkedVec<T> {
        LinkedVec {
            data: Vec::with_capacity(capacity),
            first: IndexRef::None,
            last: IndexRef::None,
            next_unused: IndexRef::None,
            unused_count: 0
        }
    }

    /// Adds the element to the end of the LinkedVec, returning it's index
    /// O(1)
    pub fn push_back(&mut self, element: T) -> Id {
        if let IndexRef::Index(unused) = self.next_unused {
            let unused = unused.to_index();
            // If there is an unused chain, get the start of it

            // Redirect start of unused chain to next
            if let ElementType::Unused { next_unused } = self.data[unused] {
                self.next_unused = next_unused;
                self.unused_count -= 1;
            } else {
                unreachable!()
            }

            self.data[unused] = ElementType::Used(Element {
                // Set the previous to the old last
                previous: self.last,
                element,
                // Set the unused element to be the last
                next: IndexRef::None
            });

            // Update last
            if let Some(last) = self.last.to_index() {
                self.data[last.0].assume_used_mut().next = IndexRef::Index(OffsetIndex::new(unused));
            }

            self.last = IndexRef::Index(OffsetIndex::new(unused));

            Id(unused)
        } else {
            // Else need to acquire new element
            match self.last {
                IndexRef::None => {
                    // If currently empty
                    self.data.push(ElementType::Used(Element {
                        previous: IndexRef::None,
                        element,
                        next: IndexRef::None
                    }));
                    self.first = IndexRef::Index(OffsetIndex::new(0));
                    self.last = IndexRef::Index(OffsetIndex::new(0));
                }
                IndexRef::Index(_) => {
                    // If there is an existing chain
                    self.data.push(ElementType::Used(Element {
                        previous: self.last,
                        element,
                        next: IndexRef::None
                    }));
                    let new_index = IndexRef::Index(OffsetIndex::new(self.data.len() - 1));
                    self.data[self.last.to_index().unwrap().0].assume_used_mut().next = new_index;
                    self.last = new_index;
                }
            }
            // Always on end
            Id(self.data.len() - 1)
        }
    }

    /// Adds the element to the start of the LinkedVec, returning it's index
    /// O(1)
    pub fn push_front(&mut self, element: T) -> Id {
        if let IndexRef::Index(unused) = self.next_unused {
            let unused = unused.to_index();
            // If there is an unused chain, get the start of it

            // Redirect start of unused chain to next
            if let ElementType::Unused { next_unused } = self.data[unused] {
                self.next_unused = next_unused;
                self.unused_count -= 1;
            } else {
                unreachable!()
            }

            self.data[unused] = ElementType::Used(Element {
                // Set the unused element to be the first
                previous: IndexRef::None,
                element,
                // Set the next to the old first
                next: self.first
            });

            // Update first
            if let Some(first) = self.first.to_index() {
                self.data[first.0].assume_used_mut().previous = IndexRef::Index(OffsetIndex::new(unused));
            }

            self.first = IndexRef::Index(OffsetIndex::new(unused));

            Id(unused)
        } else {
            // Else need to acquire new element
            match self.first {
                IndexRef::None => {
                    // If currently empty
                    self.data.push(ElementType::Used(Element {
                        previous: IndexRef::None,
                        element,
                        next: IndexRef::None
                    }));
                    self.first = IndexRef::Index(OffsetIndex::new(0));
                    self.last = IndexRef::Index(OffsetIndex::new(0));
                }
                IndexRef::Index(_) => {
                    // If there is an existing chain
                    self.data.push(ElementType::Used(Element {
                        previous: IndexRef::None,
                        element,
                        next: self.first
                    }));
                    let new_index = IndexRef::Index(OffsetIndex::new(self.data.len() - 1));
                    self.data[self.first.to_index().unwrap().0].assume_used_mut().previous = new_index;
                    self.first = new_index;
                }
            }
            // Always on end
            Id(self.data.len() - 1)
        }
    }

    /// Inserts an element **after** element with index, returning the new index
    /// O(1)
    pub fn insert(&mut self, index: Id, element: T) -> Id {
        let next = self.data[index.0].assume_used().next;

        if let IndexRef::Index(unused) = self.next_unused {
            let unused = unused.to_index();
            // If there is an unused chain, get the start of it

            // Redirect start of unused chain to next
            if let ElementType::Unused { next_unused } = self.data[unused] {
                self.next_unused = next_unused;
                self.unused_count -= 1;
            } else {
                unreachable!()
            }

            self.data[unused] = ElementType::Used(Element {
                // Set the unused element to be the first
                previous: IndexRef::Index(OffsetIndex::new(index.0)),
                element,
                // Set the next to the old first
                next
            });

            self.data[index.0].assume_used_mut().next = IndexRef::Index(OffsetIndex::new(unused));

            if let IndexRef::Index(next) = next {
                self.data[next.to_index()].assume_used_mut().previous = IndexRef::Index(OffsetIndex::new(unused));
            }

            Id(unused)
        } else {
            self.data.push(ElementType::Used(Element {
                previous: IndexRef::Index(OffsetIndex::new(index.0)),
                element,
                next
            }));

            self.data[index.0].assume_used_mut().next = IndexRef::Index(OffsetIndex::new(self.data.len() - 1));

            if let IndexRef::Index(next) = next {
                self.data[next.to_index()].assume_used_mut().previous = IndexRef::Index(OffsetIndex::new(self.data.len() - 1));
            }

            Id(self.data.len() - 1)
        }
    }

    /// Removes the element from the LinkedVec with the given index
    /// O(1)
    pub fn remove(&mut self, index: Id) -> T {
        let prev = self.data[index.0].assume_used().previous;
        let next = self.data[index.0].assume_used().next;

        if let IndexRef::Index(prev) = prev {
            // Point the previous element over the removed element
            self.data[prev.to_index()].assume_used_mut().next = next;
        } else {
            // Since no previous, this was first, so set first to the next one
            self.first = next;
        }

        if let IndexRef::Index(next) = next {
            // Point the next element over the removed element
            self.data[next.to_index()].assume_used_mut().previous = prev;
        } else {
            // Since no next, was last, so set last to the previous one
            self.last = prev;
        }

        utility::take_replace(&mut self.data[index.0], ElementType::into_used, || {
            let unused = ElementType::Unused { next_unused: self.next_unused };
            self.next_unused = IndexRef::Index(OffsetIndex::new(index.0));
            self.unused_count += 1;
            unused
        }).element
    }

    /// Removes the element at the end of the LinkedVec, also returning the index
    /// O(1)
    pub fn pop_back(&mut self) -> Option<(Id, T)> {
        let last = self.last.to_index()?;

        let element = utility::take_replace(&mut self.data[last.0], ElementType::into_used, || {
            let unused = ElementType::Unused { next_unused: self.next_unused };
            self.next_unused = IndexRef::Index(OffsetIndex::new(last.0));
            self.unused_count += 1;
            unused
        });

        if let Some(prev) = element.previous.to_index() {
            self.data[prev.0].assume_used_mut().next = IndexRef::None;
        }
        self.last = element.previous;

        Some((last, element.element))
    }

    /// Peeks the element at the end of the LinkedVec, also returning the index
    /// O(1)
    pub fn peek_back(&self) -> Option<(Id, &T)> {
        let last = self.last.to_index()?;

        Some((last, &self.data[last.0].assume_used().element))
    }

    /// Peeks the element at the end of the LinkedVec, also returning the index
    /// O(1)
    pub fn peek_mut_back(&mut self) -> Option<(Id, &mut T)> {
        let last = self.last.to_index()?;

        Some((last, &mut self.data[last.0].assume_used_mut().element))
    }

    /// Removes the element at the start of the LinkedVec, also returning the index
    /// O(1)
    pub fn pop_front(&mut self) -> Option<(Id, T)> {
        let first = self.first.to_index()?;

        let element = utility::take_replace(&mut self.data[first.0], ElementType::into_used, || {
            let unused = ElementType::Unused { next_unused: self.next_unused };
            self.next_unused = IndexRef::Index(OffsetIndex::new(first.0));
            self.unused_count += 1;
            unused
        });

        if let Some(next) = element.next.to_index() {
            self.data[next.0].assume_used_mut().previous = IndexRef::None;
        }
        self.first = element.next;

        Some((first, element.element))
    }

    /// Peeks the element at the start of the LinkedVec, also returning the index
    /// O(1)
    pub fn peek_front(&self) -> Option<(Id, &T)> {
        let first = self.first.to_index().unwrap();

        Some((first, &self.data[first.0].assume_used().element))
    }

    /// Peeks the element at the start of the LinkedVec, also returning the index
    /// O(1)
    pub fn peek_mut_front(&mut self) -> Option<(Id, &mut T)> {
        let first = self.first.to_index()?;

        Some((first, &mut self.data[first.0].assume_used_mut().element))
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn iter(&self) -> Iter<T> {
        Iter::new(self)
    }

    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut::new(self)
    }
}

impl<T: Sized> Default for LinkedVec<T> {
    fn default() -> Self {
        LinkedVec::new()
    }
}

impl<T: Sized> Index<Id> for LinkedVec<T> {
    type Output = Element<T>;

    fn index(&self, index: Id) -> &Element<T> {
        self.data[index.0].assume_used()
    }
}

impl<T: Sized> IndexMut<Id> for LinkedVec<T> {
    fn index_mut(&mut self, index: Id) -> &mut Element<T> {
        self.data[index.0].assume_used_mut()
    }
}

impl<T: Sized> IntoIterator for LinkedVec<T> {
    type Item = (Id, T);
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter::new(self)
    }
}

impl<'vec, T: Sized> IntoIterator for &'vec LinkedVec<T> {
    type Item = (Id, &'vec T);
    type IntoIter = Iter<'vec, T>;

    fn into_iter(self) -> Self::IntoIter {
        Iter::new(self)
    }
}

impl<'vec, T: Sized> IntoIterator for &'vec mut LinkedVec<T> {
    type Item = (Id, &'vec mut T);
    type IntoIter = IterMut<'vec, T>;

    fn into_iter(self) -> Self::IntoIter {
        IterMut::new(self)
    }
}

pub struct Iter<'vec, T: Sized> {
    next: IndexRef,
    last: IndexRef,
    remaining: usize,
    vec: &'vec LinkedVec<T>
}

impl<'vec, T: Sized> Iter<'vec, T> {
    pub fn new(vec: &LinkedVec<T>) -> Iter<T> {
        Iter {
            next: vec.first,
            last: vec.last,
            remaining: vec.len() - vec.unused_count,
            vec
        }
    }
}

impl<'vec, T> Iterator for Iter<'vec, T> {
    type Item = (Id, &'vec T);

    fn next(&mut self) -> Option<(Id, &'vec T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.next.to_index() {
            None => None,
            Some(next) => {
                self.next = self.vec[next].next;
                self.remaining -= 1;
                Some((next, &self.vec[next].element))
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, Some(self.remaining))
    }
}

impl<'vec, T> DoubleEndedIterator for Iter<'vec, T> {
    fn next_back(&mut self) -> Option<(Id, &'vec T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.last.to_index() {
            None => None,
            Some(last) => {
                self.last = self.vec[last].previous;
                self.remaining -= 1;
                Some((last, &self.vec[last].element))
            }
        }
    }
}

impl<'vec, T> FusedIterator for Iter<'vec, T> {}
impl<'vec, T> ExactSizeIterator for Iter<'vec, T> {}

pub struct IterMut<'vec, T: Sized> {
    next: IndexRef,
    last: IndexRef,
    remaining: usize,
    vec: &'vec mut LinkedVec<T>
}

impl<'vec, T: Sized> IterMut<'vec, T> {
    pub fn new(vec: &mut LinkedVec<T>) -> IterMut<T> {
        IterMut {
            next: vec.first,
            last: vec.last,
            remaining: vec.len() - vec.unused_count,
            vec
        }
    }
}

impl<'vec, T> Iterator for IterMut<'vec, T> {
    type Item = (Id, &'vec mut T);

    fn next(&mut self) -> Option<(Id, &'vec mut T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.next.to_index() {
            None => None,
            Some(next) => {
                self.next = self.vec[next].next;
                self.remaining -= 1;
                let cheat = unsafe { &mut *(&mut self.vec.data[next.0].assume_used_mut().element as *mut _) };
                Some((next, cheat))
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, Some(self.remaining))
    }
}

impl<'vec, T> DoubleEndedIterator for IterMut<'vec, T> {
    fn next_back(&mut self) -> Option<(Id, &'vec mut T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.last.to_index() {
            None => None,
            Some(last) => {
                self.last = self.vec[last].previous;
                self.remaining -= 1;
                let cheat = unsafe { &mut *(&mut self.vec.data[last.0].assume_used_mut().element as *mut _) };
                Some((last, cheat))
            }
        }
    }
}

impl<'vec, T> FusedIterator for IterMut<'vec, T> {}
impl<'vec, T> ExactSizeIterator for IterMut<'vec, T> {}

pub struct IntoIter<T: Sized> {
    next: IndexRef,
    last: IndexRef,
    remaining: usize,
    vec: LinkedVec<T>
}

impl<T: Sized> IntoIter<T> {
    pub fn new(vec: LinkedVec<T>) -> IntoIter<T> {
        IntoIter {
            next: vec.first,
            last: vec.last,
            remaining: vec.len() - vec.unused_count,
            vec
        }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = (Id, T);

    fn next(&mut self) -> Option<(Id, T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.next.to_index() {
            None => None,
            Some(next) => {
                self.next = self.vec[next].next;
                self.remaining -= 1;
                Some((next, utility::take_replace(&mut self.vec.data[next.0], ElementType::into_used, || ElementType::Unused { next_unused: IndexRef::None }).element))
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, Some(self.remaining))
    }
}

impl<T> DoubleEndedIterator for IntoIter<T> {
    fn next_back(&mut self) -> Option<(Id, T)> {
        if self.remaining == 0 {
            return None;
        }

        match self.last.to_index() {
            None => None,
            Some(last) => {
                self.last = self.vec[last].previous;
                self.remaining -= 1;
                Some((last, utility::take_replace(&mut self.vec.data[last.0], ElementType::into_used, || ElementType::Unused { next_unused: IndexRef::None }).element))
            }
        }
    }
}

impl<T> FusedIterator for IntoIter<T> {}
impl<T> ExactSizeIterator for IntoIter<T> {}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;
    use crate::utility::linked_vec::LinkedVec;

    #[test]
    pub fn connected_test() {
        let mut linked_vec = LinkedVec::new();

        let mut inside = HashSet::new();

        let mut next = 1i64;

        let mut available = Vec::new();

        let k = 100;

        for _ in 0..10*k {
            let i = linked_vec.push_back(next);
            available.push(i);
            inside.insert(next);
            next += 1;
        }
        assert_eq!(10*k, linked_vec.iter().count());
        assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());

        for _ in 0..3*k {
            let r = linked_vec.remove(available.remove(rand::random::<usize>() % available.len()));
            inside.remove(&r);
        }
        assert_eq!(10*k-3*k, linked_vec.iter().count());
        assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());

        for j in 0..k {
            let i = linked_vec.push_front(next);
            assert_eq!(10*k-3*k+(j+1), linked_vec.iter().count());
            assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());
            available.push(i);
            inside.insert(next);
            next += 1;
        }
        assert_eq!(10*k-3*k+k, linked_vec.iter().count());
        assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());

        for _ in 0..2*k {
            let r = linked_vec.remove(available.remove(rand::random::<usize>() % available.len()));
            inside.remove(&r);
        }
        assert_eq!(10*k-3*k+k-2*k, linked_vec.iter().count());
        assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());

        for _ in 0..4*k {
            let i = linked_vec.push_back(next);
            available.push(i);
            inside.insert(next);
            next += 1;
        }
        assert_eq!(10*k, linked_vec.iter().count());
        assert_eq!(linked_vec.iter().len(), linked_vec.iter().count());

        let check = linked_vec.iter().map(|i| *i.1).collect::<HashSet<_>>();

        assert_eq!(inside, check);
    }
}