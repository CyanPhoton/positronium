use std::collections::BTreeMap;
use std::iter::FusedIterator;
use crate::utility::bounds::BoundedRange;

#[derive(Clone, Debug, Default)]
pub struct DisjointBufferSet {
    /// offset -> len
    regions: BTreeMap<usize, usize>
}

pub struct Region {
    pub offset: usize,
    pub len: usize,
}

impl DisjointBufferSet {
    pub fn new() -> DisjointBufferSet {
        DisjointBufferSet {
            regions: Default::default()
        }
    }

    pub fn insert_range<R: BoundedRange>(&mut self, range: R) {
        self.insert_offset(range.offset(), range.len());
    }

    pub fn insert_offset(&mut self, offset: usize, len: usize) {
        let overlaps = self.regions.range(..=offset+len).rev().take_while(|(o, l)| **o + **l >= offset).map(|v| (*v.0, *v.1)).collect::<Vec<_>>();
        for o in &overlaps {
            self.regions.remove(&o.0);
        }

        let (min_start, max_end) = overlaps.into_iter().fold((offset, offset + len), |(min, max), (o, l)| (min.min(o), max.max(o + l)));

        self.regions.insert(min_start, max_end - min_start);
    }

    pub fn clear(&mut self) {
        self.regions.clear();
    }

    pub fn iter(&self) -> impl Iterator<Item=Region> + '_  + DoubleEndedIterator + ExactSizeIterator + FusedIterator {
        self.regions.iter().map(|(offset, len)| Region {
            offset: *offset,
            len: *len
        })
    }

    pub fn into_iter(self) -> impl Iterator<Item=Region> + DoubleEndedIterator + ExactSizeIterator + FusedIterator {
        self.regions.into_iter().map(|(offset, len)| Region {
            offset,
            len
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test() {
        let mut set = DisjointBufferSet::new();

        set.insert_range(0..10);
        set.insert_range(30..40);

        assert_eq!(2, set.iter().count());

        set.insert_range(15..20);
        assert_eq!(3, set.iter().count());

        set.insert_range(10..15);
        assert_eq!(2, set.iter().count());

        set.insert_range(21..30);
        assert_eq!(2, set.iter().count());

        set.insert_range(20..21);
        assert_eq!(1, set.iter().count());
    }
}