use std::collections::BTreeMap;
use std::iter::{FusedIterator, once};
use fxhash::FxHashSet;
use crate::utility;
use crate::utility::bounds::BoundedRange;

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct Mapping {
    pub src_offset: usize,
    pub dst_offset: usize,
    pub len: usize,
}

///
/// For Mapping one buffer to another, where src regions are disjoint and dst regions are disjoint
/// But a src and dst region can "overlap"
/// A src region can only map to one destination region
///
#[derive(Default, Debug, Clone)]
pub struct DisjointBufferMap {
    /// Src offset -> (Dst offset, Len)
    forward_map: BTreeMap<usize, (usize, usize)>,
    /// Dst offset -> (Src offset, Len)
    reverse_map: BTreeMap<usize, (usize, usize)>,
}

///
/// For Mapping one buffer to itself, where src and dst regions are all together disjoint
/// A src region can only map to one destination region
///
#[derive(Debug, Clone)]
pub struct DisjointBufferSelfMap {
    /// domain_offset -> (co-domain_offset, len)
    dual_map: BTreeMap<usize, (usize, usize)>,
    /// domain_offset's of forward maps
    forward_maps: FxHashSet<usize>
}

enum MultiMapRegionTypes {
    Dst { src_offset: usize, len: usize },
    Srf { dst_offsets: Vec<usize>, len: usize },
}

//
// For Mapping one buffer to itself, where src and dst regions are all together disjoint
// A src region can map to many destination regions
//
// #[derive(Debug, Clone)]
// pub struct DisjointBufferSelfMultiMap {
//     /// domain_offset -> *
//     dual_map: BTreeMap<usize, MultiMapRegionTypes>,
// }

impl DisjointBufferMap {
    pub fn new() -> DisjointBufferMap {
        DisjointBufferMap::default()
    }

    pub fn map_ranges<R1: BoundedRange, R2: BoundedRange>(&mut self, src_range: R1, dst_range: R2) {
        assert_eq!(src_range.len(), dst_range.len());

        self.map_offsets(src_range.offset(), dst_range.offset(), src_range.len());
    }

    pub fn map_offsets(&mut self, src_offset: usize, dst_offset: usize, len: usize) {
        self::resolve_overlap(&mut self.forward_map, &mut self.reverse_map, src_offset, len);
        self::resolve_overlap(&mut self.reverse_map, &mut self.forward_map, dst_offset, len);

        self.forward_map.insert(src_offset, (dst_offset, len));
        self.reverse_map.insert(dst_offset, (src_offset, len));
    }

    pub fn push_range<R: BoundedRange>(&mut self, dst_range: R) -> usize {
        self.push_offset(dst_range.offset(), dst_range.len())
    }

    pub fn push_offset(&mut self, dst_offset: usize, len: usize) -> usize {
        if let Some(available_src_offset) = self::resolve_overlap(&mut self.reverse_map, &mut self.forward_map, dst_offset, len) {
            return available_src_offset;
        }

        let mut end = self.forward_map.range(..).rev().next().map(|(src, dst)| (*src, *dst));

        let src_offset = end.map_or(0, |(src_offset, (_, len))| src_offset + len);

        if let Some((last_src_offset, (last_dst_offset, last_len))) = end {
            if last_src_offset + last_len == src_offset && last_dst_offset + last_len == dst_offset {
                // The last section and the new one are contiguous
                self.forward_map.get_mut(&last_src_offset).unwrap().1 = last_len + len;
                self.reverse_map.get_mut(&last_dst_offset).unwrap().1 = last_len + len;

                return src_offset;
            }
        }

        self.forward_map.insert(src_offset, (dst_offset, len));
        self.reverse_map.insert(dst_offset, (src_offset, len));

        src_offset
    }

    // If these is a single mapping from src to dst, where the dst range fully contains dst_range,
    // return the src_offset of the src_range that maps into dst_range
    pub fn get_mapping_containing_dst_range<R: BoundedRange>(&self, dst_range: R) -> Option<usize> {
        let offset = dst_range.offset();
        let len = dst_range.len();

        self.reverse_map.range(..=offset).next_back()
            .map(|(mapping_dst_offset, (mapping_src_offset, mapping_len))| (mapping_dst_offset + mapping_len >= offset + len).then(|| {
                let delta = offset - mapping_dst_offset;
                mapping_src_offset + delta
            })).flatten()
    }

    pub fn clear(&mut self) {
        self.forward_map.clear();
        self.reverse_map.clear();
    }

    pub fn iter(&self) -> impl Iterator<Item=Mapping> + '_  + DoubleEndedIterator + ExactSizeIterator + FusedIterator {
        self.forward_map.iter().map(|(src_offset, (dst_offset, len))| Mapping {
            src_offset: *src_offset,
            dst_offset: *dst_offset,
            len: *len,
        })
    }

    pub fn into_iter(self) -> impl Iterator<Item=Mapping> + DoubleEndedIterator + ExactSizeIterator + FusedIterator {
        self.forward_map.into_iter().map(|(src_offset, (dst_offset, len))| Mapping {
            src_offset,
            dst_offset,
            len
        })
    }
}

impl DisjointBufferSelfMap {
    pub fn new() -> DisjointBufferSelfMap {
        DisjointBufferSelfMap {
            dual_map: Default::default(),
            forward_maps: Default::default()
        }
    }

    /// With override, a map from [0, 1) -> [1, 2) followed by [1, 2) -> [0, 1) will be
    ///             overridden by the second giving [1, 2) -> [0, 1)
    pub fn override_map_ranges<R1: BoundedRange, R2: BoundedRange>(&mut self, src_range: R1, dst_range: R2) {
        assert_eq!(src_range.len(), dst_range.len());

        self.override_map_offsets(src_range.offset(), dst_range.offset(), src_range.len());
    }

    /// With compose, a map from [0, 1) -> [1, 2) followed by [1, 2) -> [0, 1) will cancel out
    pub fn compose_map_ranges<R1: BoundedRange, R2: BoundedRange>(&mut self, src_range: R1, dst_range: R2) {
        assert_eq!(src_range.len(), dst_range.len());

        self.compose_map_offsets(src_range.offset(), dst_range.offset(), src_range.len());
    }

    /// With override, a map from [0, 1) -> [1, 2) followed by [1, 2) -> [0, 1) will be
    ///             overridden by the second giving [1, 2) -> [0, 1)
    pub fn override_map_offsets(&mut self, src_offset: usize, dst_offset: usize, len: usize) {
        assert!(!utility::is_overlapping(src_offset, dst_offset, len), "Copy src and dst must be disjoint");

        // Remove any mapping into and/or out of the src range
        self::remove_self_overlap(&mut self.dual_map, &mut self.forward_maps, src_offset, len);
        // Remove any mapping into and/or out of the dst range
        self::remove_self_overlap(&mut self.dual_map, &mut self.forward_maps, dst_offset, len);

        // Since overriding then just insert new mappings
        self.dual_map.insert(src_offset, (dst_offset, len));
        self.dual_map.insert(dst_offset, (src_offset, len));

        self.forward_maps.insert(src_offset);
    }

    /// With compose, a map from [0, 1) -> [1, 2) followed by [1, 2) -> [0, 1) will cancel out
    pub fn compose_map_offsets(&mut self, src_offset: usize, dst_offset: usize, len: usize) {
        assert!(!utility::is_overlapping(src_offset, dst_offset, len), "Copy src and dst must be disjoint");

        #[derive(Copy, Clone)]
        struct ComposedMapping {
            src_offset: usize,
            dst_offset: usize,
            len: usize,
            overridden_src_offset: usize
        }

        // Remove any mappings into and/or out of the src range
        let removed = self::remove_self_overlap(&mut self.dual_map, &mut self.forward_maps, src_offset, len);

        // Remove any remaining mappings into and/or out of the dst range
        self::remove_self_overlap(&mut self.dual_map, &mut self.forward_maps, dst_offset, len);

        let removed_composed = removed
            .into_iter()
            .filter(|r| !r.is_forward)
            .map(|r| r.invert())
            .map(|r| {
                let delta = r.co_offset - src_offset; // Offset into [src..src+len) it was mapped
                ComposedMapping {
                    src_offset: r.offset,
                    dst_offset: dst_offset + delta,
                    len: r.len,
                    overridden_src_offset: r.co_offset
                }
            }).collect::<Vec<_>>();
        // Note: removed_composed will be inherently sorted by `overridden_src_offset` due to the ordering of removed

        let over_pairs = {
            let first = once(None).chain(removed_composed.iter().copied().map(Some));
            let second = removed_composed.iter().copied().map(Some).chain(once(None));
            first.zip(second)
        };

        struct Mapping {
            src_offset: usize,
            dst_offset: usize,
            len: usize,
        }

        let mut to_add = Vec::<Mapping>::new();

        for (a, b) in over_pairs {
            let a: Option<ComposedMapping> = a;
            let b: Option<ComposedMapping> = b;

            match (a, b) {
                (None, Some(b)) => {
                    if src_offset < b.overridden_src_offset {
                        to_add.push(Mapping {
                            src_offset,
                            dst_offset,
                            len: b.overridden_src_offset - src_offset
                        });
                    }
                }
                (Some(a), b) => {
                    if a.src_offset != a.dst_offset {
                        to_add.push(Mapping {
                            src_offset: a.src_offset,
                            dst_offset: a.dst_offset,
                            len: a.len
                        });
                    }

                    let a_end = a.overridden_src_offset + a.len;
                    let this_end = b.map(|b| b.overridden_src_offset).unwrap_or(src_offset + len);
                    if a_end < this_end {
                        let delta = a_end - src_offset;

                        to_add.push(Mapping {
                            src_offset: a_end,
                            dst_offset: dst_offset + delta,
                            len: this_end - a_end
                        });
                    }
                }
                (None, None) => {
                    to_add.push(Mapping {
                        src_offset,
                        dst_offset,
                        len
                    });
                }
            }
        }

        for Mapping { src_offset, dst_offset, len } in to_add {
            // Insert forward mapping from old src to new dst
            self.dual_map.insert(src_offset, (dst_offset, len));
            // Insert the reverse mapping
            self.dual_map.insert(dst_offset, (src_offset, len));

            self.forward_maps.insert(src_offset);
        }
    }

    pub fn clear_dst_range<R: BoundedRange>(&mut self, range: R) {
        remove_self_overlap_dir(&mut self.dual_map, &mut self.forward_maps, range.offset(), range.len(), false);
    }

    pub fn clear(&mut self) {
        self.dual_map.clear();
        self.forward_maps.clear();
    }

    pub fn iter(&self) -> impl Iterator<Item=Mapping> + '_  + DoubleEndedIterator + FusedIterator {
        self.dual_map.iter().filter_map(|e| self.forward_maps.contains(e.0).then(|| e)).map(|(src_offset, (dst_offset, len))| Mapping {
            src_offset: *src_offset,
            dst_offset: *dst_offset,
            len: *len,
        })
    }

    pub fn into_iter(self) -> impl Iterator<Item=Mapping> + DoubleEndedIterator + FusedIterator {
        self.dual_map.into_iter().filter_map(move |e| self.forward_maps.contains(&e.0).then(|| e)).map(|(src_offset, (dst_offset, len))| Mapping {
            src_offset,
            dst_offset,
            len
        })
    }
}

fn overlaps(map: &BTreeMap<usize, (usize, usize)>, offset: usize, len: usize) -> impl Iterator<Item=(usize, (usize, usize))> + '_ {
    map.range(..offset + len).rev()
        .map(|(overlap_offset, (overlap_co_offset, overlap_len))| (*overlap_offset, (*overlap_co_offset, *overlap_len)))
        .take_while(move |(overlap_offset, (_, overlap_len))| *overlap_offset + *overlap_len > offset)
}

///
/// Clearing the range [domain_offset..domain_offset+len) from the domain map, and the corresponding ranges in the co-domain.
/// If the range lies inside a single region then it just returns the corresponding co_domain_offset
///
fn resolve_overlap(domain: &mut BTreeMap<usize, (usize, usize)>, co_domain: &mut BTreeMap<usize, (usize, usize)>, domain_offset: usize, len: usize) -> Option<usize> {
    let domain_overlaps = self::overlaps(domain, domain_offset, len).collect::<Vec<_>>();

    if domain_overlaps.len() == 1 {
        // There is only one overlap
        let (overlap_src_offset, (overlap_dst_offset, overlap_len)) = domain_overlaps[0];
        if overlap_src_offset + overlap_len >= domain_offset + len {
            // The overlap ends at or after the range, and thus the range is a subset of it
            return Some(overlap_dst_offset + (domain_offset - overlap_src_offset));
        }
    }

    for (overlap_domain_offset, (overlap_co_domain_offset, overlap_len)) in domain_overlaps.into_iter().rev() {
        domain.remove(&overlap_domain_offset).unwrap();
        co_domain.remove(&overlap_co_domain_offset).unwrap();

        if overlap_domain_offset < domain_offset {
            // Overlap starts before this
            let before_len = domain_offset - overlap_domain_offset;
            domain.insert(overlap_domain_offset, (overlap_co_domain_offset, before_len));
            co_domain.insert(overlap_co_domain_offset, (overlap_domain_offset, before_len));
        }

        if overlap_domain_offset + overlap_len > domain_offset + len {
            // Overlap ends after this
            let after_len = (overlap_domain_offset + overlap_len) - (domain_offset + len);
            let offset = (domain_offset + len) - overlap_domain_offset;
            domain.insert(overlap_domain_offset + offset, (overlap_co_domain_offset + offset, after_len));
            co_domain.insert(overlap_co_domain_offset + offset, (overlap_domain_offset + offset, after_len));
        }
    }

    None
}

struct RemovedMapping {
    offset: usize,
    co_offset: usize,
    len: usize,
    is_forward: bool
}

impl RemovedMapping {
    pub fn invert(self) -> Self {
        RemovedMapping {
            offset: self.co_offset,
            co_offset: self.offset,
            len: self.len,
            is_forward: !self.is_forward
        }
    }
}

///
/// Clearing the range [offset..offset+len) from the dual_domain map, and the mapped range as well, and remove/move the corresponding dir_map entries.
///
/// This then returns all the ranges removed from [offset..offset+len)
///
fn remove_self_overlap(dual_domain: &mut BTreeMap<usize, (usize, usize)>, forward_map: &mut FxHashSet<usize>, offset: usize, len: usize) -> Vec<RemovedMapping> {
    let overlaps = self::overlaps(dual_domain, offset, len).collect::<Vec<_>>();
    let mut result = vec![];

    for (overlap_offset, (overlap_co_offset, overlap_len)) in overlaps.into_iter().rev() {
        dual_domain.remove(&overlap_offset).unwrap();
        dual_domain.remove(&overlap_co_offset).unwrap();
        let dir = forward_map.remove(&overlap_offset);
        if !dir {
            forward_map.remove(&overlap_co_offset);
        }

        if overlap_offset < offset {
            // Overlap starts before this
            let before_len = offset - overlap_offset;

            dual_domain.insert(overlap_offset, (overlap_co_offset, before_len));
            dual_domain.insert(overlap_co_offset, (overlap_offset, before_len));

            forward_map.insert(if dir { overlap_offset } else { overlap_co_offset });
        }

        if overlap_offset + overlap_len > offset + len {
            // Overlap ends after this
            let after_len = (overlap_offset + overlap_len) - (offset + len);
            let offset = (offset + len) - overlap_offset;

            dual_domain.insert(overlap_offset + offset, (overlap_co_offset + offset, after_len));
            dual_domain.insert(overlap_co_offset + offset, (overlap_offset + offset, after_len));

            forward_map.insert(if dir { overlap_offset } else { overlap_co_offset } + offset);
        }

        {
            let max_offset = offset.max(overlap_offset);
            let min_offset = offset.min(overlap_offset);
            let delta = max_offset - min_offset;

            let removed_offset = max_offset;
            let removed_co_offset = overlap_co_offset + delta;

            let end = offset + len;
            let overlap_end = overlap_offset + overlap_len;

            let min_end = end.min(overlap_end);
            let removed_len = min_end - max_offset;

            if removed_len > 0 {
                result.push(RemovedMapping {
                    offset: removed_offset,
                    co_offset: removed_co_offset,
                    len: removed_len,
                    is_forward: dir
                });
            }
        }
    }

    result
}

///
/// Clearing the range [offset..offset+len) from the dual_domain map, for maps that are in the given direction, and the mapped range as well, and remove/move the corresponding dir_map entries.
///
/// This then returns all the ranges removed from [offset..offset+len)
///
fn remove_self_overlap_dir(dual_domain: &mut BTreeMap<usize, (usize, usize)>, forward_map: &mut FxHashSet<usize>, offset: usize, len: usize, forward: bool) -> Vec<RemovedMapping> {
    let overlaps = self::overlaps(dual_domain, offset, len).filter(|(s, _)| forward_map.contains(s) == forward).collect::<Vec<_>>();
    let mut result = vec![];

    for (overlap_offset, (overlap_co_offset, overlap_len)) in overlaps.into_iter().rev() {
        dual_domain.remove(&overlap_offset).unwrap();
        dual_domain.remove(&overlap_co_offset).unwrap();
        let dir = forward_map.remove(&overlap_offset);
        if !dir {
            forward_map.remove(&overlap_co_offset);
        }

        if overlap_offset < offset {
            // Overlap starts before this
            let before_len = offset - overlap_offset;

            dual_domain.insert(overlap_offset, (overlap_co_offset, before_len));
            dual_domain.insert(overlap_co_offset, (overlap_offset, before_len));

            forward_map.insert(if dir { overlap_offset } else { overlap_co_offset });
        }

        if overlap_offset + overlap_len > offset + len {
            // Overlap ends after this
            let after_len = (overlap_offset + overlap_len) - (offset + len);
            let offset = (offset + len) - overlap_offset;

            dual_domain.insert(overlap_offset + offset, (overlap_co_offset + offset, after_len));
            dual_domain.insert(overlap_co_offset + offset, (overlap_offset + offset, after_len));

            forward_map.insert(if dir { overlap_offset } else { overlap_co_offset } + offset);
        }

        {
            let max_offset = offset.max(overlap_offset);
            let min_offset = offset.min(overlap_offset);
            let delta = max_offset - min_offset;

            let removed_offset = max_offset;
            let removed_co_offset = overlap_co_offset + delta;

            let end = offset + len;
            let overlap_end = overlap_offset + overlap_len;

            let min_end = end.min(overlap_end);
            let removed_len = min_end - max_offset;

            if removed_len > 0 {
                result.push(RemovedMapping {
                    offset: removed_offset,
                    co_offset: removed_co_offset,
                    len: removed_len,
                    is_forward: dir
                });
            }
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use crate::utility::disjoint_buffer_map::{DisjointBufferMap, Mapping};

    #[test]
    pub fn push_test() {
        let mut map = DisjointBufferMap::new();

        assert_eq!(map.push_range(10..20), 0);
        assert_eq!(map.push_range(0..10), 10);
        assert_eq!(map.push_range(5..15), 15);

        let mut iter = map.into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 5,
            dst_offset: 15,
            len: 5
        }));
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 10,
            dst_offset: 0,
            len: 15
        }));
        assert_eq!(iter.next(), None);
    }

    #[test]
    pub fn push_fill_test() {
        let mut map = DisjointBufferMap::new();

        assert_eq!(map.push_range(10..20), 0);
        assert_eq!(map.push_range(0..10), 10);
        assert_eq!(map.push_range(5..15), 15);
        assert_eq!(map.push_range(20..25), 25);
        assert_eq!(map.push_range(20..25), 25);
        assert_eq!(map.push_range(5..10), 15);
        assert_eq!(map.push_range(10..15), 20);

        let mut iter = map.into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 5,
            dst_offset: 15,
            len: 5
        }));
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 10,
            dst_offset: 0,
            len: 15
        }));
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 25,
            dst_offset: 20,
            len: 5
        }));
        assert_eq!(iter.next(), None);
    }

    #[test]
    pub fn push_join() {
        let mut map = DisjointBufferMap::new();

        assert_eq!(map.push_range(0..10), 0);
        assert_eq!(map.push_range(10..20), 10);

        let mut iter = map.into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 0,
            dst_offset: 0,
            len: 20
        }));

        assert_eq!(iter.next(), None);
    }

    /*#[test]
    pub fn check() {
        let mut map = DisjointBufferMap::new();

        dbg!(map.push_range(0..10));
        dbg!(map.push_range(10..20));
        dbg!(map.iter().collect::<Vec<_>>());

        map.clear();

        dbg!(map.map_ranges(0..10, 0..10));
        dbg!(map.map_ranges(10..20, 10..20));
        dbg!(map.iter().collect::<Vec<_>>());

    }*/
}

#[cfg(test)]
mod disjoint_self_test {
    use super::*;

    #[test]
    pub fn compose_test() {
        let mut map = DisjointBufferSelfMap::new();

        map.compose_map_ranges(0..10, 10..20);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 0,
            dst_offset: 10,
            len: 10
        }));
        assert_eq!(iter.next(), None);

        map.compose_map_ranges(10..20, 0..10);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), None);

        map.compose_map_ranges(0..10, 10..20);
        map.compose_map_ranges(10..15, 5..10);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 0,
            dst_offset: 5,
            len: 5
        }));
        assert_eq!(iter.next(), None);
    }

    #[test]
    pub fn override_test() {
        let mut map = DisjointBufferSelfMap::new();

        map.override_map_ranges(0..10, 10..20);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 0,
            dst_offset: 10,
            len: 10
        }));
        assert_eq!(iter.next(), None);

        map.override_map_ranges(10..20, 0..10);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 10,
            dst_offset: 00,
            len: 10
        }));
        assert_eq!(iter.next(), None);

        map.clear();

        map.override_map_ranges(0..10, 10..20);
        map.override_map_ranges(10..15, 5..10);

        let mut iter = map.clone().into_iter();
        assert_eq!(iter.next(), Some(Mapping {
            src_offset: 10,
            dst_offset: 5,
            len: 5
        }));
        assert_eq!(iter.next(), None);
    }
}

