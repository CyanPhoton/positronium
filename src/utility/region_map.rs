use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::convert::identity;
use std::fmt::Debug;
use std::ops::{Add, Bound, RangeBounds};
use std::ops::Bound::{Excluded, Unbounded};
use crate::utility::bounds::{AdjacentValueCheck, LowerBound, Range, UpperBound};

type U<K> = UpperBound<TotallyOrdered<K>>;
type L<K> = LowerBound<TotallyOrdered<K>>;

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct RegionMap<K, V> where K: TotalOrd + AdjacentValueCheck + Copy, V: Copy {
    // [k, v.0]
    disjoint_regions: BTreeMap<L<K>, (U<K>, V)>,
    // [v, k]
    reverse_disjoint_regions: BTreeMap<U<K>, L<K>>,
}

impl<K: TotalOrd + AdjacentValueCheck + Copy, V: Copy> Default for RegionMap<K, V> {
    fn default() -> Self {
        RegionMap {
            disjoint_regions: Default::default(),
            reverse_disjoint_regions: Default::default(),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct OverlapDescription<K: TotalOrd + AdjacentValueCheck + Copy> {
    pub range: Range<TotallyOrdered<K>>,
    pub overlap_range: Range<TotallyOrdered<K>>,
}

impl<K: TotalOrd + AdjacentValueCheck + Copy, V: Copy> RegionMap<K, V> {
    pub fn new() -> RegionMap<K, V> {
        RegionMap::default()
    }

    /// The merge function take (old, new) and returns the value set in that region of overlap
    pub fn insert<M: Fn(V, V) -> V, R: RangeBounds<K> + Clone>(&mut self, range: R, value: V, merge: M) where V: PartialEq {
        let overlapping_regions = self.get_overlap(range.clone()).map(|(o, _)| o).collect::<Vec<_>>();
        let range = Range::new_wrap(range, |v| TotallyOrdered(v));

        let mut last_free_lower = Some(range.lower);

        for overlap in overlapping_regions.into_iter().rev() {
            if let Some(last_lower) = last_free_lower {
                if last_lower < overlap.overlap_range.lower {
                    // Always Some() since there is something < it
                    let upper = overlap.overlap_range.lower.complement().unwrap();

                    self.disjoint_regions.insert(last_lower, (upper, value));
                    self.reverse_disjoint_regions.insert(upper, last_lower);
                }
            }
            last_free_lower = overlap.overlap_range.upper.complement();

            let old_region = self.disjoint_regions.get_mut(&overlap.range.lower).unwrap();
            let old_value = old_region.1;

            let mut to_insert_upper = None;
            let mut to_insert_lower = None;
            let mut to_change_upper = None;

            // If the overlapping region extends further to the right than the overlap, shrink its upper bound and add a new region to cover the removed section
            if overlap.range.upper > overlap.overlap_range.upper {
                let new_upper = overlap.overlap_range.upper;
                old_region.0 = new_upper;
                to_change_upper = Some(new_upper);

                // Always Some() since there is something > it
                let new_lower = overlap.overlap_range.upper.complement().unwrap();
                to_insert_upper = Some((new_lower, overlap.range.upper, old_region.1));
            }

            // If the overlapping region extends further to the left than the overlap, shrink its upper to remove the overlap section
            // then insert an overlap section merging values
            // Otherwise just merge
            if overlap.range.lower < overlap.overlap_range.lower {
                // Always Some() since there is something < it
                let new_upper = overlap.overlap_range.lower.complement().unwrap();
                old_region.0 = new_upper;
                to_change_upper = Some(new_upper);

                to_insert_lower = Some((overlap.overlap_range.lower, overlap.overlap_range.upper, merge(old_value, value)));
            } else {
                old_region.1 = merge(old_value, value);
            }

            if let Some(to_change_upper) = to_change_upper {
                let lower = self.reverse_disjoint_regions.remove(&overlap.range.upper).unwrap();
                self.reverse_disjoint_regions.insert(to_change_upper, lower);
            }

            if let Some(to_insert_upper) = to_insert_upper {
                self.disjoint_regions.insert(to_insert_upper.0, (to_insert_upper.1, to_insert_upper.2));
                self.reverse_disjoint_regions.insert(to_insert_upper.1, to_insert_upper.0);
            }

            if let Some(to_insert_lower) = to_insert_lower {
                self.disjoint_regions.insert(to_insert_lower.0, (to_insert_lower.1, to_insert_lower.2));
                self.reverse_disjoint_regions.insert(to_insert_lower.1, to_insert_lower.0);
            }
        }

        if let Some(last_lower) = last_free_lower {
            if last_lower <= range.upper {
                self.disjoint_regions.insert(last_lower, (range.upper, value));
                self.reverse_disjoint_regions.insert(range.upper, last_lower);
            }
        }

        //Merge adj equal

        let left_adj = range.lower.complement()
            .map_or(None, |lower_comp|
                self.reverse_disjoint_regions.range(..=lower_comp).next_back().filter(|possible_left_adj|
                    possible_left_adj.0.is_adjacent(&range.lower)
                )
            ).map(|(u, l)| (*l, *u));

        let right_adj = range.upper.complement()
            .map_or(None, |upper_comp|
                self.disjoint_regions.range(upper_comp..).next().filter(|possible_right_adj|
                    range.upper.is_adjacent(&possible_right_adj.0)
                )
            ).map(|(l, (u, _))| (*l, *u));

        let c = |(l, (u, _)): (&L<K>, &(U<K>, V))| (*l, *u);
        let overlap_regions = if let Some(u) = range.upper.complement() {
            self.disjoint_regions.range(range.lower..u).map(c)
        } else {
            self.disjoint_regions.range(range.lower..).map(c)
        };

        let mut overlap_check = left_adj.into_iter().chain(overlap_regions).chain(right_adj.into_iter()).map(|(l, u)| (l, u, self.disjoint_regions.get(&l).unwrap().1)).collect::<Vec<_>>().into_iter();

        if let Some(mut last) = overlap_check.next() {
            let mut new_upper = None;
            while let Some(next) = overlap_check.next() {
                if last.2 == next.2 {
                    self.disjoint_regions.remove(&next.0).unwrap();
                    self.reverse_disjoint_regions.remove(&next.1).unwrap();
                    new_upper = Some(next.1);
                } else {
                    if let Some(new_upper) = new_upper.take() {
                        self.reverse_disjoint_regions.remove(&last.1);
                        self.reverse_disjoint_regions.insert(new_upper, last.0);
                        self.disjoint_regions.get_mut(&last.0).unwrap().0 = new_upper;
                    }
                    last = next;
                }
            }
            if let Some(new_upper) = new_upper.take() {
                self.reverse_disjoint_regions.remove(&last.1);
                self.reverse_disjoint_regions.insert(new_upper, last.0);
                self.disjoint_regions.get_mut(&last.0).unwrap().0 = new_upper;
            }
        }
    }

    pub fn remove<R: RangeBounds<K>>(&mut self, range: R) {
        self.drain(range);
    }

    pub fn drain<R: RangeBounds<K>>(&mut self, range: R) -> Vec<(OverlapDescription<K>, V)> {
        let mut to_remove = self.get_overlap(range).map(|(d, v)| (d, *v)).collect::<Vec<_>>();
        to_remove.reverse();
        for (d, v) in to_remove.iter().copied() {
            self.disjoint_regions.remove(&d.range.lower).unwrap();
            self.reverse_disjoint_regions.remove(&d.range.upper).unwrap();
            if d.range != d.overlap_range {
                if d.range.lower < d.overlap_range.lower {
                    // Always Some() since there is something less than it.
                    let new_upper = d.overlap_range.lower.complement().unwrap();
                    self.disjoint_regions.insert(d.range.lower, (new_upper, v));
                    self.reverse_disjoint_regions.insert(new_upper, d.range.lower);
                }
                if d.range.upper > d.overlap_range.upper {
                    // Always Some() since there is something great then it
                    let new_lower = d.overlap_range.upper.complement().unwrap();
                    self.disjoint_regions.insert(new_lower, (d.range.upper, v));
                    self.reverse_disjoint_regions.insert(d.range.upper, new_lower);
                }
            }
        }
        to_remove
    }

    pub fn contains(&self, key: K) -> bool {
        let k_l = LowerBound::Inclusive(TotallyOrdered(key));
        let k_u = UpperBound::Inclusive(TotallyOrdered(key));

        self.disjoint_regions.range(..=k_l).next_back()
            .map_or(false, |(_, (u, _))| k_u <= *u)
    }

    pub fn get(&self, key: K) -> Option<&V> {
        let k_l = LowerBound::Inclusive(TotallyOrdered(key));
        let k_u = UpperBound::Inclusive(TotallyOrdered(key));

        self.disjoint_regions.range(..=k_l).next_back()
            .map(|(_, (u, v))| {
                (k_u <= *u).then(|| v)
            })
            .flatten()
    }

    pub fn get_mut(&mut self, key: K) -> Option<&mut V> {
        let k_l = LowerBound::Inclusive(TotallyOrdered(key));
        let k_u = UpperBound::Inclusive(TotallyOrdered(key));

        self.disjoint_regions.range_mut(..=k_l).next_back()
            .map(|(_, (u, v))| (k_u <= *u).then(|| v))
            .flatten()
    }

    /// Note: Iterates in reverse order
    pub fn get_overlap<R: RangeBounds<K>>(&self, range: R) -> impl Iterator<Item=(OverlapDescription<K>, &V)> {
        let range = Range::new_wrap(range, |v| TotallyOrdered(v));

        self.disjoint_regions.range(..=range.upper.flip()).rev().map_while(move |(lower, (upper, v))| {
            let other_range = Range { lower: *lower, upper: *upper };
            other_range.overlap(range).map(|overlap_range| (OverlapDescription {
                range: other_range,
                overlap_range,
            }, v))
        })
    }

    /// Note: Iterates in reverse order
    pub fn get_overlap_mut<R: RangeBounds<K>>(&mut self, range: R) -> impl Iterator<Item=(OverlapDescription<K>, &mut V)> {
        let range = Range::new_wrap(range, |v| TotallyOrdered(v));

        self.disjoint_regions.range_mut(..=range.upper.flip()).rev().map_while(move |(lower, (upper, v))| {
            let other_range = Range { lower: *lower, upper: *upper };
            other_range.overlap(range).map(|overlap_range| (OverlapDescription {
                range: other_range,
                overlap_range,
            }, v))
        })
    }
}

pub trait TotalEq {
    fn total_eq(&self, other: &Self) -> bool;
}

macro_rules! int_total_eq {
        ($($int:ty),*) => {
        $(
            impl TotalEq for $int {
                fn total_eq(&self, other: &Self) -> bool {
                    self.eq(other)
                }
            }
        )*
    };
}

int_total_eq!(u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize);

impl TotalEq for f32 {
    fn total_eq(&self, other: &Self) -> bool {
        self.to_bits() == other.to_bits()
    }
}

impl TotalEq for f64 {
    fn total_eq(&self, other: &Self) -> bool {
        self.to_bits() == other.to_bits()
    }
}

pub trait TotalOrd: TotalEq {
    fn total_cmp(&self, other: &Self) -> Ordering;
}

macro_rules! int_total_ord {
        ($($int:ty),*) => {
        $(
            impl TotalOrd for $int {
                fn total_cmp(&self, other: &Self) -> Ordering {
                    self.cmp(other)
                }
            }
        )*
    };
}

int_total_ord!(u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize);

impl TotalOrd for f32 {
    fn total_cmp(&self, other: &Self) -> Ordering {
        // TODO: Just use this once in stable
        // self.total_cmp(other)

        let mut left = self.to_bits() as i32;
        let mut right = other.to_bits() as i32;
        left ^= (((left >> 31) as u32) >> 1) as i32;
        right ^= (((right >> 31) as u32) >> 1) as i32;

        left.cmp(&right)
    }
}

impl TotalOrd for f64 {
    fn total_cmp(&self, other: &Self) -> Ordering {
        // TODO: Just use this once in stable
        // self.total_cmp(other)

        let mut left = self.to_bits() as i64;
        let mut right = other.to_bits() as i64;
        left ^= (((left >> 63) as u64) >> 1) as i64;
        right ^= (((right >> 63) as u64) >> 1) as i64;

        left.cmp(&right)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct TotallyOrdered<T: TotalOrd>(pub T);

impl<T: TotalOrd> Eq for TotallyOrdered<T> {}

impl<T: TotalOrd> PartialEq<Self> for TotallyOrdered<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.total_eq(&other.0)
    }
}

impl<T: TotalOrd> PartialOrd<Self> for TotallyOrdered<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: TotalOrd> Ord for TotallyOrdered<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.total_cmp(&other.0)
    }
}

impl<T: TotalOrd + AdjacentValueCheck> AdjacentValueCheck for TotallyOrdered<T> {
    fn is_adjacent(&self, other: &Self) -> bool {
        self.0.is_adjacent(&other.0)
    }
}

impl<T: Ord + TotalOrd> RangeBounds<T> for Range<TotallyOrdered<T>> {
    fn start_bound(&self) -> Bound<&T> {
        match &self.lower {
            LowerBound::Unbounded => Bound::Unbounded,
            LowerBound::Inclusive(v) => Bound::Included(&v.0),
            LowerBound::Exclusive(v) => Bound::Excluded(&v.0),
        }
    }

    fn end_bound(&self) -> Bound<&T> {
        match &self.upper {
            UpperBound::Unbounded => Bound::Unbounded,
            UpperBound::Inclusive(v) => Bound::Included(&v.0),
            UpperBound::Exclusive(v) => Bound::Excluded(&v.0),
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::utility::region_map::RegionMap;

    #[test]
    fn test_no_adj_merge() {
        let mut map = RegionMap::<usize, i32>::new();

        // After should be:
        // [0, 10] = 1
        map.insert(0..=10, 1, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1));
        assert_eq!(map.get(5).copied(), Some(1));
        assert_eq!(map.get(10).copied(), Some(1));
        assert_eq!(map.get(11).copied(), None);
        assert_eq!(map.get(100).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 10] = 1
        // [15, 20] = 2
        map.insert(15..=20, 2, |new, old| new + old);
        assert_eq!(map.get(13).copied(), None);
        assert_eq!(map.get(14).copied(), None);
        assert_eq!(map.get(15).copied(), Some(2));
        assert_eq!(map.get(20).copied(), Some(2));
        assert_eq!(map.get(21).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 4] = 1
        // [5, 10] = 1 + 4 = 5
        // [11, 14] = 4
        // [14, 17] = 2 + 4 = 6
        // [18, 20] = 2
        map.insert(5..=17, 4, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1));
        assert_eq!(map.get(4).copied(), Some(1));
        assert_eq!(map.get(5).copied(), Some(1 + 4));
        assert_eq!(map.get(10).copied(), Some(1 + 4));
        assert_eq!(map.get(13).copied(), Some(4));
        assert_eq!(map.get(17).copied(), Some(2 + 4));
        assert_eq!(map.get(20).copied(), Some(2));
        assert_eq!(map.get(21).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 4] = 1
        // [5, 10] = 1 + 4 = 5
        // [11, 14] = 4
        // [15, 17] = 2 + 4 = 6
        // [18, 20] = 2
        // [25, 50] = 8
        map.insert(25..=50, 8, |new, old| new + old);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());
        // dbg!(map.get_overlap(0..=100).collect::<Vec<_>>());

        // After should be:
        // [0, 4] = 1 + 16 = 17
        // [5, 10] = 1 + 4 + 16 = 21
        // [11, 14] = 4 + 16 = 20
        // [15, 17] = 2 + 4 + 16 = 22
        // [18, 20] = 2 + 16 = 18
        // [21, 24] = 16
        // [25, 50] = 8 + 16 = 24
        // [51, 100] = 16
        map.insert(0..=100, 16, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1 + 16));
        assert_eq!(map.get(10).copied(), Some(1 + 4 + 16));
        assert_eq!(map.get(13).copied(), Some(4 + 16));
        assert_eq!(map.get(16).copied(), Some(2 + 4 + 16));
        assert_eq!(map.get(20).copied(), Some(2 + 16));
        assert_eq!(map.get(22).copied(), Some(16));
        assert_eq!(map.get(30).copied(), Some(8 + 16));
        assert_eq!(map.get(40).copied(), Some(8 + 16));
        assert_eq!(map.get(50).copied(), Some(8 + 16));
        assert_eq!(map.get(75).copied(), Some(16));
        assert_eq!(map.get(100).copied(), Some(16));
        assert_eq!(map.get(101).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // dbg!(map.get_overlap(101..=101).collect::<Vec<_>>());
        map.insert(101..=101, 32, |new, old| new + old);
        assert_eq!(map.get(100).copied(), Some(16));
        assert_eq!(map.get(101).copied(), Some(32));
        assert_eq!(map.get(102).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());
    }

    #[test]
    fn test_no_adj_merge_excl() {
        let mut map = RegionMap::<usize, i32>::new();

        // After should be:
        // [0, 10] = 1
        map.insert(0..10 + 1, 1, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1));
        assert_eq!(map.get(5).copied(), Some(1));
        assert_eq!(map.get(10).copied(), Some(1));
        assert_eq!(map.get(11).copied(), None);
        assert_eq!(map.get(100).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 10] = 1
        // [15, 20] = 2
        map.insert(15..20 + 1, 2, |new, old| new + old);
        assert_eq!(map.get(13).copied(), None);
        assert_eq!(map.get(14).copied(), None);
        assert_eq!(map.get(15).copied(), Some(2));
        assert_eq!(map.get(20).copied(), Some(2));
        assert_eq!(map.get(21).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 4] = 1
        // [5, 10] = 1 + 4 = 5
        // [11, 14] = 4
        // [14, 17] = 2 + 4 = 6
        // [18, 20] = 2
        map.insert(5..17 + 1, 4, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1));
        assert_eq!(map.get(4).copied(), Some(1));
        assert_eq!(map.get(5).copied(), Some(1 + 4));
        assert_eq!(map.get(10).copied(), Some(1 + 4));
        assert_eq!(map.get(13).copied(), Some(4));
        assert_eq!(map.get(17).copied(), Some(2 + 4));
        assert_eq!(map.get(20).copied(), Some(2));
        assert_eq!(map.get(21).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After should be:
        // [0, 4] = 1
        // [5, 10] = 1 + 4 = 5
        // [11, 14] = 4
        // [15, 17] = 2 + 4 = 6
        // [18, 20] = 2
        // [25, 50] = 8
        map.insert(25..50 + 1, 8, |new, old| new + old);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());
        // dbg!(map.get_overlap(0..100+1).collect::<Vec<_>>());

        // After should be:
        // [0, 4] = 1 + 16 = 17
        // [5, 10] = 1 + 4 + 16 = 21
        // [11, 14] = 4 + 16 = 20
        // [15, 17] = 2 + 4 + 16 = 22
        // [18, 20] = 2 + 16 = 18
        // [21, 24] = 16
        // [25, 50] = 8 + 16 = 24
        // [51, 100] = 16
        map.insert(0..100 + 1, 16, |new, old| new + old);
        assert_eq!(map.get(0).copied(), Some(1 + 16));
        assert_eq!(map.get(10).copied(), Some(1 + 4 + 16));
        assert_eq!(map.get(13).copied(), Some(4 + 16));
        assert_eq!(map.get(16).copied(), Some(2 + 4 + 16));
        assert_eq!(map.get(20).copied(), Some(2 + 16));
        assert_eq!(map.get(22).copied(), Some(16));
        assert_eq!(map.get(30).copied(), Some(8 + 16));
        assert_eq!(map.get(40).copied(), Some(8 + 16));
        assert_eq!(map.get(50).copied(), Some(8 + 16));
        assert_eq!(map.get(75).copied(), Some(16));
        assert_eq!(map.get(100).copied(), Some(16));
        assert_eq!(map.get(101).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        map.insert(101..101 + 1, 32, |new, old| new + old);
        assert_eq!(map.get(100).copied(), Some(16));
        assert_eq!(map.get(101).copied(), Some(32));
        assert_eq!(map.get(102).copied(), None);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());
    }


    #[test]
    fn test_adj_merge() {
        let mut map = RegionMap::new();

        map.insert(0..=1, 1, |new, old| new + old);
        map.insert(3..=4, 1, |new, old| new + old);
        map.insert(2..=2, 1, |new, old| new + old);

        assert_eq!(map.get(0).copied(), Some(1));
        assert_eq!(map.get(1).copied(), Some(1));
        assert_eq!(map.get(2).copied(), Some(1));
        assert_eq!(map.get(3).copied(), Some(1));
        assert_eq!(map.get(4).copied(), Some(1));
        assert_eq!(map.get(5).copied(), None);
        assert_eq!(map.disjoint_regions.len(), 1);
        assert_eq!(map.reverse_disjoint_regions.len(), 1);
    }


    #[test]
    fn test_overlap() {
        let mut map = RegionMap::<usize, i32>::new();

        // After:
        // [10, 20] = 1
        map.insert(10..=20, 1, |new, old| new + old);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After:
        // [10, 20] = 1
        // [25, 30] = 2
        map.insert(25..=30, 2, |new, old| new + old);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // After:
        // [10, 20] = 1
        // [25, 30] = 2
        // [40, 50] = 4
        map.insert(40..=50, 4, |new, old| new + old);
        assert_eq!(map.disjoint_regions.len(), map.reverse_disjoint_regions.len());

        // [15, 35] should overlap with [10, 20] and [25, 30] but not [40, 50]
        // dbg!(map.get_overlap(15..=35).collect::<Vec<_>>());
        assert_eq!(map.get_overlap(15..=35).count(), 2);

        assert_eq!(map.get_overlap(30..=90).count(), 2);
        assert_eq!(map.get_overlap(1..=90).count(), 3);
    }
}