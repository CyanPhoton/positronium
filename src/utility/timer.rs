use std::sync::atomic::AtomicUsize;
use std::time::{Duration, Instant};

pub struct Timer {
    name: String,
    start: Instant,
    checkpoints: Vec<(String, Instant)>,
    threshold: Option<Duration>
}

impl Timer {
    pub fn start(name: String) -> Timer {
        let start = Instant::now();

        Timer {
            name,
            start,
            checkpoints: vec![],
            threshold: None
        }
    }

    pub fn with_threshold(mut self, threshold: Duration) -> Timer {
        self.threshold = Some(threshold);
        self
    }

    pub fn checkpoint(&mut self, name: String) {
        let now = Instant::now();

        self.checkpoints.push((name, now));
    }

    pub fn cancel(mut self) {
        unsafe { std::ptr::drop_in_place(&mut self.name); }
        unsafe { std::ptr::drop_in_place(&mut self.start); }
        unsafe { std::ptr::drop_in_place(&mut self.checkpoints); }
        std::mem::forget(self)
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        let end = Instant::now();
        let d = end.duration_since(self.start);
        if let Some(t) = self.threshold {
            if d < t {
                return;
            }
        }

        println!("Timer: [{}], total time: {:?}", self.name, d );
        let mut previous = self.start;
        for (cn, ct) in &self.checkpoints {
            println!("\tCheckpoint: [{}], since start: {:?}, since previous: {:?}", cn, ct.duration_since(self.start), ct.duration_since(previous));
            previous = *ct;
        }
    }
}