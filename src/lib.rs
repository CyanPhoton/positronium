#![feature(int_roundings)] // TODO: Potentially extract if the stabilization on this stalls

extern crate core;

use std::thread;
use std::time::Duration;
use crate::input_manager::InputManager;

use crate::renderer::{Renderer, RendererSettings};
use crate::window_manager::WindowManager;


#[macro_use]
pub(crate) mod utility;

pub mod window_manager;
pub mod input_manager;

pub mod renderer;

pub use positronium_vk::Version as Version;

pub use flume;
pub use winit;

const ENGINE_NAME: positronium_vk::UnsizedCStr<'static> = positronium_vk::unsized_cstr!("positronium");
const ENGINE_VERSION: Version = Version::new(0,0,1,0);

pub struct ApplicationSettings {
    pub application_name: String,
    pub application_version: Version,
    pub renderer_settings: RendererSettings,
}

impl Default for ApplicationSettings {
    fn default() -> Self {
        ApplicationSettings {
            application_name: "Positronium App".to_string(),
            application_version: Version::new(0,0,0,0),
            renderer_settings: Default::default()
        }
    }
}

pub struct TickInfo {
    pub run_duration: Duration,
    pub tick_delta: Duration,
}

pub trait Application: Sized {
    type Data;

    fn new(window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>) -> (Self, ApplicationSettings);
    fn setup(&mut self, window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>, renderer: &mut Renderer) -> Self::Data;
    fn tick(&mut self, data: &mut Self::Data, window_manager: &mut WindowManager, input_manager: &mut InputManager<Self>, renderer: &mut Renderer, tick_info: &TickInfo) -> bool;
    fn cleanup(self, data: Self::Data);
}

pub struct Positronium {}

impl Positronium {
    pub fn run<A: Application>() -> ! {
        let (mut window_manager, window_loop) = WindowManager::new();



        let main_thead = thread::Builder::new().name("Main Thread".to_string()).spawn(move || {
            let mut input_manager = InputManager::new();
            let (mut app, settings) = A::new(&mut window_manager, &mut input_manager);
            let mut renderer = Renderer::new(&settings.application_name, settings.application_version, settings.renderer_settings);
            let mut window_manager = window_manager;

            let mut data =  app.setup(&mut window_manager, &mut input_manager, &mut renderer);

            loop {
                window_manager.update(&mut input_manager, &mut app, &mut renderer);

                let tick_info = TickInfo { run_duration: Default::default(), tick_delta: Default::default() };
                let keep_alive = app.tick(&mut data, &mut window_manager, &mut input_manager, &mut renderer, &tick_info);

                if !keep_alive {
                    break;
                }
            }

            app.cleanup(data);

            window_manager.clean_up();
        }).unwrap();

        window_loop.run(Some(main_thead))
    }
}