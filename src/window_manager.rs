use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use fxhash::FxHashMap;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use winit::dpi::PhysicalSize;
use winit::event::{DeviceEvent, Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop, EventLoopProxy};
use winit::window::{Window, WindowBuilder, WindowId};
use crate::Application;
use crate::input_manager::{InputEvent, InputManager};
use crate::renderer::Renderer;

#[derive(Debug)]
pub struct WindowCreateInfo {
    title: String,
    size: PhysicalSize<u32>,
    result_sender: flume::Sender<CreatedWindowInfo>
}

#[derive(Debug)]
pub struct CreatedWindowInfo {
    window_id: WindowId,
    raw_handle: RawWindowHandleWrap,
    latest_size: Arc<Mutex<PhysicalSize<u32>>>
}

#[derive(Debug)]
pub(crate) struct RawWindowHandleWrap(RawWindowHandle);

impl Deref for RawWindowHandleWrap {
    type Target = RawWindowHandle;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

// Safety: Only used for surface creation and by HasRawWindowHandle it should live as long as the window
unsafe impl Send for RawWindowHandleWrap {}

#[derive(Debug)]
enum UserEvent {
    CreateWindow(WindowCreateInfo),
    CloseWindow(WindowId),
    SetBlockingResizeCallback(WindowId, CallbackWrap<BlockingResizeCallbackBox>),
    Shutdown,
}

#[derive(Debug)]
pub(crate) enum ReturnEvent {
    WindowCloseRequested(WindowId),
    WindowResize(WindowId, PhysicalSize<u32>),
    InputEvent(InputEvent),
    Shutdown,
}

pub type BlockingResizeCallbackBox = Box<dyn FnMut(PhysicalSize<u32>) + Send>;

struct CallbackWrap<T>(T);

impl<T> Debug for CallbackWrap<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Fn(..)")
    }
}

pub struct WindowLoop {
    event_loop: EventLoop<UserEvent>,
    return_event_sender: flume::Sender<ReturnEvent>,
    blocking_resize_callbacks: FxHashMap<WindowId, BlockingResizeCallbackBox>
}

pub struct WindowManager {
    proxy: EventLoopProxy<UserEvent>,
    windows: FxHashMap<WindowId, WindowState>,
    return_event_receiver: flume::Receiver<ReturnEvent>,
}

struct WindowState {
    is_close_requested: bool,
    size: PhysicalSize<u32>,
    latest_size: Arc<Mutex<PhysicalSize<u32>>>
}

struct InternalWindowState {
    window: Window,
    latest_size: Arc<Mutex<PhysicalSize<u32>>>
}

impl WindowState {
    pub fn new(latest_size: Arc<Mutex<PhysicalSize<u32>>>) -> WindowState {
        WindowState {
            is_close_requested: false,
            size: Default::default(),
            latest_size
        }
    }
}

pub struct WindowRef {
    proxy: EventLoopProxy<UserEvent>,
    pub(crate) window_id: WindowId,
    pub(crate) raw_handle: RawWindowHandle
    // Potentially add Rc<RefCell<WindowState>> if hashmap lookup is too slow
}

unsafe impl HasRawWindowHandle for WindowRef {
    fn raw_window_handle(&self) -> RawWindowHandle {
        self.raw_handle
    }
}

impl Drop for WindowRef {
    fn drop(&mut self) {
        self.proxy.send_event(UserEvent::CloseWindow(self.window_id)).ok(); // Don't panic if can't send
    }
}

impl WindowRef {
    pub fn close(self) {}
}

impl WindowLoop {
    pub fn run(mut self, mut main_thread: Option<JoinHandle<()>>) -> ! {
        let mut windows = FxHashMap::<WindowId, InternalWindowState>::default();

        self.event_loop.run(move |event, target, control_flow| {
            *control_flow = ControlFlow::Wait;

            match event {
                Event::NewEvents(_) => {}
                Event::WindowEvent { window_id, event } => {
                    match event {
                        WindowEvent::Resized(s) => {
                            if let Some(w) = windows.get(&window_id) {
                                *w.latest_size.lock().unwrap().deref_mut() = s;
                            }
                            self.return_event_sender.send(ReturnEvent::WindowResize(window_id, s)).unwrap();

                            if let Some(callback) = self.blocking_resize_callbacks.get_mut(&window_id) {
                                callback(s);
                            }
                        },
                        WindowEvent::CloseRequested => self.return_event_sender.send(ReturnEvent::WindowCloseRequested(window_id)).unwrap(),
                        WindowEvent::ReceivedCharacter(c) => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::ReceivedCharacter { window_id, c })).unwrap(),
                        WindowEvent::KeyboardInput { input, is_synthetic, .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::Keyboard { window_id, input, is_synthetic })).unwrap(),
                        WindowEvent::ModifiersChanged(modifiers_state) => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::ModifiersChanged { window_id, modifiers_state })).unwrap(),
                        WindowEvent::CursorMoved { position, .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::CursorMoved { window_id, position })).unwrap(),
                        WindowEvent::CursorEntered { .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::CursorEntered { window_id })).unwrap(),
                        WindowEvent::CursorLeft { .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::CursorLeft { window_id })).unwrap(),
                        WindowEvent::MouseWheel { delta, .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::MouseWheel { window_id, delta })).unwrap(),
                        WindowEvent::MouseInput { state, button, .. } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::MouseButton { window_id, state, button })).unwrap(),
                        _ => {}
                    }
                }
                Event::DeviceEvent { event, ..} => {
                    match event {
                        DeviceEvent::MouseMotion { delta } => self.return_event_sender.send(ReturnEvent::InputEvent(InputEvent::RawMouseMoved { delta })).unwrap(),
                        _ => {}
                    }
                }
                Event::UserEvent(user_event) => {
                    match user_event {
                        UserEvent::CreateWindow(info) => {
                            let window = WindowBuilder::new()
                                .with_title(info.title)
                                .with_inner_size(info.size)
                                .build(target)
                                .expect("Failed to create window");

                            let latest_size = Arc::new(Mutex::new(info.size));

                            info.result_sender.send(CreatedWindowInfo {
                                window_id: window.id(),
                                raw_handle: RawWindowHandleWrap(window.raw_window_handle()),
                                latest_size: latest_size.clone()
                            }).unwrap();

                            let id = window.id();
                            let internal_state = InternalWindowState {
                                window,
                                latest_size
                            };

                            windows.insert(id, internal_state);
                        }
                        UserEvent::Shutdown => {
                            *control_flow = ControlFlow::Exit;
                        }
                        UserEvent::CloseWindow(id) => {
                            windows.remove(&id);
                        }
                        UserEvent::SetBlockingResizeCallback(id, f) => {
                            self.blocking_resize_callbacks.insert(id, f.0);
                        }
                    }
                }
                Event::Suspended => {}
                Event::Resumed => {}
                Event::MainEventsCleared => {}
                Event::RedrawRequested(_) => {}
                Event::RedrawEventsCleared => {}
                Event::LoopDestroyed => {
                    if let Some(main_thread) = main_thread.take() {
                        self.return_event_sender.send(ReturnEvent::Shutdown).unwrap();
                        main_thread.join().unwrap();
                    }
                }
            }
        })
    }
}

impl WindowManager {
    pub fn new() -> (WindowManager, WindowLoop) {
        let event_loop = EventLoop::with_user_event();

        let (return_event_sender, return_event_receiver) = flume::unbounded();

        (
            WindowManager {
                proxy: event_loop.create_proxy(),
                windows: HashMap::default(),
                return_event_receiver,
            },
            WindowLoop {
                event_loop,
                return_event_sender,
                blocking_resize_callbacks: Default::default()
            }
        )
    }

    pub(crate) fn update<A: Application>(&mut self, input_manager: &mut InputManager<A>, app: &mut A, renderer: &mut Renderer) {
        input_manager.begin_process_events();

        while let Ok(e) = self.return_event_receiver.try_recv() {
            match e {
                ReturnEvent::WindowCloseRequested(id) => {
                    self.windows.get_mut(&id).unwrap().is_close_requested = true;
                }
                ReturnEvent::InputEvent(event) => {
                    input_manager.process_event(event, app, self, renderer);
                }
                ReturnEvent::WindowResize(id, size) => {
                    self.windows.get_mut(&id).unwrap().size = size;
                }
                ReturnEvent::Shutdown => {}
            }
        }

        input_manager.end_process_events();
    }

    pub(crate) fn set_blocking_resize_callback<F: 'static + FnMut(PhysicalSize<u32>) + Send>(&self, window: &WindowRef, callback: F) {
        self.proxy.send_event(UserEvent::SetBlockingResizeCallback(window.window_id, CallbackWrap(Box::new(callback)))).unwrap();
    }

    pub fn create_window<T: Into<String>, S: Into<PhysicalSize<u32>>>(&mut self, title: T, size: S) -> WindowRef {
        let (result_sender, result_receiver) = flume::bounded(1);

        let size = size.into();
        let create_info = WindowCreateInfo {
            title: title.into(),
            size,
            result_sender
        };

        self.proxy.send_event(UserEvent::CreateWindow(create_info)).unwrap();

        let window_info = result_receiver.recv().unwrap();

        self.windows.insert(window_info.window_id, WindowState::new(window_info.latest_size));

        let r = WindowRef {
            proxy: self.proxy.clone(),
            window_id: window_info.window_id,
            raw_handle: window_info.raw_handle.0,
        };

        r
    }

    /// Value is updated just before each tick, and will not change throughout a given tick
    pub fn get_window_size(&self, window: &WindowRef) -> PhysicalSize<u32> {
        self.windows.get(&window.window_id).unwrap().size
    }

    /// Value is the most recent possible, hopefully updating as soon as possible after a size change.
    /// Will block if the called while the window manager holds the lock to set the value
    pub fn get_window_size_latest(&self, window: &WindowRef) -> PhysicalSize<u32> {
        *self.windows.get(&window.window_id).unwrap().latest_size.lock().unwrap()
    }

    pub fn is_close_requested(&self, window: &WindowRef) -> bool {
        self.windows.get(&window.window_id)
            .map(|ws| ws.is_close_requested)
            .unwrap_or(true) // If it's already closed and thus removed, then yes
    }

    pub fn request_close(&mut self, window: &WindowRef) {
        if let Some(w) = self.windows.get_mut(&window.window_id) {
            w.is_close_requested = true;
        }
    }

    pub(crate) fn clean_up(self) {
        self.proxy.send_event(UserEvent::Shutdown).unwrap();
        loop {
            match self.return_event_receiver.recv() {
                Ok(ReturnEvent::Shutdown) | Err(_) => return,
                Ok(_) => {}
            }
        }
    }
}