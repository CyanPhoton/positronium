#![allow(dead_code)]

pub mod linked_vec;
pub mod timer;
pub mod region_set;
pub mod region_map;
pub mod bounds;
pub mod disjoint_buffer_map;
pub mod disjoint_buffer_set;

use std::mem::ManuallyDrop;

/// Replaces the existing value, while allowing you to consume the existing value
pub fn replace_with<T: Sized, F: FnOnce(T) -> T>(dst: &mut T, f: F) {
    let old = unsafe { std::ptr::read(dst) };
    let new = f(old);
    unsafe { std::ptr::write(dst, new); }
}

pub fn take_replace<T: Sized, U: Sized, F: FnOnce(T) -> U, G: FnOnce() -> T>(dst: &mut T, f: F, g: G) -> U {
    let old = unsafe { std::ptr::read(dst) };
    let result = f(old);
    let new = g();
    unsafe { std::ptr::write(dst, new); }
    result
}

/// First drops the existing value, then constructs and replaces it with the new value
///
/// This is the opposite order compared with either `*dst = f();` or `std::mem::replace(dst, f());`
pub fn drop_replace_with<T: Sized, F: FnOnce() -> T>(dst: &mut T, f: F) {
    unsafe { std::ptr::drop_in_place(dst); }
    unsafe { std::ptr::write(dst, f()); }
}

/// Replaces `dst` with `value`, placing the old value of `dst` in `storage`
pub fn store_replace<T: Sized>(dst: &mut T, storage: &mut Option<T>, value: T) {
    // Move old value to storage
    unsafe { *storage = Some(std::ptr::read(dst)) }
    // Override dst without calling destructor on a copy of the old data, since T might not be Copy
    unsafe { std::ptr::write(dst, value) }
}

/// Replaces `dst` with `value`, placing the old value of `dst` in `storage`
pub fn extend_replace<T: Sized, E: Extend<T>>(dst: &mut T, storage: &mut E, value: T) {
    // Move old value to temp
    let tmp = unsafe { std::ptr::read(dst) };
    storage.extend(std::iter::once(tmp));

    // Override dst without calling destructor on a copy of the old data, since T might not be Copy
    unsafe { std::ptr::write(dst, value) }
}

pub fn vec_retain_take<T: Sized, R: FnMut(&mut T) -> bool, F: FnMut(T)>(vec: &mut Vec<T>, mut retain: R, mut take: F) {
    // Safe since ManuallyDrop<T> and (T) have the same layout
    let vec: &mut Vec<ManuallyDrop<T>> = unsafe { std::mem::transmute(vec) };

    // If < j, then points to the first 'empty' slot one can move into
    let mut i = 0;
    // Points to the next valid value to check, or one beyond to indicate finished
    let mut j = 0;

    while j < vec.len() {
        // Safe since ManuallyDrop<T> and (T) have the same layout, and we make sure that j always points to valid entries
        let v: &mut T = unsafe { std::mem::transmute(&mut vec[j]) };

        if retain(v) {
            if i < j {
                // If first 'empty' is before this one we want to retain, copy it left
                vec.swap(i, j);
            }
            // Next 'emtpy' will be on over
            i += 1;
        } else {
            // Safe since from reference
            let v = unsafe { std::ptr::read(v) };

            take(v);
        }
        // Next valid (if any) will be 1 over
        j += 1;
    }

    // I points to first empty, thus is number of valid elements to the left
    // truncate works here since it's ManuallyDrop
    vec.truncate(i);
}

pub fn is_overlapping(src_offset: usize, dst_offset: usize, len: usize) -> bool {
    src_offset + len > dst_offset && src_offset < dst_offset + len
}

#[cfg(windows)]
macro_rules! path_sep {
    () => {
        "\\"
    };
}

#[cfg(unix)]
macro_rules! path_sep {
    () => {
        "/"
    };
}

#[macro_export]
macro_rules! include_shader {
    ($($path:literal),+) => {
        include!(
            concat!(
                env!("SHADERS_DIR") $(,path_sep!(), $path)+
            )
        )
    };
}

#[macro_export]
macro_rules! generalise {
    ($([$vis:vis])? $type_name:ident, $trait_name:ident, $placeholder:ty, $super_trait:path $(,$other_super_traits:path)* | $($type_cond:ident),* | $($free_pre_generic:ident),* $(( $pre_generic:ident, $pre_cond:ident )),* | $($free_post_generic:ident),* $(( $post_generic:ident, $post_cond:ident )),*) => {
        // pub trait $trait_name<M: Memory> {
        $($vis)? trait $trait_name< $($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)* >: $super_trait $(+ $other_super_traits)* {
            unsafe fn as_ref(&self) -> &$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*>;
            unsafe fn as_mut(&mut self) -> &mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*>;
            fn type_id(&self) -> TypeId;
        }

        impl<T: 'static $(+ $type_cond)*, $($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)*> $trait_name<$($free_pre_generic,)* $( $pre_generic ,)* $($free_post_generic,)* $( $post_generic ,)*> for $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>  {
            unsafe fn as_ref(&self) -> &$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*> {
                ::std::mem::transmute(self)
            }

            unsafe fn as_mut(&mut self) -> &mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*> {
                ::std::mem::transmute(self)
            }

            fn type_id(&self) -> TypeId {
                ::std::any::TypeId::of::<T>()
            }
        }

        impl<$($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)*> dyn $trait_name<$($free_pre_generic,)* $( $pre_generic ,)* $($free_post_generic,)* $( $post_generic ,)*> {
            pub fn monomorphism_ref<T: 'static $(+ $type_cond)*>(&self) -> Option<&$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>> {
                (self.type_id() == ::std::any::TypeId::of::<T>()).then(|| unsafe { ::std::mem::transmute(self.as_ref()) })
            }

            pub fn monomorphism_mut<T: 'static $(+ $type_cond)*>(&mut self) -> Option<&mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>> {
                (self.type_id() == ::std::any::TypeId::of::<T>()).then(|| unsafe { ::std::mem::transmute(self.as_mut()) })
            }
        }

    };
    ($([$vis:vis])? $type_name:ident, $trait_name:ident, $placeholder:ty | $($type_cond:ident),* | $($free_pre_generic:ident),* $(( $pre_generic:ident, $pre_cond:ident )),* | $($free_post_generic:ident),* $(( $post_generic:ident, $post_cond:ident )),*) => {
        // pub trait $trait_name<M: Memory> {
        $($vis)? trait $trait_name< $($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)* > {
            unsafe fn as_ref(&self) -> &$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*>;
            unsafe fn as_mut(&mut self) -> &mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*>;
            fn type_id(&self) -> TypeId;
        }

        impl<T: 'static $(+ $type_cond)*, $($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)*> $trait_name<$($free_pre_generic,)* $( $pre_generic ,)* $($free_post_generic,)* $( $post_generic ,)*> for $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>  {
            unsafe fn as_ref(&self) -> &$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*> {
                ::std::mem::transmute(self)
            }

            unsafe fn as_mut(&mut self) -> &mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  $placeholder, $($free_post_generic,)* $( $post_generic ,)*> {
                ::std::mem::transmute(self)
            }

            fn type_id(&self) -> TypeId {
                ::std::any::TypeId::of::<T>()
            }
        }

        impl<$($free_pre_generic,)* $( $pre_generic : $pre_cond ,)* $($free_post_generic,)* $( $post_generic : $post_cond ,)*> dyn $trait_name<$($free_pre_generic,)* $( $pre_generic ,)* $($free_post_generic,)* $( $post_generic ,)*> {
            pub fn monomorphism_ref<T: 'static $(+ $type_cond)*>(&self) -> Option<&$type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>> {
                (self.type_id() == ::std::any::TypeId::of::<T>()).then(|| unsafe { ::std::mem::transmute(self.as_ref()) })
            }

            pub fn monomorphism_mut<T: 'static $(+ $type_cond)*>(&mut self) -> Option<&mut $type_name<$($free_pre_generic,)* $( $pre_generic ,)*  T, $($free_post_generic,)* $( $post_generic ,)*>> {
                (self.type_id() == ::std::any::TypeId::of::<T>()).then(|| unsafe { ::std::mem::transmute(self.as_mut()) })
            }
        }

    };
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn vec_retain_take_test() {
        let mut a = vec![true, false, true, false];

        let mut b = vec![];
        vec_retain_take(&mut a, |v| *v, |v| b.push(v));

        assert_eq!(a, vec![true, true]);
        assert_eq!(b, vec![false, false]);
    }

    #[test]
    pub fn overlapping_test() {
        assert!(!is_overlapping(10, 13, 3));
        assert!(!is_overlapping(13, 10, 3));

        assert!(is_overlapping(11, 13, 3));
        assert!(is_overlapping(13, 11, 3));

        assert!(is_overlapping(10, 12, 3));
        assert!(is_overlapping(12, 10, 3));

        assert!(!is_overlapping(10, 13, 2));
        assert!(!is_overlapping(13, 10, 2));

        assert!(is_overlapping(10, 13, 4));
        assert!(is_overlapping(13, 10, 4));
    }
}