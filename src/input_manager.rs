use std::collections::{HashMap, HashSet};
use fxhash::{FxHashMap, FxHashSet};
use winit::dpi::PhysicalPosition;
use winit::event::{ElementState, KeyboardInput, ModifiersState, MouseButton, MouseScrollDelta, ScanCode, VirtualKeyCode};
use winit::window::WindowId;
use crate::Application;
use crate::renderer::Renderer;
use crate::window_manager::{WindowManager, WindowRef};

#[derive(Copy, Clone)]
pub struct MouseScrollEvent {
    pub scroll: MouseScrollDelta,
    pub mouse_position: PhysicalPosition<f64>,
    pub current_modifiers: ModifiersState
}

#[derive(Copy, Clone)]
pub struct MouseMovementEvent {
    pub old_position: PhysicalPosition<f64>,
    pub new_position: PhysicalPosition<f64>,
    pub current_modifiers: ModifiersState
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Default, Hash, Debug)]
pub struct CharStreamID(usize);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Default, Hash, Debug)]
pub struct ButtonCallbackID(usize);

struct ExtraButtonInfo {
    is_repeat: bool,
    is_synthetic: bool,
}

pub struct CallbackInfo<'context> {
    pub window_manager: &'context mut WindowManager,
    pub renderer: &'context mut Renderer,
    pub mouse_position: Option<PhysicalPosition<f64>>,
    pub is_repeat: bool,
    pub is_synthetic: bool
}

pub type ButtonCallbackBox<A> = Box<dyn FnMut(&mut A, CallbackInfo)>;

pub struct InputManager<A: Application> {
    current_modifiers: ModifiersState,

    is_pressed_scancodes: FxHashMap<WindowId, FxHashSet<ScanCode>>,
    is_pressed_virtual_keys: FxHashMap<WindowId, FxHashSet<VirtualKeyCode>>,
    is_pressed_mouse_buttons: FxHashMap<WindowId, FxHashSet<MouseButton>>,

    was_pressed_scancodes: FxHashMap<WindowId, FxHashMap<ScanCode, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,
    was_pressed_virtual_keys: FxHashMap<WindowId, FxHashMap<VirtualKeyCode, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,
    was_pressed_mouse_buttons: FxHashMap<WindowId, FxHashMap<MouseButton, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,

    next_callback_id: usize,
    callback_binding_lookup: FxHashMap<ButtonCallbackID, ButtonBinding>,

    pressed_scancode_callback: FxHashMap<WindowId, FxHashMap<ScanCode, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,
    pressed_virtual_keys_callback: FxHashMap<WindowId, FxHashMap<VirtualKeyCode, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,
    pressed_mouse_buttons_callback: FxHashMap<WindowId, FxHashMap<MouseButton, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,

    was_released_scancodes: FxHashMap<WindowId, FxHashMap<ScanCode, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,
    was_released_virtual_keys: FxHashMap<WindowId, FxHashMap<VirtualKeyCode, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,
    was_released_mouse_buttons: FxHashMap<WindowId, FxHashMap<MouseButton, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,

    released_scancode_callback: FxHashMap<WindowId, FxHashMap<ScanCode, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,
    released_virtual_keys_callback: FxHashMap<WindowId, FxHashMap<VirtualKeyCode, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,
    released_mouse_buttons_callback: FxHashMap<WindowId, FxHashMap<MouseButton, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,

    was_triggered_mouse_scroll: FxHashMap<WindowId, FxHashMap<MouseScrollDirection, Vec<(ModifiersState, Option<PhysicalPosition<f64>>, ExtraButtonInfo)>>>,
    triggered_mouse_scroll_callback: FxHashMap<WindowId, FxHashMap<MouseScrollDirection, FxHashMap<ButtonCallbackID, (ModifiersMode, ButtonCallbackBox<A>)>>>,

    current_mouse_position: Option<(WindowId, PhysicalPosition<f64>)>,
    mouse_move_events: FxHashMap<WindowId, Vec<MouseMovementEvent>>,
    raw_mouse_delta: (f64, f64),

    mouse_scroll_events: FxHashMap<WindowId, Vec<MouseScrollEvent>>,
    raw_scroll_delta: (f32, f32),

    input_chars: FxHashMap<WindowId, Vec<char>>,
    next_char_stream_id: usize,
    char_streams: FxHashMap<WindowId, FxHashMap<CharStreamID, flume::Sender<Vec<char>>>>,
}

#[derive(Debug)]
pub(crate) enum InputEvent {
    Keyboard { window_id: WindowId, input: KeyboardInput, is_synthetic: bool },
    ModifiersChanged { window_id: WindowId, modifiers_state: ModifiersState },
    CursorMoved { window_id: WindowId, position: PhysicalPosition<f64> },
    CursorEntered { window_id: WindowId },
    CursorLeft { window_id: WindowId },
    MouseWheel { window_id: WindowId, delta: MouseScrollDelta },
    MouseButton { window_id: WindowId, state: ElementState, button: MouseButton },
    ReceivedCharacter { window_id: WindowId, c: char },
    RawMouseMoved { delta: (f64, f64) },
}

#[derive(Copy, Clone)]
pub enum ModifiersMode {
    Any,
    None,
    Exactly(ModifiersState),
    AtLeast(ModifiersState)
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum MouseScrollDirection {
    Up,
    UpRight,
    UpLeft,
    Down,
    DownRight,
    DownLeft,
    Left,
    Right
}

#[derive(Copy, Clone)]
pub struct ButtonBinding {
    pub mode: ButtonBindingMode,
    pub modifiers: ModifiersMode,
}

#[derive(Copy, Clone)]
pub enum ButtonBindingMode {
    Scancode(ScanCode),
    VirtualKey(VirtualKeyCode),
    Mouse(MouseButton),
    MouseScroll(MouseScrollDirection)
}

impl<A> InputManager<A> where A: Application {
    pub fn new() -> InputManager<A> {
        InputManager {
            current_modifiers: Default::default(),
            is_pressed_scancodes: Default::default(),
            is_pressed_virtual_keys: Default::default(),
            is_pressed_mouse_buttons: Default::default(),
            was_pressed_scancodes: Default::default(),
            was_pressed_virtual_keys: Default::default(),
            was_pressed_mouse_buttons: Default::default(),
            next_callback_id: 0,
            callback_binding_lookup: Default::default(),
            pressed_scancode_callback: Default::default(),
            pressed_virtual_keys_callback: Default::default(),
            pressed_mouse_buttons_callback: Default::default(),
            triggered_mouse_scroll_callback: Default::default(),
            was_released_scancodes: Default::default(),
            was_released_virtual_keys: Default::default(),
            was_released_mouse_buttons: Default::default(),
            released_scancode_callback: Default::default(),
            released_virtual_keys_callback: Default::default(),
            released_mouse_buttons_callback: Default::default(),
            was_triggered_mouse_scroll: Default::default(),
            current_mouse_position: None,
            mouse_scroll_events: Default::default(),
            mouse_move_events: Default::default(),
            raw_mouse_delta: (0.0, 0.0),
            raw_scroll_delta: (0.0, 0.0),
            input_chars: Default::default(),
            next_char_stream_id: 0,
            char_streams: Default::default()
        }
    }

    // is_button_pressed
    // was_button_pressed
    // was_button_released
    // add/remove_on_button_press
    // add/remove_on_button_release
    //
    // get_mouse_position
    // get_mouse_delta ?
    // get_raw_mouse_delta
    // get_scroll_delta
    //
    // get_input_characters
    //
    // get_current_modifiers

    fn modifiers_condition_met(current: ModifiersState, modifiers: ModifiersMode) -> bool {
        match modifiers {
            ModifiersMode::Any => true,
            ModifiersMode::None => current == ModifiersState::empty(),
            ModifiersMode::Exactly(m) => m == current,
            ModifiersMode::AtLeast(m) => current.contains(m)
        }
    }

    pub fn is_button_pressed(&self, window: &WindowRef, binding: ButtonBinding) -> bool {
        match binding.mode {
            ButtonBindingMode::Scancode(scancode) => Self::modifiers_condition_met(self.current_modifiers, binding.modifiers)
                && self.is_pressed_scancodes.get(&window.window_id).map_or(false, |v| v.contains(&scancode)),
            ButtonBindingMode::VirtualKey(virtual_key) => Self::modifiers_condition_met(self.current_modifiers, binding.modifiers)
                && self.is_pressed_virtual_keys.get(&window.window_id).map_or(false, |v| v.contains(&virtual_key)),
            ButtonBindingMode::Mouse(button) => Self::modifiers_condition_met(self.current_modifiers, binding.modifiers)
                && self.is_pressed_mouse_buttons.get(&window.window_id).map_or(false, |v| v.contains(&button)),
            ButtonBindingMode::MouseScroll(_) => false,
        }
    }

    pub fn was_button_pressed_count(&self, window: &WindowRef, binding: ButtonBinding, allow_repeat: bool, allow_synthetic: bool) -> usize {
        let empty = vec![];

        let (mods, events) = match binding.mode {
            ButtonBindingMode::Scancode(scancode) => (binding.modifiers, self.was_pressed_scancodes.get(&window.window_id).map(|scancodes| scancodes.get(&scancode)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::VirtualKey(virtual_key) => (binding.modifiers, self.was_pressed_virtual_keys.get(&window.window_id).map(|scancodes| scancodes.get(&virtual_key)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::Mouse(button) => (binding.modifiers, self.was_pressed_mouse_buttons.get(&window.window_id).map(|scancodes| scancodes.get(&button)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::MouseScroll(direction) => (binding.modifiers, self.was_triggered_mouse_scroll.get(&window.window_id).map(|scancodes| scancodes.get(&direction)).flatten().unwrap_or(&empty))
        };

        events.iter().filter(|(modifiers, _, e)| Self::modifiers_condition_met(*modifiers, mods) && (allow_repeat || !e.is_repeat) && (allow_synthetic || !e.is_synthetic) ).count()
    }

    pub fn was_button_pressed_positions<'a>(&'a self, window: &WindowRef, binding: ButtonBinding, allow_repeat: bool, allow_synthetic: bool) -> Vec<Option<PhysicalPosition<f64>>> {
        let empty = vec![];

        let (mods, events) = match binding.mode {
            ButtonBindingMode::Scancode(scancode) => (binding.modifiers, self.was_pressed_scancodes.get(&window.window_id).map(|scancodes| scancodes.get(&scancode)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::VirtualKey(virtual_key) => (binding.modifiers, self.was_pressed_virtual_keys.get(&window.window_id).map(|scancodes| scancodes.get(&virtual_key)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::Mouse(button) => (binding.modifiers, self.was_pressed_mouse_buttons.get(&window.window_id).map(|scancodes| scancodes.get(&button)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::MouseScroll(direction) => (binding.modifiers, self.was_triggered_mouse_scroll.get(&window.window_id).map(|scancodes| scancodes.get(&direction)).flatten().unwrap_or(&empty))
        };

        events.iter().filter_map(|(modifiers, p, e)| if Self::modifiers_condition_met(*modifiers, mods) && (allow_repeat || !e.is_repeat) && (allow_synthetic || !e.is_synthetic) { Some(*p) } else { None }).collect()
    }

    pub fn register_on_button_pressed<F>(&mut self, window: &WindowRef, binding: ButtonBinding, callback: F) -> ButtonCallbackID where F: FnMut(&mut A, CallbackInfo) + 'static {
        let id = ButtonCallbackID(self.next_callback_id);
        self.next_callback_id += 1;

        self.callback_binding_lookup.insert(id, binding);

        match binding.mode {
            ButtonBindingMode::Scancode(scancode) => self.pressed_scancode_callback.entry(window.window_id).or_default().entry(scancode).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::VirtualKey(virtual_key) => self.pressed_virtual_keys_callback.entry(window.window_id).or_default().entry(virtual_key).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::Mouse(button) => self.pressed_mouse_buttons_callback.entry(window.window_id).or_default().entry(button).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::MouseScroll(direction) => self.triggered_mouse_scroll_callback.entry(window.window_id).or_default().entry(direction).or_default().insert(id, (binding.modifiers, Box::new(callback))),
        };

        id
    }

    pub fn unregister_on_button_pressed<F>(&mut self, window: &WindowRef, callback_id: ButtonCallbackID) -> Option<(ButtonBinding, ButtonCallbackBox<A>)> {
        self.callback_binding_lookup.remove(&callback_id).map(|button_binding| {
            match button_binding.mode {
                ButtonBindingMode::Scancode(scancode) => {
                    self.pressed_scancode_callback.get_mut(&window.window_id).map(|scancodes|
                        scancodes.get_mut(&scancode).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                },
                ButtonBindingMode::VirtualKey(virtual_key) => {
                    self.pressed_virtual_keys_callback.get_mut(&window.window_id).map(|vks|
                        vks.get_mut(&virtual_key).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
                ButtonBindingMode::Mouse(button) => {
                    self.pressed_mouse_buttons_callback.get_mut(&window.window_id).map(|buttons|
                        buttons.get_mut(&button).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
                ButtonBindingMode::MouseScroll(direction) => {
                    self.triggered_mouse_scroll_callback.get_mut(&window.window_id).map(|dirs|
                        dirs.get_mut(&direction).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
            }
        }).flatten().flatten().flatten()
    }

    pub fn was_button_released_count(&self, window: &WindowRef, binding: ButtonBinding, allow_synthetic: bool) -> usize {
        let empty = vec![];

        let (mods, events) = match binding.mode {
            ButtonBindingMode::Scancode(scancode) => (binding.modifiers, self.was_released_scancodes.get(&window.window_id).map(|scancodes| scancodes.get(&scancode)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::VirtualKey(virtual_key) => (binding.modifiers, self.was_released_virtual_keys.get(&window.window_id).map(|scancodes| scancodes.get(&virtual_key)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::Mouse(button) => (binding.modifiers, self.was_released_mouse_buttons.get(&window.window_id).map(|scancodes| scancodes.get(&button)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::MouseScroll(direction) => (binding.modifiers, self.was_triggered_mouse_scroll.get(&window.window_id).map(|scancodes| scancodes.get(&direction)).flatten().unwrap_or(&empty))
        };

        events.iter().filter(|(modifiers, _, e)| Self::modifiers_condition_met(*modifiers, mods) && (allow_synthetic || !e.is_synthetic)).count()
    }

    pub fn was_button_released_positions<'a>(&'a self, window: &WindowRef, binding: ButtonBinding, allow_synthetic: bool) -> Vec<Option<PhysicalPosition<f64>>> {
        let empty = vec![];

        let (mods, events) = match binding.mode {
            ButtonBindingMode::Scancode(scancode) => (binding.modifiers, self.was_released_scancodes.get(&window.window_id).map(|scancodes| scancodes.get(&scancode)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::VirtualKey(virtual_key) => (binding.modifiers, self.was_released_virtual_keys.get(&window.window_id).map(|scancodes| scancodes.get(&virtual_key)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::Mouse(button) => (binding.modifiers, self.was_released_mouse_buttons.get(&window.window_id).map(|scancodes| scancodes.get(&button)).flatten().unwrap_or(&empty)),
            ButtonBindingMode::MouseScroll(direction) => (binding.modifiers, self.was_triggered_mouse_scroll.get(&window.window_id).map(|scancodes| scancodes.get(&direction)).flatten().unwrap_or(&empty))
        };

        events.iter().filter_map(|(modifiers, p, e)| if Self::modifiers_condition_met(*modifiers, mods) && (allow_synthetic || !e.is_synthetic) { Some(*p) } else { None }).collect()
    }

    pub fn register_on_button_released<F>(&mut self, window: &WindowRef, binding: ButtonBinding, callback: F) -> ButtonCallbackID where F: FnMut(&mut A, CallbackInfo) + 'static {
        let id = ButtonCallbackID(self.next_callback_id);
        self.next_callback_id += 1;

        self.callback_binding_lookup.insert(id, binding);

        match binding.mode {
            ButtonBindingMode::Scancode(scancode) => self.released_scancode_callback.entry(window.window_id).or_default().entry(scancode).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::VirtualKey(virtual_key) => self.released_virtual_keys_callback.entry(window.window_id).or_default().entry(virtual_key).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::Mouse(button) => self.released_mouse_buttons_callback.entry(window.window_id).or_default().entry(button).or_default().insert(id, (binding.modifiers, Box::new(callback))),
            ButtonBindingMode::MouseScroll(direction) => self.triggered_mouse_scroll_callback.entry(window.window_id).or_default().entry(direction).or_default().insert(id, (binding.modifiers, Box::new(callback))),
        };

        id
    }

    pub fn unregister_on_button_released<F>(&mut self, window: &WindowRef, callback_id: ButtonCallbackID) -> Option<(ButtonBinding, ButtonCallbackBox<A>)> {
        self.callback_binding_lookup.remove(&callback_id).map(|button_binding| {
            match button_binding.mode {
                ButtonBindingMode::Scancode(scancode) => {
                    self.released_scancode_callback.get_mut(&window.window_id).map(|scancodes|
                        scancodes.get_mut(&scancode).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                },
                ButtonBindingMode::VirtualKey(virtual_key) => {
                    self.released_virtual_keys_callback.get_mut(&window.window_id).map(|vks|
                        vks.get_mut(&virtual_key).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
                ButtonBindingMode::Mouse(button) => {
                    self.released_mouse_buttons_callback.get_mut(&window.window_id).map(|buttons|
                        buttons.get_mut(&button).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
                ButtonBindingMode::MouseScroll(direction) => {
                    self.triggered_mouse_scroll_callback.get_mut(&window.window_id).map(|dirs|
                        dirs.get_mut(&direction).map(|callbacks| callbacks.remove(&callback_id).map(|(_, callback)| (button_binding, callback)))
                    )
                }
            }
        }).flatten().flatten().flatten()
    }

    pub fn get_mouse_position(&self, window: &WindowRef) -> Option<PhysicalPosition<f64>> {
        if let Some((w, p)) = &self.current_mouse_position {
            (&window.window_id == w).then(|| *p)
        } else {
            None
        }
    }

    pub fn get_mouse_move_events(&self, window: &WindowRef) -> Option<&Vec<MouseMovementEvent>> {
        self.mouse_move_events.get(&window.window_id)
    }

    pub fn get_raw_mouse_delta(&self) -> (f64, f64) {
        self.raw_mouse_delta
    }

    pub fn get_scroll_events(&self, window: &WindowRef) -> Option<&Vec<MouseScrollEvent>> {
        self.mouse_scroll_events.get(&window.window_id)
    }

    pub fn get_raw_scroll_delta(&self) -> (f32, f32) {
        self.raw_scroll_delta
    }

    pub fn get_input_chars(&self, window: &WindowRef) -> Option<&Vec<char>> {
        self.input_chars.get(&window.window_id)
    }

    pub fn register_input_char_stream(&mut self, window: &WindowRef) -> (flume::Receiver<Vec<char>>, CharStreamID) {
        let (sender, receiver) = flume::unbounded();

        let id = CharStreamID(self.next_char_stream_id);
        self.next_char_stream_id += 1;

        self.char_streams.entry(window.window_id).or_default().insert(id, sender);

        (receiver, id)
    }

    pub fn unregister_input_char_stream(&mut self, window: &WindowRef, stream_id: CharStreamID) {
        if let Some(s) = self.char_streams.get_mut(&window.window_id) {
            s.remove(&stream_id);
        }
    }

    pub fn get_current_modifiers(&self) -> ModifiersState {
        self.current_modifiers
    }

    pub(crate) fn begin_process_events(&mut self) {
        self.was_pressed_scancodes.clear();
        self.was_pressed_virtual_keys.clear();
        self.was_pressed_mouse_buttons.clear();

        self.was_released_scancodes.clear();
        self.was_released_virtual_keys.clear();
        self.was_released_mouse_buttons.clear();

        self.was_triggered_mouse_scroll.clear();

        self.mouse_scroll_events.clear();
        self.mouse_move_events.clear();
        self.raw_mouse_delta = (0.0, 0.0);
        self.raw_scroll_delta = (0.0, 0.0);

        self.input_chars.clear();
    }

    pub(crate) fn process_event(&mut self, event: InputEvent, app: &mut A, window_manager: &mut WindowManager, renderer: &mut Renderer) {
        match event {
            InputEvent::Keyboard { window_id, input, is_synthetic } => {
                let p = self.current_mouse_position.filter(|(w, _)| w == &window_id).map(|(_, p)| p);

                match input.state {
                    ElementState::Pressed => {

                        let is_repeat = !self.is_pressed_scancodes.entry(window_id).or_default().insert(input.scancode);
                        let extra = ExtraButtonInfo { is_repeat, is_synthetic };

                        self.was_pressed_scancodes.entry(window_id).or_default().entry(input.scancode).or_default()
                            .push((self.current_modifiers, p, extra));

                        if let Some(scancodes) = self.pressed_scancode_callback.get_mut(&window_id) {
                            if let Some(callbacks) = scancodes.get_mut(&input.scancode) {
                                for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                    let callback_info = CallbackInfo {
                                        window_manager,
                                        renderer,
                                        mouse_position: p,
                                        is_repeat,
                                        is_synthetic
                                    };

                                    callback(app, callback_info);
                                }
                            }
                        }
                    }
                    ElementState::Released => {
                        let extra = ExtraButtonInfo { is_repeat: false, is_synthetic };

                        self.is_pressed_scancodes.get_mut(&window_id).map(|v| v.remove(&input.scancode));
                        self.was_released_scancodes.entry(window_id).or_default().entry(input.scancode).or_default()
                            .push((self.current_modifiers, p, extra));

                        if let Some(scancodes) = self.released_scancode_callback.get_mut(&window_id) {
                            if let Some(callbacks) = scancodes.get_mut(&input.scancode) {
                                for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                    let callback_info = CallbackInfo {
                                        window_manager,
                                        renderer,
                                        mouse_position: p,
                                        is_repeat: false,
                                        is_synthetic
                                    };

                                    callback(app, callback_info);
                                }
                            }
                        }
                    }
                }
                if let Some(vk) = input.virtual_keycode {
                    match input.state {
                        ElementState::Pressed => {
                            let is_repeat = !self.is_pressed_virtual_keys.entry(window_id).or_default().insert(vk);
                            let extra = ExtraButtonInfo { is_repeat, is_synthetic };

                            self.was_pressed_virtual_keys.entry(window_id).or_default().entry(vk).or_default()
                                .push((self.current_modifiers, p, extra));

                            if let Some(vks) = self.pressed_virtual_keys_callback.get_mut(&window_id) {
                                if let Some(callbacks) = vks.get_mut(&vk) {
                                    for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                        let callback_info = CallbackInfo {
                                            window_manager,
                                            renderer,
                                            mouse_position: p,
                                            is_repeat,
                                            is_synthetic
                                        };

                                        callback(app, callback_info);
                                    }
                                }
                            }
                        }
                        ElementState::Released => {
                            let extra = ExtraButtonInfo { is_repeat: false, is_synthetic };

                            self.is_pressed_virtual_keys.get_mut(&window_id).map(|v| v.remove(&vk));
                            self.was_released_virtual_keys.entry(window_id).or_default().entry(vk).or_default()
                                .push((self.current_modifiers, p, extra));

                            if let Some(vks) = self.released_virtual_keys_callback.get_mut(&window_id) {
                                if let Some(callbacks) = vks.get_mut(&vk) {
                                    for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                        let callback_info = CallbackInfo {
                                            window_manager,
                                            renderer,
                                            mouse_position: p,
                                            is_repeat: false,
                                            is_synthetic
                                        };

                                        callback(app, callback_info);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            InputEvent::ModifiersChanged { window_id, modifiers_state } => {
                self.current_modifiers = modifiers_state;
            }
            InputEvent::CursorMoved { window_id, position } => {
                if let Some((old_w, old_p)) = self.current_mouse_position {
                    if old_w == window_id {
                        self.mouse_move_events.entry(window_id).or_default().push(MouseMovementEvent {
                            old_position: old_p,
                            new_position: position,
                            current_modifiers: self.current_modifiers
                        });
                    }
                }
                self.current_mouse_position = Some((window_id, position))
            }
            InputEvent::CursorEntered { .. } => {}
            InputEvent::CursorLeft { window_id } => {
                if let Some((w, _)) = self.current_mouse_position {
                    if w == window_id {
                        self.current_mouse_position = None;
                    }
                }
            }
            InputEvent::MouseWheel { window_id, delta } => {
                if let Some((w, p)) = self.current_mouse_position {
                    if w == window_id {
                        self.mouse_scroll_events.entry(window_id).or_default().push(MouseScrollEvent {
                            scroll: delta,
                            mouse_position: p,
                            current_modifiers: self.current_modifiers
                        });
                    }
                }


                let (dx, dy) = match delta {
                    MouseScrollDelta::LineDelta(x, y) => {
                        (x, y)
                    }
                    MouseScrollDelta::PixelDelta(d) => {
                        (d.x as f32, d.y as f32)
                    }
                };

                self.raw_scroll_delta.0 += dx;
                self.raw_scroll_delta.1 += dy;

                let dir = if dx < 0.0 {
                    if dy < 0.0 {
                        MouseScrollDirection::DownLeft
                    } else if dy == 0.0 {
                        MouseScrollDirection::Left
                    } else {
                        MouseScrollDirection::UpLeft
                    }
                } else if dx == 0.0 {
                    if dy < 0.0 {
                        MouseScrollDirection::Down
                    } else if dy == 0.0 {
                        unreachable!()
                    } else {
                        MouseScrollDirection::Up
                    }
                } else {
                    if dy < 0.0 {
                        MouseScrollDirection::DownRight
                    } else if dy == 0.0 {
                        MouseScrollDirection::Right
                    } else {
                        MouseScrollDirection::UpRight
                    }
                };

                let p = self.current_mouse_position.filter(|(w, _)| w == &window_id).map(|(_, p)| p);
                let extra = ExtraButtonInfo { is_repeat: false, is_synthetic: false };

                self.was_triggered_mouse_scroll.entry(window_id).or_default().entry(dir).or_default()
                    .push((self.current_modifiers, p, extra));

                if let Some(directions) = self.triggered_mouse_scroll_callback.get_mut(&window_id) {
                    if let Some(callbacks) = directions.get_mut(&dir) {
                        for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                            let callback_info = CallbackInfo {
                                window_manager,
                                renderer,
                                mouse_position: p,
                                is_repeat: false,
                                is_synthetic: false
                            };

                            callback(app, callback_info);
                        }
                    }
                }
            }
            InputEvent::MouseButton { window_id, state, button } => {
                let p = self.current_mouse_position.filter(|(w, _)| w == &window_id).map(|(_, p)| p);
                match state {
                    ElementState::Pressed => {
                        self.is_pressed_mouse_buttons.entry(window_id).or_default().insert(button);
                        let extra = ExtraButtonInfo { is_repeat: false, is_synthetic: false };

                        self.was_pressed_mouse_buttons.entry(window_id).or_default().entry(button).or_default()
                            .push((self.current_modifiers, p, extra));

                        if let Some(buttons) = self.pressed_mouse_buttons_callback.get_mut(&window_id) {
                            if let Some(callbacks) = buttons.get_mut(&button) {
                                for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                    let callback_info = CallbackInfo {
                                        window_manager,
                                        renderer,
                                        mouse_position: p,
                                        is_repeat: false,
                                        is_synthetic: false
                                    };

                                    callback(app, callback_info);
                                }
                            }
                        }
                    }
                    ElementState::Released => {
                        self.is_pressed_mouse_buttons.get_mut(&window_id).map(|v| v.remove(&button));
                        let extra = ExtraButtonInfo { is_repeat: false, is_synthetic: false };

                        self.was_released_mouse_buttons.entry(window_id).or_default().entry(button).or_default()
                            .push((self.current_modifiers, p, extra));

                        if let Some(buttons) = self.released_mouse_buttons_callback.get_mut(&window_id) {
                            if let Some(callbacks) = buttons.get_mut(&button) {
                                for (_, callback) in callbacks.values_mut().filter(|(mods, _)| Self::modifiers_condition_met(self.current_modifiers, *mods)) {

                                    let callback_info = CallbackInfo {
                                        window_manager,
                                        renderer,
                                        mouse_position: p,
                                        is_repeat: false,
                                        is_synthetic: false
                                    };

                                    callback(app, callback_info);
                                }
                            }
                        }
                    }
                }
            }
            InputEvent::ReceivedCharacter { window_id, c } => {
                self.input_chars.entry(window_id).or_default().push(c)
            }
            InputEvent::RawMouseMoved { delta } => {
                self.raw_mouse_delta.0 += delta.0;
                self.raw_mouse_delta.1 += delta.1;
            }
        }
    }

    pub(crate) fn end_process_events(&mut self) {
        for (w, chars) in &self.input_chars {
            if let Some(streams) = self.char_streams.get(w) {
                for stream in streams.values() {
                    stream.send(chars.clone()).unwrap();
                }
            }
        }
    }
}