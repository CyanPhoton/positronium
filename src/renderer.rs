mod instance;
mod surface;
mod device;
mod swapchain;
mod queues;
mod render_pass;
mod graphics_pipeline;
mod framebuffer;
mod commands;
mod scene;
mod shaders;
mod memory;
pub mod resources;

use std::cell::RefCell;
use std::ops::DerefMut;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;
use flume::RecvError;
use parking_lot::RwLock;
use winit::dpi::PhysicalSize;
use instance::Instance;
use surface::Surface;
use device::Device;
use queues::QueueManager;
use swapchain::{Swapchain, AcquiredResult, PresentResult, InFlightHandle};
use render_pass::RenderPass;
use graphics_pipeline::GraphicsPipeline;
use framebuffer::Framebuffer;
use commands::{CommandManager, CommandBuffer};
use scene::Scene;
use crate::renderer::resources::{DeviceResourceManager, DeviceResourceManagerRef};
use crate::renderer::shaders::Vertex;

pub use crate::renderer::scene::SceneData;
pub use crate::renderer::shaders::basic::Vertex as BasicVertex;
pub use crate::renderer::shaders::basic::BasicShader as BasicShader;
use crate::utility::{self, replace_with, vec_retain_take};

use crate::Version;
use crate::window_manager::{WindowManager, WindowRef};
use crate::winit::window::WindowId;

pub struct RendererSettings {
    pub support_windowing: bool,
    pub use_validation_layers: bool,
}

impl Default for RendererSettings {
    fn default() -> Self {
        RendererSettings {
            support_windowing: true,
            #[cfg(debug_assertions)]
            use_validation_layers: true,
            #[cfg(not(debug_assertions))]
            use_validation_layers: false,
        }
    }
}

#[derive(Copy, Clone)]
pub struct RenderTargetSettings {
    pub swapchain_settings: SwapchainSettings,
}

impl Default for RenderTargetSettings {
    fn default() -> Self {
        RenderTargetSettings {
            swapchain_settings: Default::default()
        }
    }
}

#[derive(Copy, Clone)]
pub struct SwapchainSettings {
    /// Acquiring will block if `max_frames_in_flight` renders have not finished, until one has (or until timeout?)
    /// Gets clamped to [1, (max_images - min_images + 1)] (no upper limit if max_images = 0), since only being able to have 0 in flight
    /// is not handled, upper is because because can only guarantee that vkAcquireNextImageKHR will return in finite
    /// time if "..the number of images that the application has currently acquired is less or equal than the difference between
    ///   the number of images in swapchain and the value of VkSurfaceCapabilitiesKHR::minImageCount"
    pub max_frames_in_flight: u32,
    /// A number of additional images to request (will cap at max_images), with allows continuing to render and present images
    /// while recreating swapchain and everything else that depends on it.
    pub requested_additional_images: u32,
}

impl Default for SwapchainSettings {
    fn default() -> Self {
        SwapchainSettings {
            max_frames_in_flight: 2,
            requested_additional_images: 0,
        }
    }
}

pub struct Renderer {
    instance: Instance,
}

pub struct RenderTarget {
    old_data_thread: Option<JoinHandle<()>>,
    old_data_sender: flume::Sender<Option<OldData>>,
    command_buffer_cleanup_receiver: flume::Receiver<Vec<CommandBuffer>>,
    on_resize_sender: flume::Sender<BlockingWindowResizeData>,
    //
    settings: RenderTargetSettings,
    target_window: WindowId,
    recreate_swapchain: bool,
    swapchain_was_recreated: bool,
    current_size: PhysicalSize<u32>,
    device: Arc<Device>,
    device_resource_manager: DeviceResourceManager,
    queue_manager: QueueManager,
    command_manager: CommandManager,
    surface: Surface,
    swapchain: Swapchain,
    scene: Scene,
}

impl Drop for RenderTarget {
    fn drop(&mut self) {
        // Preemptively wait
        self.swapchain.wait_finished();

        self.on_resize_sender.try_send(BlockingWindowResizeData::Quit).ok();

        // Send 'Quit' to OldData thread
        // Quit old data first, so it can use the update_thread to cleanup command buffers
        // Though if update_thread does stop first, the command pool and thus command buffers should be freed anyway.
        self.old_data_sender.send(None).ok();
        self.old_data_thread.take().map(|thread| thread.join());
    }
}

struct BlockingWindowResizeContext {
    on_resize: flume::Receiver<BlockingWindowResizeData>
}

enum BlockingWindowResizeData {
    Data {
        new_size: PhysicalSize<u32>,
        inflight_handle: Option<InFlightHandle>
    },
    Quit
}

struct OldDataThreadContext {
    old_data_receiver: flume::Receiver<Option<OldData>>,
    old_data: Vec<OldData>,
    command_buffer_cleanup_sender: flume::Sender<Vec<CommandBuffer>>,
}

struct OldData {
    inflight_renders: Vec<InFlightHandle>,
    #[allow(dead_code)] // This is just for keep alive
    old_swapchain: Option<Swapchain>,
    old_scene: Scene,
}

pub trait RenderTargetable {}

impl Renderer {
    pub fn new(application_name: &str, application_version: Version, settings: RendererSettings) -> Renderer {
        Renderer {
            instance: Instance::new(application_name, application_version, settings)
        }
    }

    pub fn create_render_target(&self, window_manager: &WindowManager, target_window: &WindowRef, settings: RenderTargetSettings) -> RenderTarget {
        let mut surface = Surface::new(&self.instance, target_window);

        let device = Arc::new(Device::new(&self.instance, &surface));
        let queue_manager = QueueManager::new(device.as_ref());
        let mut command_manager = CommandManager::new(device.as_ref(), &queue_manager);
        let window_size = window_manager.get_window_size(target_window);
        let swapchain = Swapchain::new(device.clone(), queue_manager.presentation_queue.clone(), &mut surface, window_size, None, &settings.swapchain_settings);

        let device_resource_manager = DeviceResourceManager::new(device.clone());
        let scene = Scene::new(device.as_ref(), &swapchain, &mut command_manager, device_resource_manager.get_ref(), SceneData::new(device.clone()));

        let (old_data_sender, old_data_receiver) = flume::unbounded();
        let (command_buffer_cleanup_sender, command_buffer_cleanup_receiver) = flume::unbounded();
        let (on_resize_sender, on_resize) = flume::bounded(0);

        let old_data_thread = OldDataThreadContext {
            old_data_receiver,
            old_data: vec![],
            command_buffer_cleanup_sender
        }.create_old_data_thread();

        let blocking_window_resize_context = BlockingWindowResizeContext {
            on_resize
        };
        window_manager.set_blocking_resize_callback(target_window, move |size| blocking_window_resize_context.resize(size));

        RenderTarget {
            old_data_thread: Some(old_data_thread),
            old_data_sender,
            command_buffer_cleanup_receiver,
            on_resize_sender,
            settings,
            target_window: target_window.window_id,
            recreate_swapchain: false,
            swapchain_was_recreated: false,
            current_size: window_size,
            device,
            device_resource_manager,
            queue_manager,
            command_manager,
            surface,
            swapchain,
            scene
        }
    }
}

impl BlockingWindowResizeContext {
    pub fn resize(&self, size: PhysicalSize<u32>) {
        loop {
            match self.on_resize.recv() {
                Ok(BlockingWindowResizeData::Data { new_size, inflight_handle }) => {
                    if new_size != size {
                        continue;
                    }
                    if let Some(handle) = inflight_handle {
                        handle.wait_finished();
                        break;
                    }
                }
                Ok(BlockingWindowResizeData::Quit) | Err(_) => {
                    break;
                },
            }
        }
    }
}

impl OldDataThreadContext {
    pub fn create_old_data_thread(mut self) -> JoinHandle<()> {
        thread::Builder::new().name("Old Data Thread".to_string()).spawn(move || {
            loop {
                if self.old_data.is_empty() {
                    // No work to do, so block till there is
                    if let Some(old_data) = self.old_data_receiver.recv().unwrap() {
                        old_data.old_swapchain.as_ref().map(|old_swapchain| old_swapchain.quit());
                        self.old_data.push(old_data);
                    } else {
                        // Sending a None is used to signal thread should exit
                        break;
                    }
                } else {
                    match self.old_data_receiver.try_recv() {
                        Ok(Some(old_data)) => {
                            old_data.old_swapchain.as_ref().map(|old_swapchain| old_swapchain.quit());
                            self.old_data.push(old_data);
                        },
                        Ok(None) => {
                            // Can freely just forget command buffers since command pool has dropped
                            break;
                        },
                        Err(_) => {}
                    }

                    for mut old in self.old_data.drain(..) {
                        for f in old.inflight_renders.drain(..) {
                            f.wait_finished();
                        }
                    }
                }
            }
        }).unwrap()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash)]
pub enum RenderResult {
    Success,
    Timeout,
    Updating,
}

impl RenderTarget {
    pub fn render(&mut self, window_manager: &WindowManager, target_window: &WindowRef, acquire_timeout: Option<Duration>) -> RenderResult {
        assert_eq!(self.target_window, target_window.window_id, "Must render to the same window as was created with");

        self.check_cleanup();
        self.check_update(window_manager, target_window);

        let mut acquired = match self.swapchain.acquire_image(acquire_timeout) {
            AcquiredResult::Success { acquired_image, request_update } => {
                self.recreate_swapchain = self.recreate_swapchain || request_update;
                acquired_image
            }
            AcquiredResult::RequireUpdate => {
                self.recreate_swapchain = true;
                return self.render(window_manager, target_window, None);
            }
            AcquiredResult::Timeout => {
                return RenderResult::Timeout;
            }
        };

        self.scene.render(&mut acquired, self.queue_manager.graphics_queue.write().deref_mut());
        self.swapchain.present_image(acquired);

        if self.swapchain_was_recreated {
            // signal blocking callback once render done
            self.on_resize_sender.try_send(BlockingWindowResizeData::Data {
                new_size: self.current_size,
                inflight_handle: self.swapchain.get_inflight_handles().last().cloned()
            }).ok();
            self.swapchain_was_recreated = false;
        }

        RenderResult::Success
    }

    fn check_cleanup(&mut self) {
        let cmd = self.command_buffer_cleanup_receiver.try_iter().flatten().map(|c| c.command);
        self.command_manager.graphics_pool.command_pool.free_command_buffers(cmd);
    }

    fn check_update(&mut self, window_manager: &WindowManager, target_window: &WindowRef) {
        let size = window_manager.get_window_size(target_window);
        if size != self.current_size || self.recreate_swapchain {
            self.update_swapchain(size);
        }
    }

    fn update_swapchain(&mut self, size: PhysicalSize<u32>) {
        self.recreate_swapchain = false;
        self.swapchain_was_recreated = true;
        self.current_size = size;

        utility::replace_with(&mut self.swapchain, |old_swapchain| {
            let inflight_renders = old_swapchain.get_inflight_handles();
            let (old_swapchain_ref, old_swapchain) = old_swapchain.into_old();
            let swapchain = Swapchain::new(self.device.clone(), self.queue_manager.presentation_queue.clone(), &mut self.surface, size, Some(old_swapchain_ref), &self.settings.swapchain_settings);

            utility::replace_with(&mut self.scene, |mut old_scene| {
                let scene = Scene::new(self.device.as_ref(), &swapchain, &mut self.command_manager, old_scene.resource_ref.clone(), std::mem::replace(&mut old_scene.scene_data, SceneData::new(self.device.clone())));

                self.old_data_sender.send(Some(OldData {
                    inflight_renders,
                    old_swapchain: Some(old_swapchain),
                    old_scene
                })).unwrap();

                scene
            });

            swapchain
        });
    }

    pub fn create_scene_data(&self) -> SceneData {
        SceneData::new(self.device.clone())
    }

    pub fn set_scene_data(&mut self, scene_data: SceneData) -> SceneData {
        utility::take_replace(&mut self.scene, |mut old_scene| {
            let old_data = std::mem::replace(&mut old_scene.scene_data, SceneData::new(self.device.clone()));
            self.old_data_sender.send(Some(OldData {
                inflight_renders: self.swapchain.get_inflight_handles(),
                old_swapchain: None,
                old_scene
            })).unwrap();

            old_data
        }, || {
            Scene::new(self.device.as_ref(), &self.swapchain, &mut self.command_manager, self.device_resource_manager.get_ref(), scene_data)
        })
    }

    pub fn update_scene_data<F: FnOnce(SceneData) -> SceneData>(&mut self, update: F) {
        utility::replace_with(&mut self.scene, |mut old_scene| {
            let old_data = std::mem::replace(&mut old_scene.scene_data, SceneData::new(self.device.clone()));
            self.old_data_sender.send(Some(OldData {
                inflight_renders: self.swapchain.get_inflight_handles(),
                old_swapchain: None,
                old_scene
            })).unwrap();

            let scene_data = update(old_data);

            Scene::new(self.device.as_ref(), &self.swapchain, &mut self.command_manager, self.device_resource_manager.get_ref(), scene_data)
        });
    }
}