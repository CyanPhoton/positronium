use std::any::TypeId;
use std::sync::Arc;
use fxhash::FxHashMap;
use positronium_vk as vk;
use crate::renderer::queues::QueueManager;
use crate::renderer::RenderTarget;
use crate::renderer::resources::{DeviceIndexRegion, DeviceModelHandle, DeviceModelHandleType, DeviceResourceManagerRef, DeviceVertexRegion, IndexData, RangeInfo};
use crate::renderer::shaders::basic::BasicShader;
use crate::renderer::shaders::hello_triangle::HelloTriangleShader;
use crate::renderer::shaders::{GenericShader, GenericVertex, GenericVertexWrapper, Shader, ShaderID, Vertex};
use crate::renderer::swapchain::{AcquiredImageInfo, Swapchain};
use super::{Device, RenderPass, GraphicsPipeline, Framebuffer, CommandManager, CommandBuffer};

pub struct Scene {
    pub render_pass: RenderPass,
    pub hello_triangle_graphics_pipeline: Option<GraphicsPipeline>,
    pub render_graphics_pipelines: FxHashMap<ShaderID, GraphicsPipeline>,
    pub framebuffers: Vec<Framebuffer>,
    pub command_buffers: Vec<CommandBuffer>,
    pub resource_ref: DeviceResourceManagerRef,
    pub scene_data: SceneData,
}

struct MeshCollection {
    vertex_description: Box<dyn GenericVertex>,
    meshes: Vec<DeviceVertexRegion>,
    u16_indexed_meshes: Vec<(DeviceVertexRegion, DeviceIndexRegion)>,
    u32_indexed_meshes: Vec<(DeviceVertexRegion, DeviceIndexRegion)>,
}

impl MeshCollection {
    pub fn add_model<V: Vertex>(&mut self, model: DeviceModelHandle<V>) {
        match model.ty {
            DeviceModelHandleType::NonIndexed { vertices } => self.meshes.push(vertices),
            DeviceModelHandleType::U16Indexed { vertices, indices } => self.u16_indexed_meshes.push((vertices, indices)),
            DeviceModelHandleType::U32Indexed { vertices, indices } => self.u32_indexed_meshes.push((vertices, indices)),
        }
    }
}

impl MeshCollection {
    fn new<V: Vertex>() -> Self {
        Self {
            vertex_description: Box::new(GenericVertexWrapper::<V>::default()),
            meshes: Default::default(),
            u16_indexed_meshes: Default::default(),
            u32_indexed_meshes: Default::default()
        }
    }
}

pub struct SceneData {
    device: Arc<Device>,
    pub hello_world: bool,
    rendered_models: FxHashMap<ShaderID, (Box<dyn GenericShader>, MeshCollection)>
}

impl SceneData {
    pub fn new(device: Arc<Device>) -> Self {
        Self {
            device,
            hello_world: false,
            rendered_models: Default::default()
        }
    }

    pub fn add_model<S: 'static + Shader>(&mut self, model: DeviceModelHandle<S::Vertex>) {
        self.rendered_models.entry(S::id()).or_insert_with(|| {
            (Box::new(S::new(self.device.as_ref())), MeshCollection::new::<S::Vertex>())
        }).1.add_model(model);
    }
}

impl Scene {
    pub fn new(device: &Device, swapchain: &Swapchain, command_manager: &mut CommandManager, resource_ref: DeviceResourceManagerRef, scene_data: SceneData) -> Scene {
        let render_pass = RenderPass::new(device, swapchain);

        let hello_triangle_graphics_pipeline = scene_data.hello_world.then(|| {
            let shader = HelloTriangleShader::new(device);
            let empty_vertex = GenericVertexWrapper::<()>::default();
            GraphicsPipeline::new(device, swapchain, &render_pass, &shader, &empty_vertex as &dyn GenericVertex)
        });

        let render_graphics_pipelines = scene_data.rendered_models.iter()
            .map(|(shader_id, (shader, v))| (*shader_id, GraphicsPipeline::new(device, swapchain, &render_pass, shader.as_ref(), v.vertex_description.as_ref())))
            .collect::<FxHashMap<_, _>>();

        let framebuffers: Vec<_> = swapchain.image_views.iter().map(|i| {
            Framebuffer::new(device, &render_pass, &[i.as_handle()], swapchain.extent)
        }).collect();

        let mut command_buffers = command_manager.graphics_pool.allocate_command_buffers(framebuffers.len() as u32);

        let clear_values = [
            vk::ClearValue::Colour(
                vk::ClearColourValue::Float32(
                    [0.0, 0.0, 0.0, 1.0]
                ).into_raw()
            ).into_raw()
        ];

        let mut render_pass_info = vk::RenderPassBeginInfo {
            render_pass: render_pass.render_pass.as_handle(),
            framebuffer: vk::Handle::null(),
            render_area: vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: swapchain.extent,
            },
            clear_values: &clear_values[..],
        }.into_raw();

        for (cmd, fb) in command_buffers.iter_mut().zip(&framebuffers) {
            render_pass_info.framebuffer = fb.framebuffer.as_handle();

            cmd.record(&mut command_manager.graphics_pool, |mut cmd| {
                cmd.begin_render_pass(&render_pass_info, vk::SubpassContents::eInline);

                if let Some(graphics_pipeline) = &hello_triangle_graphics_pipeline {
                    cmd.bind_pipeline(vk::PipelineBindPoint::eGraphics, &graphics_pipeline.pipeline);
                    cmd.draw(3, 1, 0, 0);
                }

                for (shader_id, graphics_pipeline) in &render_graphics_pipelines {
                    cmd.bind_pipeline(vk::PipelineBindPoint::eGraphics, &graphics_pipeline.pipeline);

                    let (shader, meshes) = &scene_data.rendered_models[shader_id];

                    let vertex_buffer = resource_ref.vertex_buffers()[&shader.vertex_id()].buffer.as_handle();
                    cmd.bind_vertex_buffers(0, &[vertex_buffer], &[0]);

                    for mesh in &meshes.meshes {
                        let RangeInfo { count: vertex_count, first: first_vertex } = mesh.range_info();

                        cmd.draw(vertex_count as u32, 1, first_vertex as u32, 0);
                    }

                    if let Some(index_buffer) = resource_ref.u16_index_buffer() {
                        if !meshes.u16_indexed_meshes.is_empty() {
                            cmd.bind_index_buffer(&index_buffer.buffer.as_ref(), 0, vk::IndexType::eUint16);

                            for (vertex, index) in &meshes.u16_indexed_meshes {
                                let RangeInfo { first: vertex_offset, .. } = vertex.range_info();
                                let RangeInfo { count: index_count, first: first_index } = index.range_info();

                                cmd.draw_indexed(index_count as u32, 1, first_index as u32, vertex_offset as i32, 0);
                            }
                        }
                    }

                    if let Some(index_buffer) = resource_ref.u32_index_buffer() {
                        if !meshes.u32_indexed_meshes.is_empty() {
                            cmd.bind_index_buffer(&index_buffer.buffer.as_ref(), 0, vk::IndexType::eUint32);

                            for (vertex, index) in &meshes.u32_indexed_meshes {
                                let RangeInfo { first: vertex_offset, .. } = vertex.range_info();
                                let RangeInfo { count: index_count, first: first_index } = index.range_info();

                                cmd.draw_indexed(index_count as u32, 1, first_index as u32, vertex_offset as i32, 0);
                            }
                        }
                    }
                }

                cmd.end_render_pass();

                Ok(())
            });
        }

        Scene {
            render_pass,
            hello_triangle_graphics_pipeline,
            render_graphics_pipelines,
            framebuffers,
            command_buffers,
            resource_ref,
            scene_data,
        }
    }

    pub fn render(&self, acquired: &mut AcquiredImageInfo, graphics_queue: &mut vk::Queue) {
        let wait_semaphores = [acquired.image_ready.as_handle()];

        let wait_dst_stage_masks = [vk::PipelineStageFlags::eColourAttachmentOutputBit];
        let command_buffers = [self.command_buffers[acquired.image_index as usize].command.as_handle()];

        let signal_semaphores = [acquired.render_finished_semaphore.as_handle()];

        let submit_infos = [positronium_vk::SubmitInfo {
            wait_semaphores: &wait_semaphores[..],
            wait_dst_stage_mask: &wait_dst_stage_masks[..],
            command_buffers: &command_buffers[..],
            signal_semaphores: &signal_semaphores[..],
        }.into_raw()];

        graphics_queue.queue_submit(&submit_infos[..], Some(&mut acquired.render_finished_fence))
            .expect("Failed to submit render to queue");
    }
}