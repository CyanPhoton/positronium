use std::collections::{HashMap, HashSet};
use positronium_vk as vk;
use smallvec::SmallVec;
use crate::renderer::queues::QueueFamilyIndices;
use super::instance::Instance;
use super::surface::Surface;

pub struct Device {
    pub physical_device: vk::PhysicalDevice,
    pub capabilities: DeviceCapabilities,
    pub device: vk::Device,
}

#[derive(Clone)]
pub struct DepthStencilFormatInfo {
    pub format: vk::Format,
    pub depth_bits: u8,
    pub stencil_bits: Option<u8>,
}

#[derive(Clone)]
pub struct DeviceCapabilities {
    pub name: String,
    pub supported_extensions: HashMap<vk::UnsizedCStr<'static>, bool>,
    pub is_discrete: bool,
    pub queue_families: QueueFamilyIndices,
    pub min_msaa_support: Vec<vk::SampleCountFlagBits>,
    pub supported_features: vk::PhysicalDeviceFeatures,
    pub supported_depth_formats: Vec<DepthStencilFormatInfo>,
}

impl Device {
    pub fn new(instance: &Instance, surface: &Surface) -> Device {
        let (physical_device, capabilities) = Self::pick_physical_device(instance, surface);

        let device = Self::create_device(instance, &physical_device, &capabilities);

        Device {
            physical_device,
            capabilities,
            device,
        }
    }

    fn pick_physical_device(instance: &Instance, surface: &Surface) -> (vk::PhysicalDevice, DeviceCapabilities) {
        let is_device_suitable = |(_, capabilities): &(vk::PhysicalDevice, DeviceCapabilities)| -> bool {
            capabilities.is_discrete
        };

        instance.instance.enumerate_physical_devices::<SmallVec<[_; 4]>>()
            .expect("Failed to enumerate physical devices").0.into_iter()
            .filter_map(|d| DeviceCapabilities::new(&d, surface).map(|c| (d, c)))
            .filter(is_device_suitable)
            .next()// TODO: Provide API for app/user selection
            .expect("Failed to find a suitable physical device")
    }

    fn create_device(instance: &Instance, physical_device: &vk::PhysicalDevice, capabilities: &DeviceCapabilities) -> vk::Device {
        let priority = [1.0; 1];

        let queue_create_infos: Vec<_> = capabilities.queue_families.unique_queues.iter().map(|i| {
            vk::DeviceQueueCreateInfo {
                flags: Default::default(),
                queue_family_index: *i,
                queue_priorities: &priority[..],
            }.into_raw()
        }).collect();

        let extensions: Vec<_> = capabilities.supported_extensions.iter().filter_map(|(e, s)| s.then(|| *e)).collect();

        let supported_features = capabilities.supported_features.into_raw();

        let create_info = vk::DeviceCreateInfo {
            flags: Default::default(),
            queue_create_infos: &queue_create_infos[..],
            enabled_layer_names: &instance.validation_layers[..],
            enabled_extension_names: &extensions[..],
            enabled_features: Some(&supported_features),
        }.into_raw();

        physical_device.create_device(&create_info)
            .expect("Failed to create logical device").0
    }
}

impl DeviceCapabilities {
    pub fn new(physical_device: &vk::PhysicalDevice, surface: &Surface) -> Option<DeviceCapabilities> {
        // Extensions
        let requested_extensions = Self::requested_extensions();
        let available_extensions = physical_device.enumerate_device_extension_properties::<SmallVec<[_; 32]>>(None)
            .expect("Failed to enumerate physical device extensions").0;
        let extension_names = available_extensions.iter().map(|e| vk::UnsizedCStr::from(e.extension_name.as_cstr().unwrap())).collect::<HashSet<_>>();

        let mut supported_extensions = HashMap::new();

        for (re, required) in &requested_extensions {
            let supported = extension_names.contains(re);

            if *required && !supported {
                return None;
            }

            supported_extensions.insert(*re, supported);
        }

        // Properties
        let properties = physical_device.get_physical_device_properties();

        let name = properties.device_name.to_string().expect("Invalid device name");
        let is_discrete = properties.device_type == vk::PhysicalDeviceType::eDiscreteGpu;
        let min_msaa_support = (properties.limits.framebuffer_colour_sample_counts & properties.limits.framebuffer_depth_sample_counts)
            .into_flags().map(|v| v.expect("Received invalid bitfield")).collect();

        // Features
        let (optional_features, required_features) = Self::requested_features();
        let available_features = physical_device.get_physical_device_features();

        let mut features = [false; vk::PhysicalDeviceFeatures::NUMBER_OF_BOOLS];
        for i in 0..vk::PhysicalDeviceFeatures::NUMBER_OF_BOOLS {
            if required_features.bool_slice()[i] && !available_features.bool_slice()[i] {
                return None;
            }
            if available_features.bool_slice()[i] && (required_features.bool_slice()[i] || optional_features.bool_slice()[i]) {
                features[i] = true;
            }
        }
        let supported_features = vk::PhysicalDeviceFeatures::from_array(features);

        // Queue families
        let queue_families = QueueFamilyIndices::init(physical_device, surface)?;

        // Depth formats
        //vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint, vk::Format::eD16
        let supported_depth_formats = Self::depth_stencil_options().into_iter().filter(|f| {
            let properties = physical_device.get_physical_device_format_properties(f.format);

            properties.optimal_tiling_features & vk::FormatFeatureFlags::eDepthStencilAttachmentBit == vk::FormatFeatureFlags::eDepthStencilAttachmentBit
        }).collect();

        // {
        //     println!("{:?}: mem", properties.device_name.to_string());
        //     let prop = physical_device.get_physical_device_memory_properties();
        //     for i in 0..prop.memory_heap_count {
        //         println!("\theap: {:?}", prop.memory_heaps[i as usize]);
        //     }
        //     for i in 0..prop.memory_type_count {
        //         println!("\ttype: {:?}", prop.memory_types[i as usize]);
        //     }
        // }

        Some(DeviceCapabilities {
            name,
            supported_extensions,
            is_discrete,
            queue_families,
            min_msaa_support,
            supported_features,
            supported_depth_formats,
        })
    }

    /// Returns Vec<(name, required)>
    fn requested_extensions() -> Vec<(vk::UnsizedCStr<'static>, bool)> {
        vec![
            (vk::KHR_SWAPCHAIN_EXTENSION_NAME_UNSIZED_CSTR, true)
        ]
    }

    /// Returns (optional, required)
    fn requested_features() -> (vk::PhysicalDeviceFeatures, vk::PhysicalDeviceFeatures) {
        let mut optional = vk::PhysicalDeviceFeatures::all_false();
        let required = vk::PhysicalDeviceFeatures::all_false();

        optional.sampler_anisotropy = true;
        optional.sample_rate_shading = true;

        (optional, required)
    }

    fn depth_stencil_options() -> Vec<DepthStencilFormatInfo> {
        vec![
            DepthStencilFormatInfo {
                format: vk::Format::eD32Sfloat,
                depth_bits: 32,
                stencil_bits: None,
            },
            DepthStencilFormatInfo {
                format: vk::Format::eD32SfloatS8Uint,
                depth_bits: 32,
                stencil_bits: Some(8),
            },
            DepthStencilFormatInfo {
                format: vk::Format::eD24UnormS8Uint,
                depth_bits: 24,
                stencil_bits: Some(8),
            },
            DepthStencilFormatInfo {
                format: vk::Format::eX8D24UnormPack32,
                depth_bits: 24,
                stencil_bits: None,
            },
            DepthStencilFormatInfo {
                format: vk::Format::eD16Unorm,
                depth_bits: 16,
                stencil_bits: None,
            },
            DepthStencilFormatInfo {
                format: vk::Format::eD16UnormS8Uint,
                depth_bits: 16,
                stencil_bits: Some(8),
            },
        ]
    }
}