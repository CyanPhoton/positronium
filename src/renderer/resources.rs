use std::any::{Any, TypeId};
use std::borrow::BorrowMut;
use std::marker::PhantomData;
use std::ops::DerefMut;
use positronium_vk as vk;

use std::sync::Arc;
use fxhash::FxHashMap;
use flume;
use crate::renderer::device::Device;
use crate::renderer::memory::allocators::host_local::{HostLocalMemory, HostLocalMemoryRef};
use crate::renderer::memory::collections::dynamic_vec::{self, AllocationMode, StableRegionVec, GeneralStableRegionVec, Region};
use crate::renderer::memory::Memory;
use crate::renderer::RenderTarget;
use crate::renderer::shaders::{Vertex, VertexID};
use crate::utility;

pub enum IndexData {
    None,
    U16(Vec<u16>),
    U32(Vec<u32>),
}

impl IndexData {
    pub fn vk_type(&self) -> Option<vk::IndexType> {
        match self {
            IndexData::None => None,
            IndexData::U16(_) => Some(vk::IndexType::eUint16),
            IndexData::U32(_) => Some(vk::IndexType::eUint32),
        }
    }
}

pub struct ModelData<V: Vertex> {
    data: Vec<V>,
    indices: IndexData,
}

#[derive(Clone)]
pub struct DeviceVertexRegion(Arc<dynamic_vec::ManagedRegion>);
#[derive(Clone)]
pub struct DeviceIndexRegion(Arc<dynamic_vec::ManagedRegion>);

impl DeviceVertexRegion {
    pub fn new(v: dynamic_vec::ManagedRegion) -> Self {
        Self(Arc::new(v))
    }
}

impl DeviceIndexRegion {
    pub fn new(v: dynamic_vec::ManagedRegion) -> Self {
        Self(Arc::new(v))
    }
}

#[derive(Clone)]
pub enum DeviceModelHandleType {
    NonIndexed { vertices: DeviceVertexRegion },
    U16Indexed { vertices: DeviceVertexRegion, indices: DeviceIndexRegion },
    U32Indexed { vertices: DeviceVertexRegion, indices: DeviceIndexRegion },
}

#[derive(Clone)]
pub struct DeviceModelHandle<V: Vertex> {
    pub(crate) ty: DeviceModelHandleType,
    _phantom: PhantomData<V>
}

pub struct RangeInfo {
    pub count: usize,
    pub first: usize,
}

impl DeviceVertexRegion {
    pub fn range_info(&self) -> RangeInfo {
        RangeInfo {
            count: self.0.size(),
            first: self.0.offset(),
        }
    }
}

impl DeviceIndexRegion {
    pub fn range_info(&self) -> RangeInfo {
        RangeInfo {
            count: self.0.size(),
            first: self.0.offset(),
        }
    }
}

impl<V: Vertex> DeviceModelHandle<V> {
    pub fn vertex_range_info(&self) -> RangeInfo {
        match &self.ty {
            DeviceModelHandleType::NonIndexed { vertices }
            | DeviceModelHandleType::U16Indexed { vertices, .. }
            | DeviceModelHandleType::U32Indexed { vertices, .. } => {
                vertices.range_info()
            }
        }
    }

    pub fn index_range_info(&self) -> Option<RangeInfo> {
        match &self.ty {
            DeviceModelHandleType::NonIndexed { .. } => None,
            DeviceModelHandleType::U16Indexed { indices, .. } => Some(indices.range_info()),
            DeviceModelHandleType::U32Indexed { indices, .. } => Some(indices.range_info()),
        }
    }
}

impl<V: Vertex> ModelData<V> {
    pub fn new(data: Vec<V>, indices: IndexData) -> ModelData<V> {
        ModelData {
            data,
            indices,
        }
    }

    pub fn load_to_device(&self, render_target: &mut RenderTarget) -> DeviceModelHandle<V> where V: Copy {
        let vertex_buffer = render_target.device_resource_manager.get_vertex_buffer();
        let vertex_region = vertex_buffer.insert_slice(self.data.as_slice());
        let vertices = DeviceVertexRegion::new(vertex_buffer.promote_region(vertex_region));

        DeviceModelHandle {
            ty: match &self.indices {
                IndexData::None => DeviceModelHandleType::NonIndexed { vertices },
                IndexData::U16(indices) => {
                    let index_buffer = render_target.device_resource_manager.get_u16_index_buffer();
                    let index_region = index_buffer.insert_slice(indices.as_slice());
                    let indices = DeviceIndexRegion::new(index_buffer.promote_region(index_region));
                    DeviceModelHandleType::U16Indexed { vertices, indices }
                }
                IndexData::U32(indices) => {
                    let index_buffer = render_target.device_resource_manager.get_u32_index_buffer();
                    let index_region = index_buffer.insert_slice(indices.as_slice());
                    let indices = DeviceIndexRegion::new(index_buffer.promote_region(index_region));
                    DeviceModelHandleType::U32Indexed { vertices, indices }
                }
            },
            _phantom: Default::default()
        }
    }
}

pub struct DeviceResourceManager {
    device: Arc<Device>,
    vertex_buffers: FxHashMap<VertexID, Box<dyn GeneralStableRegionVec<HostLocalMemory>>>,
    u16_index_buffer: Option<StableRegionVec<u16, HostLocalMemory>>,
    u32_index_buffer: Option<StableRegionVec<u32, HostLocalMemory>>,
}

#[derive(Clone)]
pub struct DeviceResourceManagerRef {
    vertex_buffers: FxHashMap<VertexID, HostLocalMemoryRef>,
    // index_buffers: FxHashMap<TypeId, HostLocalMemoryRef>,
    u16_index_buffer: Option<HostLocalMemoryRef>,
    u32_index_buffer: Option<HostLocalMemoryRef>,
}

impl DeviceResourceManagerRef {
    pub fn vertex_buffers(&self) -> &FxHashMap<VertexID, HostLocalMemoryRef> {
        &self.vertex_buffers
    }

    pub fn u16_index_buffer(&self) -> Option<&HostLocalMemoryRef> {
        self.u16_index_buffer.as_ref()
    }

    pub fn u32_index_buffer(&self) -> Option<&HostLocalMemoryRef> {
        self.u32_index_buffer.as_ref()
    }
}

impl DeviceResourceManager {
    pub fn new(device: Arc<Device>) -> DeviceResourceManager {
        DeviceResourceManager {
            device,
            vertex_buffers: Default::default(),
            u16_index_buffer: None,
            u32_index_buffer: None,
        }
    }

    fn get_vertex_buffer<V: Vertex>(&mut self) -> &mut StableRegionVec<V, HostLocalMemory> {
        self.vertex_buffers.entry(V::id()).or_insert_with(||
            Box::new(Self::create_buffer::<V>(self.device.clone(), vk::BufferUsageFlags::eVertexBufferBit))
        ).monomorphism_mut().unwrap()
    }

    fn get_u16_index_buffer(&mut self) -> &mut StableRegionVec<u16, HostLocalMemory> {
        self.u16_index_buffer.get_or_insert_with(||
            Self::create_buffer::<u16>(self.device.clone(), vk::BufferUsageFlags::eIndexBufferBit)
        )
    }

    fn get_u32_index_buffer(&mut self) -> &mut StableRegionVec<u32, HostLocalMemory> {
        self.u32_index_buffer.get_or_insert_with(||
            Self::create_buffer::<u32>(self.device.clone(), vk::BufferUsageFlags::eIndexBufferBit)
        )
    }

    fn create_buffer<T>(device: Arc<Device>, usage: vk::BufferUsageFlags) -> StableRegionVec<T, HostLocalMemory> {
        let memory = HostLocalMemory::new(device, usage);
        let mut buffer = StableRegionVec::with_memory(memory);
        buffer.set_allocation_mode(AllocationMode::Exponential { factor: 2.0 });
        buffer
    }

    pub fn get_ref(&self) -> DeviceResourceManagerRef {
        DeviceResourceManagerRef {
            vertex_buffers: self.vertex_buffers.iter().map(|(k, v)| (*k, v.memory().get_ref())).collect(),
            u16_index_buffer: self.u16_index_buffer.as_ref().map(|b| b.memory().get_ref()),
            u32_index_buffer: self.u32_index_buffer.as_ref().map(|b| b.memory().get_ref()),
        }
    }
}