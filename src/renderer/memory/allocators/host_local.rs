use positronium_vk as vk;

use super::Device;
use std::sync::Arc;
use crate::utility;
use crate::renderer::memory;
use crate::renderer::memory::{CopyableMemory, Memory, ReadableMemory, WriteableMemory};

pub struct HostLocalMemory {
    device: Arc<Device>,
    usage: vk::BufferUsageFlags,
    buffer: Arc<vk::Buffer>,
    buffer_memory: Arc<vk::DeviceMemory>,
    buffer_size: vk::DeviceSize,
    mapped: vk::MappedMemory,
}

#[derive(Clone)]
pub struct HostLocalMemoryRef {
    pub buffer: Arc<vk::Buffer>,
    pub buffer_memory: Arc<vk::DeviceMemory>,
    pub buffer_size: vk::DeviceSize,
}

impl HostLocalMemory {
    const INIT_CAPACITY: vk::DeviceSize = 128;

    pub fn new(device: Arc<Device>, usage: vk::BufferUsageFlags) -> HostLocalMemory {
        Self::with_capacity(device, usage, Self::INIT_CAPACITY)
    }

    pub fn with_capacity(device: Arc<Device>, usage: vk::BufferUsageFlags, capacity: vk::DeviceSize) -> HostLocalMemory {
        let (buffer, mut buffer_memory, buffer_size) = super::allocate_memory(device.as_ref(), usage,
                                                                              vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit,
                                                                              vk::MemoryPropertyFlags::eHostCachedBit,
                                                                              vk::MemoryPropertyFlags::eDeviceLocalBit,
                                                                              capacity);

        let mapped = buffer_memory.map_memory(0, buffer_size, vk::MemoryMapFlags::eNone)
            .expect("Failed to map memory").0;

        HostLocalMemory {
            device,
            usage,
            buffer: Arc::new(buffer),
            buffer_memory: Arc::new(buffer_memory),
            buffer_size,
            mapped,
        }
    }
}

pub struct HostLocalMemoryInit {
    pub device: Arc<Device>,
    pub usage: vk::BufferUsageFlags
}

impl Memory for HostLocalMemory {
    type MemoryRef = HostLocalMemoryRef;
    type Init = HostLocalMemoryInit;

    fn new(init: HostLocalMemoryInit) -> Self {
        HostLocalMemory::new(init.device, init.usage)
    }

    fn with_capacity(init: HostLocalMemoryInit, bytes: usize) -> memory::Result<Self> {
        Ok(HostLocalMemory::with_capacity(init.device, init.usage, bytes as vk::DeviceSize))
    }

    fn capacity(&self) -> usize {
        self.buffer_size as usize
    }

    fn resize(&mut self, bytes: usize) -> memory::Result<()> {
        let mut new = Self::with_capacity(self.device.clone(), self.usage, bytes as vk::DeviceSize);

        let common_size = self.buffer_size.min(new.buffer_size) as usize;
        new.mapped[..common_size].copy_from_slice(&self.mapped[..common_size]);

        *self = new;
        Ok(())
    }

    fn get_ref(&self) -> Self::MemoryRef {
        HostLocalMemoryRef {
            buffer: self.buffer.clone(),
            buffer_memory: self.buffer_memory.clone(),
            buffer_size: self.buffer_size
        }
    }
}

impl ReadableMemory for HostLocalMemory {
    fn read<F: FnOnce(&[u8])>(&self, offset: usize, len: usize, f: F) {
        f(&self.mapped[offset..offset+len])
    }
}

impl CopyableMemory for HostLocalMemory  {
    fn copy_non_overlapping(&mut self, src_offset: usize, dst_offset: usize, len: usize) {
        assert!(!utility::is_overlapping(src_offset, dst_offset, len), "Ranges must not be overlapping");

        let (before, after) = self.mapped.split_at_mut(src_offset);
        let (source, after) = after.split_at_mut(len);

        if src_offset < dst_offset {
            // Dst in after
            let offset = dst_offset - (before.len() + source.len());
            after[offset..offset+len].copy_from_slice(source);
        } else {
            // Dst in before
            before[dst_offset..dst_offset+len].copy_from_slice(source);
        }
    }
}

impl WriteableMemory for HostLocalMemory {
    fn write<F: FnOnce(&mut [u8])>(&mut self, offset: usize, len: usize, f: F) {
        f(&mut self.mapped[offset..offset+len]);
    }
}