use std::ops::RangeInclusive;
use positronium_vk as vk;

use std::sync::Arc;
use crate::renderer::commands::CommandPool;
use crate::renderer::memory;
use crate::renderer::memory::{CopyableMemory, FlushableMemory, Memory, ReadableMemory, WriteableMemory};
use crate::utility::disjoint_buffer_map::{DisjointBufferMap, DisjointBufferSelfMap};
use crate::utility;
use crate::utility::disjoint_buffer_set::DisjointBufferSet;
use super::host_local::HostLocalMemory;
use super::Device;

pub enum StagingStrategy {
    Parity,
    Minimal { destroy_on_copy: bool },
}

enum StagingMemory {
    Parity {
        staged_memory: HostLocalMemory,
        // Set of ranges [usize, usize] for which need to flushed (to the same place) in device memory
        invalidated_regions: DisjointBufferSet,
    },
    Minimal {
        staged_memory: Option<HostLocalMemory>,
        // Destroy the staging buffer when flushing, to keep size minimal
        destroy_on_flush: bool,
        // Map a range from ranges in the device buffer to other ranges in the device buffer that need to be copied.
        // internal_copy_regions: DisjointBufferSelfMap,

        // Map a range from ranges in the staging buffer to ranges in the device buffer that need to be copied over.
        write_regions: DisjointBufferMap,
        // New size to either increase to or reduce to
        resize: Option<vk::DeviceSize>,
    },
}

pub struct DeviceLocalStagedMemory {
    device: Arc<Device>,
    usage: vk::BufferUsageFlags,
    device_buffer: Arc<vk::Buffer>,
    device_buffer_memory: Arc<vk::DeviceMemory>,
    device_buffer_size: vk::DeviceSize,
    staged_memory: StagingMemory,
}

#[derive(Clone)]
pub struct DeviceLocalMemoryRef {
    device_buffer: Arc<vk::Buffer>,
    device_buffer_memory: Arc<vk::DeviceMemory>,
    buffer_size: vk::DeviceSize,
    // Don't need to hold onto staging memory, since not used for rendering
}

impl DeviceLocalStagedMemory {
    const INIT_CAPACITY: vk::DeviceSize = 128;

    pub fn new(device: Arc<Device>, usage: vk::BufferUsageFlags, staging_strategy: StagingStrategy) -> DeviceLocalStagedMemory {
        Self::with_capacity(device, usage, Self::INIT_CAPACITY, staging_strategy)
    }

    pub fn with_capacity(device: Arc<Device>, usage: vk::BufferUsageFlags, capacity: vk::DeviceSize, staging_strategy: StagingStrategy) -> DeviceLocalStagedMemory {
        let (device_buffer, device_buffer_memory, buffer_size) = super::allocate_memory(device.as_ref(), usage,
                                                                                        vk::MemoryPropertyFlags::eDeviceLocalBit,
                                                                                        vk::MemoryPropertyFlags::eNone,
                                                                                        vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit | vk::MemoryPropertyFlags::eHostCachedBit,
                                                                                        capacity);

        let staged_memory = match staging_strategy {
            StagingStrategy::Parity => StagingMemory::Parity {
                staged_memory: HostLocalMemory::with_capacity(device.clone(), usage, buffer_size),
                invalidated_regions: Default::default(),
            },
            StagingStrategy::Minimal { destroy_on_copy } => StagingMemory::Minimal {
                staged_memory: None,
                destroy_on_flush: destroy_on_copy,
                write_regions: DisjointBufferMap::new(),
                // internal_copy_regions: DisjointBufferSelfMap::new(),
                resize: None,
            }
        };

        DeviceLocalStagedMemory {
            device,
            usage,
            device_buffer: Arc::new(device_buffer),
            device_buffer_memory: Arc::new(device_buffer_memory),
            device_buffer_size: buffer_size,
            staged_memory,
        }
    }

    /// Only really effects StagingStrategy::Minimal when destroy_on_copy is false
    pub fn shrink_staged(&mut self) {}
}

pub struct DeviceLocalStagedMemoryInit {
    device: Arc<Device>,
    usage: vk::BufferUsageFlags,
    staging_strategy: StagingStrategy,
}

impl Memory for DeviceLocalStagedMemory {
    type MemoryRef = DeviceLocalMemoryRef;
    type Init = DeviceLocalStagedMemoryInit;

    fn new(init: Self::Init) -> Self {
        DeviceLocalStagedMemory::new(init.device, init.usage, init.staging_strategy)
    }

    fn with_capacity(init: Self::Init, bytes: usize) -> memory::Result<Self> {
        Ok(DeviceLocalStagedMemory::with_capacity(init.device, init.usage, bytes as vk::DeviceSize, init.staging_strategy))
    }

    fn capacity(&self) -> usize {
        self.device_buffer_size as usize
    }

    fn resize(&mut self, bytes: usize) -> memory::Result<()> {
        match &mut self.staged_memory {
            StagingMemory::Parity { staged_memory, .. } => {
                staged_memory.resize(bytes)?;
            }
            StagingMemory::Minimal { resize, .. } => {
                *resize = Some(bytes as vk::DeviceSize);
            }
        }

        Ok(())
    }

    fn get_ref(&self) -> DeviceLocalMemoryRef {
        DeviceLocalMemoryRef {
            device_buffer: self.device_buffer.clone(),
            device_buffer_memory: self.device_buffer_memory.clone(),
            buffer_size: self.device_buffer_size,
        }
    }
}

impl WriteableMemory for DeviceLocalStagedMemory {
    fn write<F: FnOnce(&mut [u8])>(&mut self, offset: usize, len: usize, f: F) {
        match &mut self.staged_memory {
            StagingMemory::Parity { staged_memory, invalidated_regions } => {
                // Just directly write to staging buffer and invalidate the
                staged_memory.write(offset, len, f);
                invalidated_regions.insert_range(offset..offset + len);
            }
            StagingMemory::Minimal { staged_memory, destroy_on_flush: destroy_on_copy/*, internal_copy_regions*/, write_regions, resize } => {
                let staged_memory = staged_memory.get_or_insert_with(|| {
                    HostLocalMemory::with_capacity(self.device.clone(), self.usage, len as vk::DeviceSize)
                });

                if let Some(mapping) = write_regions.get_mapping_containing_dst_range(offset..offset + len) {
                    // There is also a contiguous copy into this range, so just overwrite it's data instead of allocating anything, also don't need to invalid anything
                    staged_memory.write(mapping, len, f);
                    return;
                }

                // Else need to create a space in the staging buffer to copy from
                let src_offset = write_regions.push_offset(offset, len);
                if src_offset + len > staged_memory.capacity() {
                    staged_memory.resize(src_offset + len).unwrap();
                }

                staged_memory.write(src_offset, len, f);

                // If we are writing to a region, no need to copy to it first
                // internal_copy_regions.clear_dst_range(offset..offset+len);
            }
        }
    }
}

// impl CopyableMemory for DeviceLocalStagedMemory {
//     fn copy_non_overlapping(&mut self, src_offset: usize, dst_offset: usize, len: usize) {
//         assert!(!utility::is_overlapping(src_offset, dst_offset, len), "Ranges must not be overlapping");
//
//         match &mut self.staged_memory {
//             StagingMemory::Parity { staged_memory, invalidated_regions } => {
//                 // Just directly copy in staging buffer, and mark the destination as invalidated
//                 staged_memory.copy_non_overlapping(src_offset, dst_offset, len);
//                 invalidated_regions.insert(dst_offset..dst_offset+len);
//             }
//             StagingMemory::Minimal { staged_memory, destroy_on_copy, internal_copy_regions, write_regions, resize } => {
//                 // Need to check if copy src range overlaps any copy_region destinations, because it so should write the corresponding src values instead
//                 // If the copy dst range overlaps any copy_region
//
//             }
//         }
//     }
// }

impl FlushableMemory for DeviceLocalStagedMemory {
    fn flush(&mut self, command_pool: &mut CommandPool, queue: &mut vk::Queue) {
        let mut cmd = command_pool.allocate_command_buffer();

        cmd.record(command_pool, |mut cmd| {
            match &mut self.staged_memory {
                StagingMemory::Parity { staged_memory, invalidated_regions } => {
                    if staged_memory.capacity() as vk::DeviceSize != self.device_buffer_size {
                        let (new_device_buffer, new_device_buffer_memory, new_buffer_size) = super::allocate_memory(self.device.as_ref(), self.usage,
                                                                                                                    vk::MemoryPropertyFlags::eDeviceLocalBit,
                                                                                                                    vk::MemoryPropertyFlags::eNone,
                                                                                                                    vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit | vk::MemoryPropertyFlags::eHostCachedBit,
                                                                                                                    staged_memory.capacity() as vk::DeviceSize);

                        let region = vk::BufferCopy {
                            src_offset: 0,
                            dst_offset: 0,
                            size: self.device_buffer_size.min(new_buffer_size),
                        }.into_raw();

                        cmd.copy_buffer(self.device_buffer.as_ref(), &new_device_buffer, &[region]);

                        self.device_buffer = Arc::new(new_device_buffer);
                        self.device_buffer_memory = Arc::new(new_device_buffer_memory);
                        self.device_buffer_size = new_buffer_size;
                    }

                    let regions = std::mem::replace(invalidated_regions, Default::default()).into_iter().map(|region| vk::BufferCopy {
                        src_offset: region.offset as vk::DeviceSize,
                        dst_offset: region.offset as vk::DeviceSize,
                        size: region.len as vk::DeviceSize,
                    }.into_raw()).collect::<Vec<_>>();

                    if !regions.is_empty() {
                        cmd.copy_buffer(staged_memory.get_ref().buffer.as_ref(), self.device_buffer.as_ref(), &regions[..]);
                    }
                }
                StagingMemory::Minimal { staged_memory, destroy_on_flush, write_regions, resize } => {
                    if let Some(new_capacity) = resize.take() {
                        let (new_device_buffer, new_device_buffer_memory, new_buffer_size) = super::allocate_memory(self.device.as_ref(), self.usage,
                                                                                                                    vk::MemoryPropertyFlags::eDeviceLocalBit,
                                                                                                                    vk::MemoryPropertyFlags::eNone,
                                                                                                                    vk::MemoryPropertyFlags::eHostVisibleBit | vk::MemoryPropertyFlags::eHostCoherentBit | vk::MemoryPropertyFlags::eHostCachedBit,
                                                                                                                    new_capacity);

                        let region = vk::BufferCopy {
                            src_offset: 0,
                            dst_offset: 0,
                            size: self.device_buffer_size.min(new_buffer_size),
                        }.into_raw();

                        cmd.copy_buffer(self.device_buffer.as_ref(), &new_device_buffer, &[region]);

                        self.device_buffer = Arc::new(new_device_buffer);
                        self.device_buffer_memory = Arc::new(new_device_buffer_memory);
                        self.device_buffer_size = new_buffer_size;
                    }

                    if let Some(staged_memory) = staged_memory {
                        let regions = std::mem::replace(write_regions, Default::default()).into_iter().map(|mapping| vk::BufferCopy {
                            src_offset: mapping.src_offset as vk::DeviceSize,
                            dst_offset: mapping.dst_offset as vk::DeviceSize,
                            size: mapping.len as vk::DeviceSize,
                        }.into_raw()).collect::<Vec<_>>();

                        if !regions.is_empty() {
                            cmd.copy_buffer(staged_memory.get_ref().buffer.as_ref(), self.device_buffer.as_ref(), &regions[..]);
                        }
                    }
                }
            }
            Ok(())
        });

        let cmds = [cmd.command.as_handle()];

        let submit_info = vk::SubmitInfo {
            wait_semaphores: &[],
            wait_dst_stage_mask: &[],
            command_buffers: &cmds[..],
            signal_semaphores: &[],
        }.into_raw();

        queue.queue_submit(&[submit_info], None).unwrap();
        queue.queue_wait_idle().unwrap();
    }
}