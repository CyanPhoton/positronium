use positronium_vk as vk;
use super::Device;

pub mod host_local;
pub mod device_local_staged;

fn allocate_memory(device: &Device, usage: vk::BufferUsageFlags, required_properties: vk::MemoryPropertyFlags, requested_properties: vk::MemoryPropertyFlags, excluded_properties: vk::MemoryPropertyFlags, min_capacity: vk::DeviceSize) -> (vk::Buffer, vk::DeviceMemory, vk::DeviceSize)  {
    let buffer_info = vk::BufferCreateInfo {
        flags: Default::default(),
        size: min_capacity,
        usage,
        sharing_mode: vk::SharingMode::eExclusive,
        queue_family_indices: &[],
    }.into_raw();

    let mut buffer = device.device.create_buffer(&buffer_info)
        .expect("Failed to create buffer").0;

    let mut memory_requirements = buffer.get_buffer_memory_requirements();

    if memory_requirements.size != min_capacity {
        // Recreate buffer if the allocation is going to have a different (more) amount of memory
        let buffer_info = vk::BufferCreateInfo {
            flags: Default::default(),
            size: memory_requirements.size,
            usage,
            sharing_mode: vk::SharingMode::eExclusive,
            queue_family_indices: &[],
        }.into_raw();

        buffer = device.device.create_buffer(&buffer_info)
            .expect("Failed to create buffer").0;

        memory_requirements = buffer.get_buffer_memory_requirements();
    }

    let memory_type_index = find_memory_type(device, memory_requirements.memory_type_bits, required_properties, requested_properties, excluded_properties);

    let alloc_info = vk::MemoryAllocateInfo {
        allocation_size: memory_requirements.size,
        memory_type_index,
    }.into_raw();

    let mut buffer_memory = device.device.allocate_memory(&alloc_info)
        .expect("Failed to allocate memory").0;

    buffer.bind_buffer_memory(&buffer_memory, 0)
        .expect("Failed to bind buffer to memory");

    (buffer, buffer_memory, memory_requirements.size)
}

fn find_memory_type(device: &Device, type_filter: u32, required_properties: vk::MemoryPropertyFlags, requested_properties: vk::MemoryPropertyFlags, excluded_properties: vk::MemoryPropertyFlags) -> u32 {
    let memory_properties = device.physical_device.get_physical_device_memory_properties();

    let mut best_option: Option<(u32, u32)> = None;

    for i in 0..memory_properties.memory_type_count {
        if type_filter & (1 << i) != 0 {
            let properties = memory_properties.memory_types[i as usize].property_flags;
            if properties & (required_properties | requested_properties) == (required_properties | requested_properties)
                && properties & excluded_properties == vk::MemoryPropertyFlags::eNone {
                return i;
            } else if properties & required_properties == required_properties
                && properties & excluded_properties == vk::MemoryPropertyFlags::eNone {

                let count = u32::from((properties & requested_properties)).count_ones();

                if let Some((_, best_count)) = best_option {
                    if count > best_count {
                        best_option = Some((i, count));
                    }
                } else {
                    best_option = Some((i, count));
                }
            }
        }
    }

    if let Some((best, _)) = best_option {
        return best;
    }

    panic!("Failed to find memory type")
}