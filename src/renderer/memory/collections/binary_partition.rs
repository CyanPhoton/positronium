// use std::cmp::max;
// use std::fmt::{Debug, Display, Formatter};
// use std::marker::PhantomData;
// use std::mem::{ManuallyDrop, MaybeUninit};
// use std::sync::atomic::{AtomicUsize, Ordering};
// use crate::renderer::memory::Memory;


//
// pub struct DynamicCPUMemory {
//     data: Vec<u8>
// }
//
// impl Memory for DynamicCPUMemory {
//     type MemoryRef = Vec<u8>;
//
//     fn new() -> Self {
//         DynamicCPUMemory {
//             data: Vec::new()
//         }
//     }
//
//     fn with_capacity(bytes: usize) -> Result<Self> {
//         let mut data = Vec::new();
//         data.try_reserve_exact(bytes)
//             .map_err(|_| ErrorType::AllocatorOutOfMemory { tried: bytes }.to_err())?;
//
//         data.resize(bytes, 0);
//         Ok(DynamicCPUMemory {
//             data
//         })
//     }
//
//     fn capacity(&self) -> usize {
//         self.data.len()
//     }
//
//     fn resize(&mut self, bytes: usize) -> Result<()> {
//         self.data.try_reserve_exact(bytes.saturating_sub(self.data.len()))
//             .map_err(|_| ErrorType::AllocatorOutOfMemory { tried: bytes }.to_err())?;
//
//         self.data.resize(bytes, 0);
//         Ok(())
//     }
//
//     fn map_slice(&self, offset: usize, range: usize) -> &[u8] {
//         &self.data[offset..offset+range]
//     }
//
//     fn map_slice_mut(&mut self, offset: usize, range: usize) -> &mut [u8] {
//         &mut self.data[offset..offset+range]
//     }
//
//     fn get_ref(&self) -> Self::MemoryRef {
//         todo!()
//     }
// }
//
// // TODO: impl Memory for a GPU memory allocator (extend as needed)
//
// static NEXT_BP_ID: AtomicUsize = AtomicUsize::new(1);
//
// pub struct BinaryPartition<M: Memory> {
//     #[cfg(debug_assertions)]
//     id: usize,
//     memory: M,
//     block_size: usize, // Always a power of 2
//     // Levels are indexed from bottom, so [0] is bottom level,
//     // The highest empty slot below a given slot
//     empty_slot_info: Vec<Vec<i8>>,
//     // Derived from max_capacity
//     max_levels: Option<i8>
// }
//
// pub struct Index<T: Sized> {
//     #[cfg(debug_assertions)]
//     bp_id: usize,
//     level: u8,
//     slot: usize,
//     offset: usize,
//     _phantom: PhantomData<T>
// }
//
// pub struct SliceIndex<T: Sized> {
//     #[cfg(debug_assertions)]
//     bp_id: usize,
//     level: u8,
//     slot: usize,
//     elements: usize,
//     offset: usize,
//     _phantom: PhantomData<T>
// }
//
// impl<T: Sized> Index<T> {
//     pub fn null() -> Self {
//         Index {
//             #[cfg(debug_assertions)]
//             bp_id: 0,
//             level: !0,
//             slot: !0,
//             offset: 0,
//             _phantom: Default::default()
//         }
//     }
//
//     pub fn offset(&self) -> usize {
//         self.offset
//     }
// }
//
// impl<T: Sized> SliceIndex<T> {
//     pub fn null() -> Self {
//         SliceIndex {
//             #[cfg(debug_assertions)]
//             bp_id: 0,
//             level: !0,
//             slot: !0,
//             elements: !0,
//             offset: 0,
//             _phantom: Default::default()
//         }
//     }
//
//     pub fn offset(&self) -> usize {
//         self.offset
//     }
// }
//
// struct AllocInfo {
//     level: u8,
//     slot: usize,
// }
//
// pub struct Options<M: Memory> {
//     block_size: usize,
//     initial_capacity: usize,
//     max_capacity: Option<usize>,
//     memory: Option<M>
// }
//
// impl<M: Memory> Default for Options<M> {
//     fn default() -> Self {
//         Options {
//             block_size: Self::DEFAULT_MIN_SIZE,
//             initial_capacity: Self::DEFAULT_CAPACITY,
//             max_capacity: None,
//             memory: None
//         }
//     }
// }
//
// impl<M: Memory> Options<M> {
//     pub const DEFAULT_MIN_SIZE: usize = 1024;
//     pub const DEFAULT_CAPACITY: usize = 0;
//
//     pub fn new() -> Options<M> {
//         Options::default()
//     }
//
//     /// Must be power of two
//     pub fn with_block_size(mut self, block_size: usize) -> Self {
//         assert!(block_size.is_power_of_two(), "Block size must be a power of 2");
//         self.block_size = block_size;
//         self
//     }
//
//     /// In units of block_size, must be 0 or a power of two
//     pub fn with_initial_capacity(mut self, initial_capacity: usize) -> Self {
//         assert!(initial_capacity == 0 || initial_capacity.is_power_of_two(), "Initial capacity must be zero or a power of 2");
//         self.initial_capacity = initial_capacity;
//         self
//     }
//
//     /// In units of block_size, must be a power of two, must be > 0, None means that this structure does not limit it's size,
//     /// however the underlying memory allocator may return an OOM if it chooses, or it may panic.
//     pub fn with_max_capacity(mut self, max_capacity: Option<usize>) -> Self {
//         assert!(max_capacity.is_none() || max_capacity.unwrap().is_power_of_two(), "Max capacity must be None or a power of 2");
//         self.max_capacity = max_capacity;
//         self
//     }
//
//     /// With an existing memory,
//     /// When passed to new_with, must have at equal to initial_capacity (unless initial_capacity is 0), and capacity must be 2^n of block_size
//     pub fn with_memory(mut self, memory: Option<M>) -> Self {
//         self.memory = memory;
//         self
//     }
// }
//
// impl<M: Memory> BinaryPartition<M> {
//     const USED: i8 = -1;
//
//     #[cfg(debug_assertions)]
//     fn new_id() -> usize {
//         NEXT_BP_ID.fetch_update(Ordering::SeqCst, Ordering::SeqCst, |i| Some(i + 1)).unwrap()
//     }
//
//     pub fn new() -> Result<Self> {
//         Self::new_with(Options::default())
//     }
//
//     pub fn new_with(options: Options<M>) -> Result<Self> {
//         let block_size = options.block_size;
//
//         let capacity = if let Some(memory) = &options.memory {
//             if options.initial_capacity == 0 {
//                 memory.capacity()
//             } else {
//                 assert_eq!(memory.capacity() % block_size, 0);
//                 let mem_capacity = memory.capacity() / block_size;
//                 assert!(mem_capacity.is_power_of_two());
//                 assert_eq!(mem_capacity, options.initial_capacity);
//                 mem_capacity
//             }
//         } else {
//             options.initial_capacity
//         };
//
//         let max_capacity = options.max_capacity;
//         let max_levels = max_capacity.map(|max_capacity| {
//             // e.g. 4 == 0b00000100 -> 2 trailing zeros
//             //      4 units of block_size means:
//             //      [X X X X]
//             //      [X X|X X]
//             //      [X|X|X|X]
//             //      aka 3 levels
//             //      Note also equal to log2() + 1
//             max_capacity.trailing_zeros() as i8 + 1
//         });
//
//         assert!(capacity == 0 || capacity.is_power_of_two(), "Capacity must 0 or be a power of 2");
//         let capacity_size = block_size * capacity;
//
//         let levels = if capacity == 0 {
//             0
//         } else {
//             (capacity.trailing_zeros() + 1) as usize
//         };
//
//         let empty_slot_info = (0..levels).map(|l| vec![l as i8; 2usize.pow((levels - l - 1) as u32)]).collect();
//
//         let memory = if let Some(memory) = options.memory {
//             memory
//         } else {
//             M::with_capacity(capacity_size)?
//         };
//
//         Ok(BinaryPartition {
//             #[cfg(debug_assertions)]
//             id: Self::new_id(),
//             memory,
//             block_size,
//             empty_slot_info,
//             max_levels
//         })
//     }
//
//     fn alloc_uninit(&mut self, size: usize, align: usize) -> Result<AllocInfo> {
//         assert_eq!(self.block_size % align, 0, "Min size must be multiple of T alignment so that alignment is upheld");
//
//         let target_size = size.next_power_of_two();
//         let target_level = target_size.trailing_zeros().saturating_sub(self.block_size.trailing_zeros()) as i8;
//
//         if self.empty_slot_info.is_empty() || self.empty_slot_info.last().unwrap()[0] < target_level {
//             // Need to allocate more room
//
//             let existing_levels = self.empty_slot_info.len() as i8;
//
//             let new_levels = if self.empty_slot_info.is_empty() || self.empty_slot_info.last().unwrap()[0] == (self.empty_slot_info.len() - 1) as i8 {
//                 // Currently empty
//                 target_level + 1
//             } else {
//                 // Existing data, so need to leave room for existing
//                 target_level + 2
//             }.max(existing_levels + 1); // Since it must grow
//
//             if let Some(max_levels) = self.max_levels {
//                 if new_levels > max_levels {
//                     return Err(ErrorType::MaxCapacityHit { tried: new_levels, max: max_levels }.to_err());
//                 }
//             }
//
//             // Try request the extra space before modifying anything
//             let new_level_0_size = 2usize.pow((new_levels - 1) as u32) * self.block_size;
//             self.memory.resize(new_level_0_size)?;
//
//             // Grow existing vectors
//             for i in 0..existing_levels {
//                 let new_size = 2usize.pow((new_levels - i - 1) as u32);
//                 self.empty_slot_info[i as usize].resize(new_size, i);
//             }
//
//             // Add new ones
//             if self.empty_slot_info.is_empty() {
//                 for i in existing_levels..new_levels {
//                     let size = 2usize.pow((new_levels - i - 1) as u32);
//                     self.empty_slot_info.push(vec![i; size]);
//                 }
//             } else {
//                 for i in existing_levels..new_levels {
//                     let size = 2usize.pow((new_levels - i - 1) as u32);
//                     self.empty_slot_info.push((0..size).map(|j| self.empty_slot_info[i as usize - 1][j * 2].max(self.empty_slot_info[i as usize - 1][j * 2 + 1])).collect());
//                 }
//             }
//         }
//
//
//         let mut level = self.empty_slot_info.len() as i8 - 1;
//         let mut slot = 0;
//
//         // Follow binary tree of sorts down to appropriate slot
//         while level > target_level {
//             level -= 1;
//             slot *= 2;
//             if self.empty_slot_info[level as usize][slot] < target_level {
//                 slot += 1;
//             }
//         }
//
//         //Mark as used
//         self.empty_slot_info[level as usize][slot] = Self::USED;
//
//         let alloc = AllocInfo {
//             level: level as u8,
//             slot,
//         };
//
//         // Walk back up, updating empty_slot_info
//         self.propagate_change(&alloc);
//
//         Ok(alloc)
//     }
//
//     fn free(&mut self, alloc: &AllocInfo) -> i8 {
//         // Set slot to not used
//         let old_level = std::mem::replace(&mut self.empty_slot_info[alloc.level as usize][alloc.slot], alloc.level as i8);
//
//         // Propagate upwards
//         self.propagate_change(alloc);
//
//         old_level
//     }
//
//     fn unfree(&mut self, alloc: &AllocInfo, old_level: i8) {
//         // Restore state
//         self.empty_slot_info[alloc.level as usize][alloc.slot] = old_level;
//
//         // Propagate upwards
//         self.propagate_change(alloc);
//     }
//
//     fn realloc_uninit(&mut self, alloc: &mut AllocInfo, old_size: usize, new_size: usize, new_align: usize) -> Result<()> {
//         let new_target_size = new_size.next_power_of_two();
//         let new_target_level = new_target_size.trailing_zeros().saturating_sub(self.block_size.trailing_zeros()) as i8;
//
//         let level_diff = new_target_level - alloc.level as i8;
//
//         if level_diff > 0 {
//             // Increased allocation
//             let factor = 2usize.pow(level_diff as u32);
//             if alloc.slot % factor == 0 {
//                 // Current slot aligns with needed higher slot
//                 let new_slot = alloc.slot / factor;
//
//                 if self.empty_slot_info[new_target_level as usize][new_slot] == new_target_level {
//                     // That higher slot is free, so can claim that
//                     self.free(alloc);
//
//                     self.empty_slot_info[new_target_level as usize][new_slot] = Self::USED;
//                     alloc.level = new_target_level as u8;
//                     alloc.slot = new_slot;
//                     self.propagate_change(&alloc);
//
//                     return Ok(()); // Return to prevent fall down
//                 } else {
//                     // Not free, need new allocation, fall down
//                 }
//             } else {
//                 // Does not align, need new allocation, fall down
//             }
//
//             let old_offset = alloc.slot * self.block_size * 2usize.pow(alloc.level as u32);
//             let src = self.memory.map_slice(old_offset, old_size);
//             let mut data_copy = Vec::<u8>::with_capacity(old_size);
//             // Safety, size is guaranteed, all values of u8 are valid, no drop
//             unsafe { data_copy.set_len(old_size); }
//             unsafe { std::ptr::copy_nonoverlapping(src.as_ptr(), data_copy.as_mut_ptr(), old_size); }
//
//             let old_level = self.free(&alloc);
//             *alloc = match self.alloc_uninit(new_size, new_align) {
//                 Ok(a) => a,
//                 Err(e) => {
//                     self.unfree(&alloc, old_level);
//                     return Err(e);
//                 }
//             };
//
//             let new_offset = alloc.slot * self.block_size * 2usize.pow(alloc.level as u32);
//             let dst = self.memory.map_slice_mut(new_offset, old_size);
//
//             unsafe { std::ptr::copy_nonoverlapping(data_copy.as_ptr(), dst.as_mut_ptr(), old_size); }
//
//         } else if level_diff < 0 {
//             // Decreased allocation
//             let diff_abs = level_diff.unsigned_abs();
//
//             // Lower alloc
//             alloc.level -= diff_abs;
//             alloc.slot *= 2usize.pow(diff_abs as u32);
//
//             // Set used, and propagate this change upwards, will override previous USED
//             self.empty_slot_info[alloc.level as usize][alloc.slot] = Self::USED;
//             self.propagate_change(&alloc);
//         } else {
//             // Same allocation
//             // Nothing to do
//         }
//
//         Ok(())
//     }
//
//     fn propagate_change(&mut self, alloc: &AllocInfo) {
//         let mut level = alloc.level + 1;
//         let mut slot = alloc.slot / 2;
//         while level < self.empty_slot_info.len() as u8 {
//             self.empty_slot_info[level as usize][slot] = self.empty_slot_info[level as usize -1][slot * 2].max(self.empty_slot_info[level as usize - 1][slot * 2 + 1]);
//             level += 1;
//             slot /= 2;
//         }
//     }
//
//     /// Note: will not modify state if OOM
//     pub fn insert_value<T: Sized>(&mut self, data: T) -> ResultR<Index<T>, T> {
//         let alloc_info = match self.alloc_uninit(std::mem::size_of::<T>(), std::mem::align_of::<T>()) {
//             Ok(alloc_info) => alloc_info,
//             Err(e) => return Err(e.put_data(data))
//         };
//
//         let dst = self.index_alloc(&alloc_info);
//         dst.write(data);
//
//         Ok(Index {
//             #[cfg(debug_assertions)]
//             bp_id: self.id,
//             level: alloc_info.level,
//             slot: alloc_info.slot,
//             offset: alloc_info.slot * self.block_size * 2usize.pow(alloc_info.level as u32),
//             _phantom: Default::default(),
//         })
//     }
//
//     pub fn insert_array<T: Sized, const N: usize>(&mut self, data: [T; N]) -> ResultR<SliceIndex<T>, [T; N]> {
//         let alloc_info = match self.alloc_uninit(std::mem::size_of::<T>() * N, std::mem::align_of::<T>()) {
//             Ok(alloc_info) => alloc_info,
//             Err(e) => return Err(e.put_data(data))
//         };
//
//         let dst = self.index_alloc_slice(&alloc_info, N);
//
//         // Safety: T and MaybeUninit<T> have the same layout
//         let src = unsafe { std::mem::transmute::<_, &[MaybeUninit<T>]>(&data[..]) };
//
//         // Safety: Naturally this is correct, and data is forgotten so that it's destructor is not called
//         unsafe { std::ptr::copy_nonoverlapping(src.as_ptr(), dst.as_mut_ptr(), N); }
//         std::mem::forget(data);
//
//         Ok(SliceIndex {
//             #[cfg(debug_assertions)]
//             bp_id: self.id,
//             level: alloc_info.level,
//             slot: alloc_info.slot,
//             elements: N,
//             offset: alloc_info.slot * self.block_size * 2usize.pow(alloc_info.level as u32),
//             _phantom: Default::default(),
//         })
//     }
//
//     pub fn insert_vec<T: Sized>(&mut self, data: Vec<T>) -> ResultR<SliceIndex<T>, Vec<T>> {
//         let alloc_info = match self.alloc_uninit(std::mem::size_of::<T>() * data.len(), std::mem::align_of::<T>()) {
//             Ok(alloc_info) => alloc_info,
//             Err(e) => return Err(e.put_data(data)),
//         };
//
//         let dst = self.index_alloc_slice(&alloc_info, data.len());
//
//         // Safety: T and MaybeUninit<T> have the same layout
//         let src = unsafe { std::mem::transmute::<_, &[MaybeUninit<T>]>(&data[..]) };
//
//         // Safety: Naturally this is correct, and data turned into manually drop, so destructors are not called but heap memory is freed
//         unsafe { std::ptr::copy_nonoverlapping(src.as_ptr(), dst.as_mut_ptr(), data.len()); }
//         let data = unsafe { std::mem::transmute::<_, Vec<ManuallyDrop<T>>>(data) };
//
//         Ok(SliceIndex {
//             #[cfg(debug_assertions)]
//             bp_id: self.id,
//             level: alloc_info.level,
//             slot: alloc_info.slot,
//             elements: data.len(),
//             offset: alloc_info.slot * self.block_size * 2usize.pow(alloc_info.level as u32),
//             _phantom: Default::default(),
//         })
//     }
//
//     pub fn insert_slice<T: Sized + Copy>(&mut self, data: &[T]) -> Result<SliceIndex<T>> {
//         let alloc_info = self.alloc_uninit(std::mem::size_of::<T>() * data.len(), std::mem::align_of::<T>())?;
//
//         let dst = self.index_alloc_slice(&alloc_info, data.len());
//         let src = unsafe { std::mem::transmute::<_, &[MaybeUninit<T>]>(&data[..]) };
//
//         dst.copy_from_slice(src);
//
//         Ok(SliceIndex {
//             #[cfg(debug_assertions)]
//             bp_id: self.id,
//             level: alloc_info.level,
//             slot: alloc_info.slot,
//             elements: data.len(),
//             offset: alloc_info.slot * self.block_size * 2usize.pow(alloc_info.level as u32),
//             _phantom: Default::default(),
//         })
//     }
//
//     pub fn extend_array<T: Sized, const N: usize>(&mut self, index: &mut SliceIndex<T>, extension: [T; N]) -> ResultR<(), [T; N]> {
//         if extension.is_empty() {
//             return Ok(());
//         }
//
//         let mut alloc = AllocInfo {
//             level: index.level,
//             slot: index.slot
//         };
//
//         let old_elements = index.elements;
//         let new_elements = old_elements + N;
//         let old_size = std::mem::size_of::<T>() * old_elements;
//         let new_size = std::mem::size_of::<T>() * new_elements;
//
//         if let Err(e) = self.realloc_uninit(&mut alloc, old_size, new_size, std::mem::align_of::<T>()) {
//             return Err(e.put_data(extension));
//         }
//
//         index.level = alloc.level;
//         index.slot = alloc.slot;
//         index.elements = new_elements;
//         index.offset = alloc.slot * self.block_size * 2usize.pow(alloc.level as u32);
//
//         let data = self.index_alloc_slice(&alloc, index.elements);
//         // (&mut data[old_elements..]).copy_from_slice(extension);
//
//         // Safety: Naturally this is correct, and data is forgotten, so destructors are not called
//         unsafe { std::ptr::copy_nonoverlapping(extension.as_ptr() as *const MaybeUninit<T>, data.as_mut_ptr().offset(old_elements as isize), extension.len()); }
//         std::mem::forget(extension);
//
//         Ok(())
//     }
//
//     pub fn extend_vec<T: Sized>(&mut self, index: &mut SliceIndex<T>, extension: Vec<T>) -> ResultR<(), Vec<T>>{
//         if extension.is_empty() {
//             return Ok(());
//         }
//
//         let mut alloc = AllocInfo {
//             level: index.level,
//             slot: index.slot
//         };
//
//         let old_elements = index.elements;
//         let new_elements = old_elements + extension.len();
//         let old_size = std::mem::size_of::<T>() * old_elements;
//         let new_size = std::mem::size_of::<T>() * new_elements;
//
//         if let Err(e) = self.realloc_uninit(&mut alloc, old_size, new_size, std::mem::align_of::<T>()) {
//             return Err(e.put_data(extension));
//         }
//
//         index.level = alloc.level;
//         index.slot = alloc.slot;
//         index.elements = new_elements;
//         index.offset = alloc.slot * self.block_size * 2usize.pow(alloc.level as u32);
//
//         // Safety: T and MaybeUninit<T> have same layout
//         let extension = unsafe { std::mem::transmute::<_, Vec<MaybeUninit<T>>>(extension) };
//
//         let data = self.index_alloc_slice(&alloc, index.elements);
//         // (&mut data[old_elements..]).copy_from_slice(extension);
//
//         // Safety: Naturally this is correct, and data turned into manually drop, so destructors are not called but heap memory is freed
//         unsafe { std::ptr::copy_nonoverlapping(extension.as_ptr(), data.as_mut_ptr().offset(old_elements as isize), extension.len()); }
//         unsafe { std::mem::transmute::<_, Vec<ManuallyDrop<T>>>(extension) };
//
//         Ok(())
//     }
//
//     pub fn extend_slice<T: Sized + Copy>(&mut self, index: &mut SliceIndex<T>, extension: &[T]) -> Result<()> {
//         if extension.is_empty() {
//             return Ok(());
//         }
//
//         let mut alloc = AllocInfo {
//             level: index.level,
//             slot: index.slot
//         };
//
//         let old_elements = index.elements;
//         let new_elements = old_elements + extension.len();
//         let old_size = std::mem::size_of::<T>() * old_elements;
//         let new_size = std::mem::size_of::<T>() * new_elements;
//
//         self.realloc_uninit(&mut alloc, old_size, new_size, std::mem::align_of::<T>())?;
//
//         index.level = alloc.level;
//         index.slot = alloc.slot;
//         index.elements = new_elements;
//         index.offset = alloc.slot * self.block_size * 2usize.pow(alloc.level as u32);
//
//         // Safety: T and MaybeUninit<T> have same layout
//         let extension = unsafe { std::mem::transmute::<_, &[MaybeUninit<T>]>(extension) };
//
//         let data = self.index_alloc_slice(&alloc, index.elements);
//         (&mut data[old_elements..]).copy_from_slice(extension);
//
//         Ok(())
//     }
//
//     pub fn forget_value<T: Sized>(&mut self, index: Index<T>) {
//         self.free(&AllocInfo {
//             level: index.level,
//             slot: index.slot,
//         });
//     }
//
//     pub fn remove_value<T: Sized>(&mut self, index: Index<T>) -> T {
//         let range = std::mem::size_of::<T>();
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice(offset, range);
//         let data = unsafe { std::slice::from_raw_parts(data.as_ptr() as *const T, 1) };
//
//         // Safety, copy out the value, and since the underlying type us [u8] no need to worry about destructor
//         let mut result = MaybeUninit::uninit();
//         unsafe { std::ptr::copy_nonoverlapping(data.as_ptr(), result.as_mut_ptr(), 1); }
//         let result = unsafe { result.assume_init() };
//
//         self.free(&AllocInfo {
//             level: index.level,
//             slot: index.slot,
//         });
//
//         result
//     }
//
//     pub fn forget_slice<T: Sized>(&mut self, index: SliceIndex<T>) {
//         self.free(&AllocInfo {
//             level: index.level,
//             slot: index.slot,
//         });
//     }
//
//     pub fn remove_slice<T: Sized>(&mut self, index: SliceIndex<T>) {
//         let range = std::mem::size_of::<T>() * index.elements;
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice_mut(offset, range);
//         let data = unsafe { std::slice::from_raw_parts_mut(data.as_mut_ptr() as *mut ManuallyDrop<T>, index.elements) };
//
//         // Safety: Since not returning it
//         if std::mem::needs_drop::<T>() {
//             for d in data {
//                 unsafe { ManuallyDrop::drop(d); }
//             }
//         }
//
//         self.free(&AllocInfo {
//             level: index.level,
//             slot: index.slot,
//         });
//     }
//
//     fn index_alloc<T: Sized>(&mut self, index: &AllocInfo) -> &mut MaybeUninit<T> {
//         let range = std::mem::size_of::<T>();
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice_mut(offset, range);
//         let data = unsafe { std::slice::from_raw_parts_mut(data.as_ptr() as *mut MaybeUninit<T>, 1) };
//
//         &mut data[0]
//     }
//
//     fn index_alloc_slice<T: Sized>(&mut self, index: &AllocInfo, elements: usize) -> &mut [MaybeUninit<T>] {
//         let range = std::mem::size_of::<T>() * elements;
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice_mut(offset, range);
//         let data = unsafe { std::slice::from_raw_parts_mut(data.as_ptr() as *mut MaybeUninit<T>, elements) };
//
//         data
//     }
// }
//
// impl<M: Memory, T> std::ops::Index<&Index<T>> for BinaryPartition<M> {
//     type Output = T;
//
//     fn index(&self, index: &Index<T>) -> &T {
//         #[cfg(debug_assertions)]
//         assert_eq!(self.id, index.bp_id, "Index must originate from the same instance of BinaryPartition");
//
//         let range = std::mem::size_of::<T>();
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice(offset, range);
//         let data = unsafe { std::slice::from_raw_parts(data.as_ptr() as *const T, 1) };
//
//         &data[0]
//     }
// }
//
// impl<M: Memory, T> std::ops::Index<&SliceIndex<T>> for BinaryPartition<M> {
//     type Output = [T];
//
//     fn index(&self, index: &SliceIndex<T>) -> &[T] {
//         #[cfg(debug_assertions)]
//         assert_eq!(self.id, index.bp_id, "Index must originate from the same instance of BinaryPartition");
//
//         let range = std::mem::size_of::<T>() * index.elements;
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice(offset, range);
//         let data = unsafe { std::slice::from_raw_parts(data.as_ptr() as *const T, index.elements) };
//
//         data
//     }
// }
//
// impl<M: Memory, T> std::ops::IndexMut<&Index<T>> for BinaryPartition<M> {
//     fn index_mut(&mut self, index: &Index<T>) -> &mut T {
//         #[cfg(debug_assertions)]
//         assert_eq!(self.id, index.bp_id, "Index must originate from the same instance of BinaryPartition");
//
//         let range = std::mem::size_of::<T>();
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice_mut(offset, range);
//         let data = unsafe { std::slice::from_raw_parts_mut(data.as_mut_ptr() as *mut T, 1) };
//
//         &mut data[0]
//     }
// }
//
// impl<M: Memory, T> std::ops::IndexMut<&SliceIndex<T>> for BinaryPartition<M> {
//     fn index_mut(&mut self, index: &SliceIndex<T>) -> &mut [T] {
//         #[cfg(debug_assertions)]
//         assert_eq!(self.id, index.bp_id, "Index must originate from the same instance of BinaryPartition");
//
//         let range = std::mem::size_of::<T>() * index.elements;
//         let offset = index.slot * self.block_size * 2usize.pow(index.level as u32);
//
//         let data = self.memory.map_slice_mut(offset, range);
//         let data = unsafe { std::slice::from_raw_parts_mut(data.as_mut_ptr() as *mut T, index.elements) };
//
//         data
//     }
// }
//
// #[cfg(test)]
// mod test {
//     use super::{BinaryPartition, DynamicCPUMemory, Options, SliceIndex};
//
//     #[test]
//     pub fn insert_test_1() {
//         let v1 = [0x0101010101010101usize; 64]; // Half slot
//         let v2 = [0x0202020202020202usize; 128]; // 1 slot
//         let v3 = [0x0303030303030303usize; 129]; // 2 slot
//         let v4 = [0x0404040404040404usize; 1024]; // 8 slot
//         let v5 = [0x0505050505050505usize; 64]; // Half slot
//
//         let mut a = BinaryPartition::<DynamicCPUMemory>::new().unwrap();
//
//         let i1 = a.insert_array(v1).unwrap();
//         assert_eq!(i1.level, 0);
//         assert_eq!(i1.slot, 0);
//         assert_eq!(a.empty_slot_info.len(), 1);
//         assert_eq!(a.empty_slot_info[0].len(), 1);
//         assert_eq!(a.empty_slot_info[0][0], -1);
//         assert_eq!(&v1[..], &a[&i1]);
//
//         let i2 = a.insert_array(v2).unwrap();
//         assert_eq!(i2.level, 0);
//         assert_eq!(i2.slot, 1);
//         assert_eq!(a.empty_slot_info.len(), 2);
//         assert_eq!(a.empty_slot_info[0].len(), 2);
//         assert_eq!(a.empty_slot_info[0][0], -1);
//         assert_eq!(a.empty_slot_info[0][1], -1);
//         assert_eq!(a.empty_slot_info[1].len(), 1);
//         assert_eq!(a.empty_slot_info[1][0], -1);
//         assert_eq!(&v2[..], &a[&i2]);
//
//         let i3 = a.insert_array(v3).unwrap();
//         assert_eq!(i3.level, 1);
//         assert_eq!(i3.slot, 1);
//         assert_eq!(a.empty_slot_info.len(), 3);
//         assert_eq!(a.empty_slot_info[0].len(), 4);
//         assert_eq!(a.empty_slot_info[0][0], -1);
//         assert_eq!(a.empty_slot_info[0][1], -1);
//         assert_eq!(a.empty_slot_info[1].len(), 2);
//         assert_eq!(a.empty_slot_info[1][0], -1);
//         assert_eq!(a.empty_slot_info[1][1], -1);
//         assert_eq!(a.empty_slot_info[2].len(), 1);
//         assert_eq!(a.empty_slot_info[2][0], -1);
//         assert_eq!(&v3[..], &a[&i3]);
//
//         let i4 = a.insert_array(v4).unwrap();
//         assert_eq!(i4.level, 3);
//         assert_eq!(i4.slot, 1);
//         assert_eq!(a.empty_slot_info.len(), 5);
//         assert_eq!(a.empty_slot_info[0].len(), 16);
//         assert_eq!(a.empty_slot_info[0][0], -1);
//         assert_eq!(a.empty_slot_info[0][1], -1);
//         assert_eq!(a.empty_slot_info[0][4], 0);
//         assert_eq!(a.empty_slot_info[0][5], 0);
//         assert_eq!(a.empty_slot_info[0][6], 0);
//         assert_eq!(a.empty_slot_info[0][7], 0);
//         assert_eq!(a.empty_slot_info[1].len(), 8);
//         assert_eq!(a.empty_slot_info[1][0], -1);
//         assert_eq!(a.empty_slot_info[1][1], -1);
//         assert_eq!(a.empty_slot_info[1][2], 1);
//         assert_eq!(a.empty_slot_info[1][3], 1);
//         assert_eq!(a.empty_slot_info[2].len(), 4);
//         assert_eq!(a.empty_slot_info[2][0], -1);
//         assert_eq!(a.empty_slot_info[2][1], 2);
//         assert_eq!(a.empty_slot_info[3].len(), 2);
//         assert_eq!(a.empty_slot_info[3][0], 2);
//         assert_eq!(a.empty_slot_info[3][1], -1);
//         assert_eq!(a.empty_slot_info[4].len(), 1);
//         assert_eq!(a.empty_slot_info[4][0], 2);
//         assert_eq!(&v4[..], &a[&i4]);
//
//         let i5 = a.insert_array(v5).unwrap();
//         assert_eq!(i5.level, 0);
//         assert_eq!(i5.slot, 4);
//         assert_eq!(a.empty_slot_info.len(), 5);
//         assert_eq!(a.empty_slot_info[0].len(), 16);
//         assert_eq!(a.empty_slot_info[0][0], -1);
//         assert_eq!(a.empty_slot_info[0][1], -1);
//         assert_eq!(a.empty_slot_info[0][4], -1);
//         assert_eq!(a.empty_slot_info[0][5], 0);
//         assert_eq!(a.empty_slot_info[0][6], 0);
//         assert_eq!(a.empty_slot_info[0][7], 0);
//         assert_eq!(a.empty_slot_info[1].len(), 8);
//         assert_eq!(a.empty_slot_info[1][0], -1);
//         assert_eq!(a.empty_slot_info[1][1], -1);
//         assert_eq!(a.empty_slot_info[1][2], 0);
//         assert_eq!(a.empty_slot_info[1][3], 1);
//         assert_eq!(a.empty_slot_info[2].len(), 4);
//         assert_eq!(a.empty_slot_info[2][0], -1);
//         assert_eq!(a.empty_slot_info[2][1], 1);
//         assert_eq!(a.empty_slot_info[3].len(), 2);
//         assert_eq!(a.empty_slot_info[3][0], 1);
//         assert_eq!(a.empty_slot_info[3][1], -1);
//         assert_eq!(a.empty_slot_info[4].len(), 1);
//         assert_eq!(a.empty_slot_info[4][0], 1);
//         assert_eq!(&v5[..], &a[&i5]);
//
//         println!("{:?}", a.memory.data);
//     }
//
//     #[test]
//     pub fn insert_test_rand() {
//         let options = Options::new()
//             .with_block_size(std::mem::size_of::<usize>());
//         let mut v = BinaryPartition::<DynamicCPUMemory>::new_with(options).unwrap();
//
//         let mut i = Vec::new();
//
//         for j in 0..1000 as usize {
//             let index = v.insert_vec(vec![j; rand::random::<usize>() % 100]).unwrap();
//             i.push(index);
//         }
//
//         for (j, i) in i.iter().enumerate() {
//             assert!(v[i].iter().all(|v| *v == j));
//         }
//
//         for _ in 0..300 {
//             let j = rand::random::<usize>() % i.len();
//
//             let mut temp = SliceIndex::null();
//             std::mem::swap(&mut temp, &mut i[j]);
//
//             v.remove_slice(temp);
//
//             i[j] = v.insert_vec(vec![j; rand::random::<usize>() % 100]).unwrap();
//         }
//
//         for (j, i) in i.iter().enumerate() {
//             assert!(v[i].iter().all(|v| *v == j));
//         }
//
//         for _ in 0..300 {
//             let j = rand::random::<usize>() % i.len();
//
//             v.extend_vec(&mut i[j], vec![j; rand::random::<usize>() % 100]).unwrap();
//         }
//
//         for (j, i) in i.iter().enumerate() {
//             assert!(v[i].iter().all(|v| *v == j));
//         }
//
//         for j in 1000..1200 as usize {
//             let index = v.insert_vec(vec![j; rand::random::<usize>() % 1000]).unwrap();
//             i.push(index);
//         }
//
//         for (j, i) in i.iter().enumerate() {
//             assert!(v[i].iter().all(|v| *v == j));
//         }
//
//         // let view = unsafe { std::slice::from_raw_parts(v.memory.data.as_ptr() as *const usize, v.memory.data.len() / std::mem::size_of::<usize>()) };
//         //
//         // println!("{:?}", view);
//     }
// }