use std::any::TypeId;
use std::collections::{BTreeMap, BTreeSet};
use std::fmt::Debug;
use std::marker::PhantomData;
use std::ops::{Deref, Index, IndexMut};
use std::sync::atomic::{AtomicUsize, Ordering};
use crate::renderer::memory::WriteableMemory;
use crate::utility::linked_vec::{self, Id, LinkedVec};

#[derive(Debug)]
struct AllocatedRegion {
    offset: usize,
    size: usize,
    used: bool,
}

pub struct Region {
    offset: usize,
    size: usize,
    id: linked_vec::Id,
    dynamic_vec_id: usize
}

impl Region {
    pub fn null() -> Region {
        Region {
            offset: !0,
            size: !0,
            id: linked_vec::Id(0), // Ids start at 1, so 0 is invalid
            dynamic_vec_id: 0, // Again, start at 1, so 0 is invalid
        }
    }

    pub fn offset(&self) -> usize {
        self.offset
    }

    pub fn size(&self) -> usize {
        self.size
    }
}

pub struct ManagedRegion {
    region: Region,
    drop_return_sender: flume::Sender<Region>,
}

impl ManagedRegion {
    pub fn into_region(mut self) -> Region {
        let region = std::mem::replace(&mut self.region, Region::null());
        std::mem::forget(self); // Prevent `drop` running
        region
    }
}

impl Drop for ManagedRegion {
    fn drop(&mut self) {
        self.drop_return_sender.send(std::mem::replace(&mut self.region, Region::null())).ok();
    }
}

impl Deref for ManagedRegion {
    type Target = Region;

    fn deref(&self) -> &Region {
        &self.region
    }
}

#[derive(Copy, Clone, Debug)]
pub enum AllocationMode {
    Minimal, // Just allocates the needed space
    Exponential { factor: f32 } // Allocates the needed space * factor
}

impl AllocationMode {
    pub fn adjust_size(self, current: usize, require: usize) -> usize {
        match self {
            AllocationMode::Minimal => require,
            AllocationMode::Exponential { factor} => (current as f32 * factor.powi(require.div_ceil(current) as i32)).round() as usize
        }
    }
}

pub trait StableRegionVecMem<M: WriteableMemory> {
    fn memory(&self) -> &M;
}

impl<T: Sized, M: WriteableMemory> StableRegionVecMem<M> for StableRegionVec<T, M> {
    fn memory(&self) -> &M {
        &self.memory
    }
}

generalise!([pub] StableRegionVec, GeneralStableRegionVec, (), StableRegionVecMem<M> | | | (M, WriteableMemory));

static NEXT_ID: AtomicUsize = AtomicUsize::new(1);

#[derive(Debug)]
pub struct StableRegionVec<T: Sized, M: WriteableMemory> {
    memory: M,
    regions: LinkedVec<AllocatedRegion>,
    free_regions: BTreeMap<usize, BTreeSet<linked_vec::Id>>,
    allocation_mode: AllocationMode,
    drop_return: flume::Receiver<Region>,
    drop_return_sender: flume::Sender<Region>,
    id: usize,
    _data: PhantomData<T>
}

impl<T: Sized, M: WriteableMemory> StableRegionVec<T, M> {
    pub fn new(init: M::Init) -> StableRegionVec<T, M> {
        let (drop_return_sender, drop_return) = flume::unbounded();

        StableRegionVec {
            memory: M::new(init),
            regions: Default::default(),
            free_regions: Default::default(),
            allocation_mode: AllocationMode::Minimal,
            drop_return,
            drop_return_sender,
            id: NEXT_ID.fetch_add(1, Ordering::Relaxed),
            _data: Default::default()
        }
    }

    pub fn with_capacity(init: M::Init, capacity: usize) -> StableRegionVec<T, M> {
        let mut regions = LinkedVec::new();
        let id = regions.push_back(AllocatedRegion {
            offset: 0,
            size: capacity,
            used: false
        });

        let mut free_regions: BTreeMap<usize, BTreeSet<Id>> = BTreeMap::new();
        free_regions.entry(capacity).or_default().insert(id);

        let (drop_return_sender, drop_return) = flume::unbounded();

        StableRegionVec {
            memory: M::with_capacity(init, capacity * std::mem::size_of::<T>()).unwrap(),
            regions,
            free_regions,
            allocation_mode: AllocationMode::Minimal,
            drop_return,
            drop_return_sender,
            id: NEXT_ID.fetch_add(1, Ordering::Relaxed),
            _data: Default::default()
        }
    }

    pub fn with_memory(memory: M) -> StableRegionVec<T, M> {
        let mut regions = LinkedVec::new();
        let mut free_regions: BTreeMap<usize, BTreeSet<Id>> = BTreeMap::new();

        let size = memory.capacity() / std::mem::size_of::<T>();

        if size > 0 {
            let id = regions.push_back(AllocatedRegion {
                offset: 0,
                size,
                used: false
            });

            free_regions.entry(size).or_default().insert(id);
        }

        let (drop_return_sender, drop_return) = flume::unbounded();

        StableRegionVec {
            memory,
            regions,
            free_regions,
            allocation_mode: AllocationMode::Minimal,
            drop_return,
            drop_return_sender,
            id: NEXT_ID.fetch_add(1, Ordering::Relaxed),
            _data: Default::default()
        }
    }
    
    pub fn set_allocation_mode(&mut self, allocation_mode: AllocationMode) {
        self.allocation_mode = allocation_mode;
    }

    pub fn promote_region(&self, region: Region) -> ManagedRegion {
        assert_eq!(region.dynamic_vec_id, self.id);

        ManagedRegion {
            region,
            drop_return_sender: self.drop_return_sender.clone()
        }
    }
    
    /// Copies the data into the DynamicVec, allocating more memory if needed, returning the Region in the vec it was stored
    unsafe fn insert_slice_internal(&mut self, size: usize, data: *const u8) -> Region {
        // let size = data.len();
        let (offset, id) = if let Some((available_size, locations)) = self.free_regions.range_mut(size..).next() {
            // There exists an available empty slot that can fit the data, get it's id
            let free_id = *locations.iter().next().unwrap();
            locations.remove(&free_id);

            // Calculate the remaining size of the slot
            let available_size = *available_size;
            let rem = available_size - size;

            if locations.is_empty() {
                // If that was the last slot of this size, remove the entry from the map
                self.free_regions.remove(&available_size);
            }

            // Set the size of the region and mark it as used
            self.regions[free_id].size = size;
            self.regions[free_id].used = true;

            if rem > 0 {
                // If there is some remaining space, add a free region after the now used one
                let rem_id = self.regions.insert(free_id, AllocatedRegion {
                    offset: self.regions[free_id].offset + size,
                    size: rem,
                    used: false
                });
                self.free_regions.entry(rem).or_default().insert(rem_id);
            }

            (self.regions[free_id].offset, free_id)
        } else {
            // No available free region, so need to increase allocation
            let (offset, id) = if let Some((back_id, back)) = self.regions.peek_mut_back() {
                // There is a back region
                if back.used {
                    // The back most region is used, so no existing memory to partially use
                    let offset = self.memory.capacity() / std::mem::size_of::<T>();
                    let new_size = self.allocation_mode.adjust_size(self.memory.capacity(), self.memory.capacity() + size * std::mem::size_of::<T>());
                    self.memory.resize(new_size).unwrap();

                    // Add the empty region
                    let free_id = self.regions.push_back(AllocatedRegion {
                        offset,
                        size,
                        used: true
                    });

                    (offset, free_id)
                } else {
                    // The back most region is unused, so only need to extend by difference
                    let new_size = self.allocation_mode.adjust_size(self.memory.capacity(), self.memory.capacity() + (size - back.size) * std::mem::size_of::<T>());
                    self.memory.resize(new_size).unwrap();
                    {
                        // Remove the free region from free_regions
                        let free = self.free_regions.get_mut(&back.size).unwrap();
                        free.remove(&back_id);
                        if free.is_empty() {
                            self.free_regions.remove(&back.size);
                        }
                    }

                    // Update it's size
                    back.size = size;

                    (back.offset, back_id)
                }
            } else {
                // No existing regions, so just add one
                let new_size = self.allocation_mode.adjust_size(self.memory.capacity(), size * std::mem::size_of::<T>());
                self.memory.resize(new_size).unwrap();
                let new_id = self.regions.push_back(AllocatedRegion {
                    offset: 0,
                    size,
                    used: true
                });

                (0, new_id)
            };

            if (self.memory.capacity() / std::mem::size_of::<T>()) > (offset + size) {
                let extra_alloc = (self.memory.capacity() / std::mem::size_of::<T>()) - (offset + size);
                let extra_id = self.regions.push_back(AllocatedRegion {
                    offset: offset + size,
                    size: extra_alloc,
                    used: false
                });
                self.free_regions.entry(extra_alloc).or_default().insert(extra_id);
            }

            (offset, id)
        };

        let data_bytes = unsafe { std::slice::from_raw_parts(data, size * std::mem::size_of::<T>()) };
        self.memory.write(offset * std::mem::size_of::<T>(), size * std::mem::size_of::<T>(), |mem| {
            mem.copy_from_slice(data_bytes);
        });

        Region {
            offset,
            size,
            id,
            dynamic_vec_id: self.id,
        }
    }

    fn process_drop_return(&mut self) {
        while let Ok(region) = self.drop_return.try_recv() {
            self.forget_region(region)
        }
    }

    /// Copies the data into the DynamicVec, allocating more memory if needed, returning the Region in the vec it was stored
    pub fn insert_slice(&mut self, data: &[T]) -> Region where T: Copy {
        self.process_drop_return();
        unsafe { self.insert_slice_internal(data.len(), data.as_ptr() as *const u8) }
    }

    /// Moves the data into the DynamicVec, allocating more memory if needed, returning the Region in the vec it was stored
    pub fn insert_vec(&mut self, data: Vec<T>) -> Region {
        self.process_drop_return();
        unsafe { self.insert_slice_internal(data.len(), data.as_ptr() as *const u8) }
    }

    /// Moves the data into the DynamicVec, allocating more memory if needed, returning the Region in the vec it was stored
    pub fn insert_array<const N: usize>(&mut self, data: [T; N]) -> Region {
        self.process_drop_return();
        unsafe { self.insert_slice_internal(N, &data[0] as *const _ as *const u8) }
    }

    pub fn forget_region(&mut self, region: Region) {
        assert_eq!(region.dynamic_vec_id, self.id);

        // Mark no longer used
        self.regions[region.id].used = false;

        if let Some(prev_id) = self.regions[region.id].previous() {
            let prev = &self.regions[prev_id];
            if !prev.used {
                // If there is a region before this one, and it's not used, remove it and make this encompass it
                self.free_regions.get_mut(&prev.size).unwrap().remove(&prev_id);
                let prev = self.regions.remove(prev_id);
                if self.free_regions[&prev.size].is_empty() {
                    self.free_regions.remove(&prev.size);
                }

                self.regions[region.id].offset = prev.offset;
                self.regions[region.id].size += prev.size;
            }
        }

        if let Some(next_id) = self.regions[region.id].next() {
            let next = &self.regions[next_id];
            if !next.used {
                // If there is a region after this one, and it's not used, remove it and make this encompass it
                self.free_regions.get_mut(&next.size).unwrap().remove(&next_id);
                let next = self.regions.remove(next_id);
                if self.free_regions[&next.size].is_empty() {
                    self.free_regions.remove(&next.size);
                }

                self.regions[region.id].size += next.size;
            }
        }

        self.free_regions.entry(self.regions[region.id].size).or_default().insert(region.id);
    }

    pub fn memory(&self) -> &M {
        &self.memory
    }
}