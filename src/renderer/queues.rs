use std::collections::{HashMap, HashSet};
use std::ops::AddAssign;
use std::sync::Arc;
use arrayvec::ArrayVec;
use fxhash::{FxBuildHasher, FxHashMap, FxHashSet};
use parking_lot::RwLock;
use positronium_vk as vk;
use smallvec::SmallVec;
use super::device::Device;
use super::surface::Surface;

#[derive(Clone)]
pub struct QueueFamilyIndices {
    pub graphics_family: u32,
    pub presentation_family: u32,
    pub unique_queues: ArrayVec<u32, 2>,
}

impl QueueFamilyIndices {
    pub fn init(physical_device: &vk::PhysicalDevice, surface: &Surface) -> Option<Self> {
        let mut graphics_family = None;
        let mut presentation_family = None;

        for (i, q) in physical_device.get_physical_device_queue_family_properties::<SmallVec<[_; 32]>>().iter().enumerate() {
            if q.queue_flags & vk::QueueFlagBits::eGraphicsBit != vk::QueueFlags::eNone && graphics_family.is_none() {
                graphics_family.replace(i as u32);
            }
            if physical_device.get_physical_device_surface_support_khr(i as u32, &surface.surface).expect("Unable to query surface support").0.is_true() && presentation_family.is_none() {
                presentation_family.replace(i as u32);
            }
        }

        let graphics_family = graphics_family?;
        let presentation_family = presentation_family?;

        let unique_queues = [graphics_family, presentation_family]
            .into_iter().collect::<FxHashSet<_>>()
            .into_iter().collect();

        Some(QueueFamilyIndices {
            graphics_family,
            presentation_family,
            unique_queues,
        })
    }
}

#[derive(Debug)]
pub struct QueueManager {
    pub graphics_family: u32,
    pub graphics_queue: Arc<RwLock<vk::Queue>>,
    pub presentation_family: u32,
    pub presentation_queue: Arc<RwLock<vk::Queue>>,
}

impl QueueManager {
    pub fn new(device: &Device) -> Self {
        let mut existing: HashMap<u32, Arc<RwLock<vk::Queue>>, _> = FxHashMap::default();

        let graphics_queue_family = device.capabilities.queue_families.graphics_family;
        let graphics_queue = existing.entry(graphics_queue_family).or_insert_with(||
            Arc::new(RwLock::new(device.device.get_device_queue(graphics_queue_family, 0)))
        ).clone();

        let presentation_queue_family = device.capabilities.queue_families.presentation_family;
        let presentation_queue = existing.entry(presentation_queue_family).or_insert_with(||
            Arc::new(RwLock::new(device.device.get_device_queue(presentation_queue_family, 0)))
        ).clone();

        QueueManager {
            graphics_family: graphics_queue_family,
            graphics_queue,
            presentation_family: presentation_queue_family,
            presentation_queue,
        }
    }
}

