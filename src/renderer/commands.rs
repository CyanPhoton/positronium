use positronium_vk as vk;
use smallvec::SmallVec;
use crate::renderer::device::Device;
use crate::renderer::queues::QueueManager;

pub struct CommandBuffer {
    pub command: vk::CommandBuffer,
}

pub struct CommandPool {
    pub command_pool: vk::CommandPool,
}

pub struct CommandManager {
    pub graphics_pool: CommandPool,
}

impl CommandManager {
    pub fn new(device: &Device, queue_manager: &QueueManager) -> CommandManager {
        CommandManager {
            graphics_pool: CommandPool::new(device, queue_manager.graphics_family)
        }
    }
}

impl CommandPool {
    pub fn new(device: &Device, queue_family_index: u32) -> CommandPool {
        let graphics_pool_create_info = vk::CommandPoolCreateInfo {
            flags: Default::default(),
            queue_family_index,
        }.into_raw();

        let command_pool = device.device.create_command_pool(&graphics_pool_create_info)
            .expect("Failed to create command pool").0;

        CommandPool {
            command_pool
        }
    }

    pub fn allocate_command_buffers(&mut self, command_buffer_count: u32) -> Vec<CommandBuffer> {
        let mut allocate_info = vk::CommandBufferAllocateInfo {
            level: vk::CommandBufferLevel::ePrimary,
            command_buffer_count,
        }.into_raw();

        self.command_pool.allocate_command_buffers::<Vec<_>>(&mut allocate_info)
            .expect("Failed to allocate command buffers").0
            .into_iter().map(|command| CommandBuffer { command })
            .collect()
    }

    pub fn allocate_command_buffer(&mut self) -> CommandBuffer {
        let mut allocate_info = vk::CommandBufferAllocateInfo {
            level: vk::CommandBufferLevel::ePrimary,
            command_buffer_count: 1,
        }.into_raw();

        CommandBuffer {
            command: self.command_pool.allocate_command_buffers::<SmallVec<[_; 1]>>(&mut allocate_info).expect("Failed to allocate command buffers").0.into_iter().next().unwrap()
        }
    }
}

impl CommandBuffer {
    pub fn record<F: FnOnce(vk::RecordingCommandBuffer) -> Result<(), vk::ErrorCode>>(&mut self, command_pool: &mut CommandPool, record: F) {
        let begin_info = vk::CommandBufferBeginInfo {
            flags: Default::default(),
            inheritance_info: None,
        }.into_raw();

        self.command.record_with(&mut command_pool.command_pool, &begin_info, record)
            .expect("Failed to record command buffer");
    }
}

