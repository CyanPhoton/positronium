use positronium_vk as vk;
use super::Device;
use super::Swapchain;

pub struct RenderPass {
    pub render_pass: vk::RenderPass,
}

impl RenderPass {
    pub fn new(device: &Device, swapchain: &Swapchain) -> RenderPass {
        let colour_attachments = [vk::AttachmentDescription {
            flags: Default::default(),
            format: swapchain.image_format,
            samples: vk::SampleCountFlagBits::e1Bit,
            load_op: vk::AttachmentLoadOp::eClear,
            store_op: vk::AttachmentStoreOp::eStore,
            stencil_load_op: vk::AttachmentLoadOp::eDontCare,
            stencil_store_op: vk::AttachmentStoreOp::eDontCare,
            initial_layout: vk::ImageLayout::eUndefined,
            final_layout: vk::ImageLayout::ePresentSrcKhr,
        }.into_raw()];

        let colour_refs = [vk::AttachmentReference {
            attachment: 0,
            layout: vk::ImageLayout::eColourAttachmentOptimal,
        }.into_raw()];

        let subpasses = [vk::SubpassDescription {
            flags: Default::default(),
            pipeline_bind_point: vk::PipelineBindPoint::eGraphics,
            input_attachments: &[],
            colour_attachments: &colour_refs[..],
            resolve_attachments: &[],
            depth_stencil_attachment: None,
            preserve_attachments: &[],
        }.into_raw()];

        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL,
            dst_subpass: 0,
            // Wait to perform vk::ImageLayout::eUndefined -> vk::ImageLayout::eColourAttachmentOptimal
            // until the eColourAttachmentOutputBit stage, since this is when the queue submit waits on the swapchain image being ready
            src_stage_mask: vk::PipelineStageFlags::eColourAttachmentOutputBit,
            src_access_mask: vk::AccessFlags::eNoneKhr,
            // Wait so that this transition is finished before we try to a colour to it
            dst_stage_mask: vk::PipelineStageFlags::eColourAttachmentOutputBit,
            dst_access_mask: vk::AccessFlags::eColourAttachmentWriteBit,
            dependency_flags: Default::default(),
        }.into_raw()];

        let render_pass_info = vk::RenderPassCreateInfo {
            flags: Default::default(),
            attachments: &colour_attachments[..],
            subpasses: &subpasses[..],
            dependencies: &dependencies[..],
        }.into_raw();

        let render_pass = device.device.create_render_pass(&render_pass_info)
            .expect("Failed to create render pass").0;

        RenderPass {
            render_pass
        }
    }
}