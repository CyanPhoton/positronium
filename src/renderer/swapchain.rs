use std::collections::VecDeque;
use std::ops::DerefMut;
use std::sync::{Arc, Weak};
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;
use defer_alloc::DeferAllocArc;
use parking_lot::{Mutex, RwLock, Condvar};
use positronium_vk as vk;
use smallvec::SmallVec;
use crate::renderer::SwapchainSettings;
use crate::winit::dpi::PhysicalSize;
use super::queues::QueueManager;
use super::device::Device;
use super::surface::Surface;
use crate::utility;

pub struct SwapchainSupportDetails {
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub formats: SmallVec<[vk::SurfaceFormatKHR; 8]>,
    pub present_modes: SmallVec<[vk::PresentModeKHR; 8]>,
}

pub struct ImageCounts {
    // Total number of images requested
    total_count: u32,
    // Number of images that can be rendering at the same time, >= 1
    max_in_flight: u32,
    // Number of images that can be held at once, >= max_in_flight
    queue_count: u32,
}

impl SwapchainSupportDetails {
    pub fn init(device: &Device, surface: &Surface) -> Result<Self, vk::ErrorCode> {
        let capabilities = device.physical_device.get_physical_device_surface_capabilities_khr(&surface.surface)?.0;
        let formats = device.physical_device.get_physical_device_surface_formats_khr::<SmallVec<[_; 8]>>(Some(&surface.surface))?.0;
        let present_modes = device.physical_device.get_physical_device_surface_present_modes_khr::<SmallVec<[_; 8]>>(Some(&surface.surface))?.0;

        // println!("Available formats: {:?}", formats);
        // println!("Available present_modes: {:?}", present_modes);

        Ok(SwapchainSupportDetails {
            capabilities,
            formats,
            present_modes,
        })
    }

    pub fn choose_format(&self) -> vk::SurfaceFormatKHR {
        self.formats.iter().copied().filter(|f| {
            f.format == vk::Format::eB8G8R8A8Srgb && f.colour_space == vk::ColourSpaceKHR::eSrgbNonlinear
        })
            .next().unwrap_or(*self.formats.first().unwrap())
    }

    pub fn choose_present_mode(&self) -> vk::PresentModeKHR {
        let mut mailbox_available = false;
        let mut immediate_available = false;

        for pm in &self.present_modes {
            match *pm {
                vk::PresentModeKHR::eImmediate => immediate_available = true,
                vk::PresentModeKHR::eMailbox => mailbox_available = true,
                _ => {}
            }
        }

        let pm = if mailbox_available {
            vk::PresentModeKHR::eMailbox
        } else if immediate_available {
            vk::PresentModeKHR::eImmediate
        } else {
            vk::PresentModeKHR::eFifo
        };

        // println!("Present mode chosen: {:?}", pm);
        pm
    }

    pub fn choose_swap_extent(&self, window_size: PhysicalSize<u32>) -> vk::Extent2D {
        if self.capabilities.current_extent.width != u32::MAX {
            self.capabilities.current_extent
        } else {
            let width = window_size.width.clamp(self.capabilities.min_image_extent.width, self.capabilities.max_image_extent.width);
            let height = window_size.height.clamp(self.capabilities.min_image_extent.height, self.capabilities.max_image_extent.height);

            vk::Extent2D {
                width,
                height,
            }
        }
    }

    pub fn choose_image_count(&self, present_mode: vk::PresentModeKHR, settings: &SwapchainSettings) -> ImageCounts {
        let max_frames_in_flight = settings.max_frames_in_flight.max(1);

        let extra = match present_mode {
            vk::PresentModeKHR::eMailbox => 1, //TODO: Is this extra one actually needed?
            _ => 0,
        }
            + max_frames_in_flight - 1 // -1 Since, if ask for 1 max_in_flight, #image = min, will work
            + settings.requested_additional_images;

        let total_count = if self.capabilities.max_image_count == 0 {
            self.capabilities.min_image_count + extra
        } else {
            (self.capabilities.min_image_count + extra).min(self.capabilities.max_image_count)
        };

        let queue_count = total_count - self.capabilities.min_image_count + 1;
        let max_in_flight = max_frames_in_flight.min(queue_count);

        let ic = ImageCounts {
            total_count,
            max_in_flight,
            queue_count,
        };

        // println!("Image count choose: {:?}, max_in_flight: {:?}, queue_count: {:?}, min count: {:?}, max count: {:?}", ic.total_count, ic.max_in_flight, ic.queue_count, self.capabilities.min_image_count, self.capabilities.max_image_count);
        ic
    }
}

pub struct Swapchain {
    acquire_present_thread: Option<JoinHandle<()>>,
    acquire_receiver: flume::Receiver<AcquiredResult>,
    present_requester: flume::Sender<AcquiredImageInfo>,
    acquire_present_quit_signal: flume::Sender<()>,
    //
    swapchain: Arc<RwLock<vk::SwapchainKHR>>,
    pub image_format: vk::Format,
    pub extent: vk::Extent2D,
    pub images: Vec<vk::Image>,
    pub image_views: Vec<vk::ImageView>,
    //
    retired: Arc<AtomicBool>,
    old_swapchain: Weak<RwLock<vk::SwapchainKHR>>,
    in_flight_handles: Arc<RwLock<VecDeque<InFlightHandle>>>
}

struct AcquirePresentThreadContext {
    acquire_sender: flume::Sender<AcquiredResult>,
    present_receiver: flume::Receiver<AcquiredImageInfo>,
    quit_signal: flume::Receiver<()>,
    //
    retired: Arc<AtomicBool>,
    device: Arc<Device>,
    swapchain: Arc<RwLock<vk::SwapchainKHR>>,
    presentation_queue: Arc<RwLock<vk::Queue>>,
    available_per_in_flight: VecDeque<PerInflightFrameData>,
    currently_in_flight: VecDeque<PerInflightFrameData>,
    in_flight_handles: Arc<RwLock<VecDeque<InFlightHandle>>>,
}

pub struct OldSwapchainRef {
    old_swapchain: Weak<RwLock<vk::SwapchainKHR>>,
}

#[derive(Debug)]
pub struct PerInflightFrameData {
    pub image_ready: vk::Semaphore,
    pub render_finished_semaphore: vk::Semaphore,
    pub render_finished_fence: vk::Fence,
}

#[derive(Clone)]
pub struct InFlightHandle {
    finished: DeferAllocArc<(Mutex<bool>, Condvar)>,
}

pub struct AcquiredImageInfo {
    pub image_index: u32,
    pub image_ready: vk::Semaphore,
    pub render_finished_semaphore: vk::Semaphore,
    pub render_finished_fence: vk::Fence,
    _private: [u8; 0],
}

pub enum AcquiredResult {
    Success {
        acquired_image: AcquiredImageInfo,
        request_update: bool,
    },
    RequireUpdate,
    Timeout,
}

pub enum PresentResult {
    Success {
        request_update: bool
    },
    RequireUpdate,
}

impl Swapchain {
    pub fn new(device: Arc<Device>, presentation_queue: Arc<RwLock<vk::Queue>>, surface: &mut Surface, window_size: PhysicalSize<u32>, mut old_swapchain: Option<OldSwapchainRef>, settings: &SwapchainSettings) -> Swapchain {
        let old_swapchain_weak = old_swapchain.as_ref().map_or(Weak::new(), |old_swapchain| old_swapchain.old_swapchain.clone());

        let swapchain_support = SwapchainSupportDetails::init(device.as_ref(), surface)
            .expect("Failed to get swap chain support details");

        let surface_format = swapchain_support.choose_format();
        let present_mode = swapchain_support.choose_present_mode();
        let extent = swapchain_support.choose_swap_extent(window_size);
        let image_count = swapchain_support.choose_image_count(present_mode, settings);

        let queues = &device.capabilities.queue_families;
        let queue_families = [queues.presentation_family, queues.graphics_family];
        let (image_sharing_mode, queue_family_indices) = if queues.presentation_family == queues.graphics_family {
            (vk::SharingMode::eExclusive, &[][..])
        } else {
            (vk::SharingMode::eConcurrent, &queue_families[..])
        };

        let owned_old_swapchain = old_swapchain_weak.upgrade();
        let mut owned_old_swapchain_write_lock = owned_old_swapchain.as_ref().map(|o| o.write());
        let old_swapchain_handle = owned_old_swapchain_write_lock.as_mut().map_or(vk::Handle::null(), |old_swapchain| old_swapchain.as_mut_handle());

        let mut create_info = vk::SwapchainCreateInfoKHR {
            flags: Default::default(),
            min_image_count: image_count.total_count,
            image_format: surface_format.format,
            image_colour_space: surface_format.colour_space,
            image_extent: extent,
            image_array_layers: 1,
            image_usage: vk::ImageUsageFlags::eColourAttachmentBit,
            image_sharing_mode,
            queue_family_indices,
            pre_transform: swapchain_support.capabilities.current_transform,
            composite_alpha: vk::CompositeAlphaFlagBitsKHR::eOpaqueBit,
            present_mode,
            clipped: true,
            old_swapchain: old_swapchain_handle,
        }.into_raw();

        let swapchain = device.device.create_swapchain_khr(&mut surface.surface, &mut create_info).expect("Failed to create swapchain").0;

        drop(owned_old_swapchain_write_lock);
        drop(owned_old_swapchain); // Drop Arc reference, just keeping Weak so that it can be freed whenever

        let images = swapchain.get_swapchain_images_khr::<Vec<_>>(&device.device).expect("Failed to get swapchain images").0;

        let image_views = images.iter().map(|image| {
            let create_info = vk::ImageViewCreateInfo {
                flags: Default::default(),
                image: image.as_handle(),
                view_type: vk::ImageViewType::e2d,
                format: surface_format.format,
                components: vk::ComponentMapping {
                    r: vk::ComponentSwizzle::eIdentity,
                    g: vk::ComponentSwizzle::eIdentity,
                    b: vk::ComponentSwizzle::eIdentity,
                    a: vk::ComponentSwizzle::eIdentity,
                },
                subresource_range: vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::eColourBit,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                },
            }.into_raw();

            device.device.create_image_view(&create_info).expect("Failed to create image view").0
        }).collect();

        let semaphore_create_info = vk::SemaphoreCreateInfo {
            flags: Default::default(),
        }.into_raw();

        let fence_create_info = vk::FenceCreateInfo {
            flags: Default::default(),
        }.into_raw();

        let available_per_in_flight = (0..image_count.max_in_flight).map(|_| {
            PerInflightFrameData {
                image_ready: device.device.create_semaphore(&semaphore_create_info).expect("Failed to create image_ready semaphore").0,
                render_finished_semaphore: device.device.create_semaphore(&semaphore_create_info).expect("Failed to create render_finished semaphore").0,
                render_finished_fence: device.device.create_fence(&fence_create_info).expect("Failed to create render_finished fence").0,
            }
        }).collect();

        let swapchain = Arc::new(RwLock::new(swapchain));

        let (acquire_sender, acquire_receiver) = flume::bounded(0);
        let (present_requester, present_receiver) = flume::bounded(0);
        let (acquire_present_quit_signal, quit_signal) = flume::bounded(2);

        let currently_in_flight = VecDeque::with_capacity(image_count.max_in_flight as usize);
        let in_flight_handles = Arc::new(RwLock::new(VecDeque::with_capacity(image_count.max_in_flight as usize)));

        let retired = Arc::new(AtomicBool::new(false));

        let acquire_present_thread = AcquirePresentThreadContext {
            acquire_sender,
            present_receiver,
            quit_signal,
            retired: retired.clone(),
            device,
            swapchain: swapchain.clone(),
            available_per_in_flight,
            currently_in_flight,
            presentation_queue,
            in_flight_handles: in_flight_handles.clone(),
        }.create_thread();

        drop(old_swapchain);

        Swapchain {
            acquire_present_thread: Some(acquire_present_thread),
            acquire_receiver,
            present_requester,
            acquire_present_quit_signal,
            swapchain,
            image_format: surface_format.format,
            extent,
            images,
            image_views,
            old_swapchain: old_swapchain_weak,
            retired,
            in_flight_handles,
        }
    }

    pub fn acquire_image(&mut self, inflight_frame_timeout: Option<Duration>) -> AcquiredResult {
        if self.retired.load(Ordering::SeqCst) {
            return AcquiredResult::RequireUpdate;
        }

        let acquired = if let Some(timeout) = inflight_frame_timeout {
            match self.acquire_receiver.recv_timeout(timeout) {
                Ok(acquired) => {
                    acquired
                }
                Err(flume::RecvTimeoutError::Timeout) => AcquiredResult::Timeout,
                e => {
                    e.unwrap();
                    unreachable!()
                }
            }
        } else {
            self.acquire_receiver.recv().unwrap()
        };

        if let AcquiredResult::RequireUpdate = acquired {
            self.retired.store(true, Ordering::SeqCst);
        }

        acquired
    }

    pub fn present_image(&mut self, acquired: AcquiredImageInfo) {
        self.in_flight_handles.write().push_back(InFlightHandle { finished: DeferAllocArc::new((Mutex::new(false), Condvar::new())) });
        self.present_requester.send(acquired).unwrap()
    }

    pub fn get_inflight_handles(&self) -> Vec<InFlightHandle> {
        self.in_flight_handles.read().iter().filter(|f| !f.is_finished()).cloned().collect()
    }

    pub fn into_old(self) -> (OldSwapchainRef, Swapchain) {
        self.retired.store(true, Ordering::SeqCst);
        (
            OldSwapchainRef {
                old_swapchain: Arc::downgrade(&self.swapchain),
            },
            self
        )
    }

    pub fn quit(&self) {
        // Double send single two quit stages
        self.acquire_present_quit_signal.send(()).ok();
        self.acquire_present_quit_signal.send(()).ok();
    }

    pub fn wait_finished(&mut self) {
        self.quit();
        self.acquire_present_thread.take().map(|t| t.join()); // This should trigger other thread to start stopping
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        self.quit();
        self.acquire_present_thread.take().map(|t| t.join()); // This should trigger other thread to start stopping
    }
}

impl AcquirePresentThreadContext {
    pub fn create_thread(mut self) -> JoinHandle<()> {
        thread::Builder::new().name("Acquire Present Thread".to_string()).spawn(move || {
            let mut running = true;

            while running {
                // Try acquire an image
                let per_in_flight_data = self.get_per_in_flight_data();
                let acquired = self.acquire_image(per_in_flight_data);

                if let AcquiredResult::RequireUpdate = &acquired {
                    running = false;
                }

                // Try to provide the acquired image
                flume::select::Selector::new()
                    .recv(&self.quit_signal, |_| running = false)
                    .send(&self.acquire_sender, acquired, |r| r.unwrap())
                    .wait();

                if !running {
                    break;
                }

                // Wait for a present request
                let mut to_present = None;

                flume::select::Selector::new()
                    .recv(&self.quit_signal, |_| running = false)
                    .recv(&self.present_receiver, |p| to_present = Some(p.unwrap()))
                    .wait();

                if let Some(to_present) = to_present {
                    // Try to present
                    if !self.present_image(to_present) {
                        running = false;

                        flume::select::Selector::new()
                            .recv(&self.quit_signal, |_| {})
                            .send(&self.acquire_sender, AcquiredResult::RequireUpdate, |r| r.unwrap())
                            .wait();
                    }
                }
            }

            // Wait for the signal to start trying to signal,
            // sent when the OldData thread receives the Swapchain
            // Needed so that we know al possible OldSwapchain have been created
            self.quit_signal.recv().unwrap();

            // Wait for frames to finish
            while !self.currently_in_flight.is_empty() {
                self.get_per_in_flight_data();
            }

            for f in self.in_flight_handles.write().drain(..) {
                if !f.finished.is_stack() {
                    f.finished.with(|(finished, sig)| {
                        *finished.lock() = true;
                        sig.notify_all();
                    })
                }
            }

            // std::thread::sleep(Duration::from_millis(100));
        }).unwrap()
    }

    fn get_per_in_flight_data(&mut self) -> PerInflightFrameData {
        while self.available_per_in_flight.is_empty() || self.currently_in_flight.front().map_or(false, |f| f.render_finished_fence.get_fence_status() == Ok(vk::SuccessCode::eSuccess)) {
            let mut in_flight = self.currently_in_flight.pop_front().unwrap();

            self.device.device.wait_for_fences(std::slice::from_ref(&in_flight.render_finished_fence.as_handle()), vk::Bool32::False, u64::MAX).expect("Failed to wait for fences");

            let handle = self.in_flight_handles.write().pop_front().unwrap();
            if !handle.finished.is_stack() {
                handle.finished.with(|(finished, sig)| {
                    *finished.lock() = true;
                    sig.notify_all();
                });
            }

            self.device.device.reset_fences(std::slice::from_mut(&mut in_flight.render_finished_fence.as_mut_handle())).expect("Failed to reset fence");

            self.available_per_in_flight.push_back(PerInflightFrameData {
                image_ready: in_flight.image_ready,
                render_finished_semaphore: in_flight.render_finished_semaphore,
                render_finished_fence: in_flight.render_finished_fence,
            });
        }

        self.available_per_in_flight.pop_front().unwrap()
    }

    fn acquire_image(&self, mut per_in_flight_data: PerInflightFrameData) -> AcquiredResult {
        let mut request_update = false;

        if self.retired.load(Ordering::SeqCst) {
            return AcquiredResult::RequireUpdate;
        }

        let image_index = match self.swapchain.write().acquire_next_image_khr(u64::MAX, Some(&mut per_in_flight_data.image_ready), None) {
            Ok((image_index, r)) => {
                if vk::SuccessCode::eSuboptimalKhr == r {
                    request_update = true;
                }
                image_index
            }
            Err(vk::ErrorCode::eErrorOutOfDateKhr) => return AcquiredResult::RequireUpdate,
            err => {
                err.expect("Failed to acquire next image");
                unreachable!()
            }
        };

        AcquiredResult::Success {
            acquired_image: AcquiredImageInfo {
                image_index,
                image_ready: per_in_flight_data.image_ready,
                render_finished_semaphore: per_in_flight_data.render_finished_semaphore,
                render_finished_fence: per_in_flight_data.render_finished_fence,
                _private: [],
            },
            request_update,
        }
    }

    fn present_image(&mut self, mut acquired: AcquiredImageInfo) -> bool {
        let mut wait_semaphores = [acquired.render_finished_semaphore.as_mut_handle()];

        let mut swapchain_write_lock = self.swapchain.write();
        let mut swapchains = [swapchain_write_lock.as_mut_handle()];

        let image_indices = [acquired.image_index];

        let mut present_info = vk::PresentInfoKHR {
            wait_semaphores: &mut wait_semaphores[..],
            swapchains: &mut swapchains[..],
            image_indices: &image_indices[..],
            results: &mut [],
        }.into_raw();

        let mut success = true;

        let r = self.presentation_queue.write().queue_present_khr(&mut present_info);
        match r {
            Ok(_) => vk::flush_window_compositor(),
            Err(vk::ErrorCode::eErrorOutOfDateKhr) => success = false, // TODO: Start creating new swapchain earlier
            e => {
                e.expect("Failed to present image");
                unreachable!()
            }
        }

        self.currently_in_flight.push_back(PerInflightFrameData {
            image_ready: acquired.image_ready,
            render_finished_semaphore: acquired.render_finished_semaphore,
            render_finished_fence: acquired.render_finished_fence,
        });

        success
    }
}

impl InFlightHandle {
    pub fn is_finished(&self) -> bool {
        self.finished.with(|(finished, sig)| {
            *finished.lock()
        })
    }

    pub fn wait_finished(&self) {
        self.finished.with(|(finished, sig)| {
            let mut finished = finished.lock();

            while !*finished {
                sig.wait(&mut finished);
            }
        })
    }
}

