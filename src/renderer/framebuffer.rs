use positronium_vk as vk;
use super::{Device, RenderPass};

pub struct Framebuffer {
    pub framebuffer: vk::Framebuffer,
}

impl Framebuffer {
    pub fn new(device: &Device, render_pass: &RenderPass, image_views: &[vk::ImageViewHandle], extent: vk::Extent2D) -> Framebuffer {
        let framebuffer_create_info = vk::FramebufferCreateInfo {
            flags: Default::default(),
            render_pass: render_pass.render_pass.as_handle(),
            attachments: image_views,
            width: extent.width,
            height: extent.height,
            layers: 1,
        }.into_raw();

        let framebuffer = device.device.create_framebuffer(&framebuffer_create_info)
            .expect("Failed to create framebuffer").0;

        Framebuffer {
            framebuffer
        }
    }
}