use std::any::TypeId;
use std::marker::PhantomData;
use positronium_vk as vk;
use positronium_vk::{VertexInputAttributeDescription, VertexInputBindingDescription};
use smallvec::{SmallVec, smallvec};
use super::Device;

pub mod hello_triangle;
pub mod basic;

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
pub struct VertexID(TypeId);

pub trait Vertex: 'static + Sized + Send + Sync {
    fn id() -> VertexID {
        VertexID(TypeId::of::<Self>())
    }

    fn binding_description() -> Option<vk::VertexInputBindingDescription> {
        None
    }

    fn attribute_descriptions() -> SmallVec<[vk::VertexInputAttributeDescription; 4]> {
        smallvec![]
    }
}

impl Vertex for () {}

pub trait GenericVertex: Send + Sync {
    fn id(&self) -> VertexID;
    fn binding_description(&self) -> Option<vk::VertexInputBindingDescription>;
    fn attribute_descriptions(&self) -> SmallVec<[vk::VertexInputAttributeDescription; 4]>;
}

pub struct GenericVertexWrapper<V: Vertex>(PhantomData<V>);

impl<V: Vertex> Default for GenericVertexWrapper<V> {
    fn default() -> Self {
        GenericVertexWrapper(Default::default())
    }
}

impl<V: Vertex> GenericVertex for GenericVertexWrapper<V> {
    fn id(&self) -> VertexID {
        VertexID(TypeId::of::<V>())
    }

    fn binding_description(&self) -> Option<VertexInputBindingDescription> {
        V::binding_description()
    }

    fn attribute_descriptions(&self) -> SmallVec<[VertexInputAttributeDescription; 4]> {
        V::attribute_descriptions()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
pub struct ShaderID(TypeId);

pub trait GenericShader: Send + Sync {
    fn vertex_id(&self) -> VertexID;

    fn shader_stages(&self) -> SmallVec<[vk::PipelineShaderStageCreateInfo; 8]>;
}

pub trait Shader: GenericShader {
    type Vertex: Vertex;

    fn id() -> ShaderID where Self: 'static {
        ShaderID(TypeId::of::<Self>())
    }

    fn new(device: &Device) -> Self;
}

fn create_shader(device: &Device, code: &[u32]) -> vk::ShaderModule {
    let create_info = vk::ShaderModuleCreateInfo {
        flags: Default::default(),
        code,
    }.into_raw();

    device.device.create_shader_module(&create_info)
        .expect("Failed to create shader module").0
}