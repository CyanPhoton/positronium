use positronium_vk as vk;
use crate::window_manager::WindowRef;
use super::instance::Instance;

pub struct Surface {
    pub surface: vk::SurfaceKHR,
}

impl Surface {
    pub fn new(instance: &Instance, window: &WindowRef) -> Surface {
        let surface = instance.instance.create_surface(window)
            .expect("Unsupported platform")
            .expect("Failed to create surface").0;

        Surface {
            surface
        }
    }
}