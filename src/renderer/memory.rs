use std::fmt::{Debug, Display};
use std::io::Write;
use std::sync::Arc;
use positronium_vk as vk;
use crate::renderer::commands::CommandPool;
use super::Device;

pub mod allocators;
pub mod collections;

pub struct Error<R> {
    pub ty: ErrorType,
    /// If a function consumes a type, but fails to place it, it will return it here
    pub data: Option<R>
}

impl Error<()> {
    pub fn put_data<R>(self, data: R) -> Error<R> {
        Error {
            ty: self.ty,
            data: Some(data)
        }
    }
}

pub enum ErrorType {
    AllocatorOutOfMemory {
        tried: usize
    },
    CollectionMaxCapacityHit {
        tried: usize,
        max: usize
    }
}

impl ErrorType {
    fn to_err(self) -> Error<()> {
        Error { ty: self, data: None }
    }
}

impl<R> Debug for Error<R> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.ty {
            ErrorType::AllocatorOutOfMemory { tried } => write!(f, "The allocator has run out of memory, tried to allocate {} bytes", tried),
            ErrorType::CollectionMaxCapacityHit { tried, max } => write!(f, "The collections configured memory limit has been reached, tired to {}/{} bytes", tried, max),
        }
    }
}

impl<R> Display for Error<R> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl<R> std::error::Error for Error<R> {}

pub type Result<T> = std::result::Result<T, Error<()>>;
pub type ResultR<T, R> = std::result::Result<T, Error<R>>;

pub trait Memory: Sized {
    type MemoryRef;
    type Init;

    fn new(init: Self::Init) -> Self;
    fn with_capacity(init: Self::Init, bytes: usize) -> Result<Self>;
    fn capacity(&self) -> usize;
    fn resize(&mut self, bytes: usize) -> Result<()>;

    fn get_ref(&self) -> Self::MemoryRef;
}

pub trait ReadableMemory: Memory {
    fn read<F: FnOnce(&[u8])>(&self, offset: usize, len: usize, f: F);

    fn read_into_slice(&self, offset: usize, dst: &mut [u8]) {
        self.read(offset, dst.len(), |src| dst.copy_from_slice(src));
    }
}

pub trait CopyableMemory: Memory {
    fn copy_non_overlapping(&mut self, src_offset: usize, dst_offset: usize, len: usize);
}

pub trait WriteableMemory: Memory {
    fn write<F: FnOnce(&mut [u8])>(&mut self, offset: usize, len: usize, f: F);

    fn write_from_slice(&mut self, offset: usize, src: &[u8]) {
        self.write(offset, src.len(), |dst| dst.copy_from_slice(src))
    }
}

pub trait FlushableMemory: Memory {
    fn flush(&mut self, command_pool: &mut CommandPool, queue: &mut vk::Queue);
}