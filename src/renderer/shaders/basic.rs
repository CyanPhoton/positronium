use std::any::TypeId;
use positronium_maths::{Vector2f, Vector3f};
use positronium_vk as vk;
use positronium_vk::{VertexInputAttributeDescription, VertexInputBindingDescription};
use smallvec::{SmallVec, smallvec};
use crate::renderer::shaders::Vertex as _;
use crate::renderer::shaders::VertexID;
use super::Device;

pub struct BasicShader {
    vertex_shader: vk::ShaderModule,
    fragment_shader: vk::ShaderModule,
}

#[derive(Copy, Clone)]
#[positronium_maths::field_align(offset = offset)]
pub struct Vertex {
    pub pos: Vector2f,
    pub colour: Vector3f,
}

impl BasicShader {
    pub fn new(device: &Device) -> BasicShader {
        let vertex_shader = super::create_shader(device, &include_shader!("basic", "vert.out"));
        let fragment_shader = super::create_shader(device, &include_shader!("basic", "frag.out"));

        BasicShader {
            vertex_shader,
            fragment_shader,
        }
    }
}

impl super::Vertex for Vertex {
    fn binding_description() -> Option<vk::VertexInputBindingDescription> {
        Some(vk::VertexInputBindingDescription {
            binding: 0,
            stride: std::mem::size_of::<Self>() as u32,
            input_rate: vk::VertexInputRate::eVertex,
        })
    }

    fn attribute_descriptions() -> SmallVec<[vk::VertexInputAttributeDescription; 4]> {
        smallvec![
            vk::VertexInputAttributeDescription {
                location: 0,
                binding: 0,
                format: vk::Format::eR32G32Sfloat,
                offset: Self::pos_offset() as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 1,
                binding: 0,
                format: vk::Format::eR32G32B32Sfloat,
                offset: Self::colour_offset() as u32,
            }
        ]
    }
}

impl super::GenericShader for BasicShader {
    fn vertex_id(&self) -> VertexID {
        VertexID(TypeId::of::<Vertex>())
    }

    fn shader_stages(&self) -> SmallVec<[vk::PipelineShaderStageCreateInfo; 8]> {
        smallvec![
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eVertexBit,
                module: self.vertex_shader.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None
            },
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eFragmentBit,
                module: self.fragment_shader.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None
            },
        ]
    }
}

impl super::Shader for BasicShader {
    type Vertex = Vertex;

    fn new(device: &Device) -> Self {
        Self::new(device)
    }
}