use std::any::TypeId;
use positronium_vk as vk;
use positronium_vk::{VertexInputAttributeDescription, VertexInputBindingDescription};
use smallvec::{SmallVec, smallvec};
use crate::renderer::shaders::{GenericShader, VertexID};
use crate::renderer::shaders::Vertex as _;
use super::Device;

use super::Shader;

pub struct HelloTriangleShader {
    vertex_shader: vk::ShaderModule,
    fragment_shader: vk::ShaderModule,
}

impl HelloTriangleShader {
    pub fn new(device: &Device) -> HelloTriangleShader {
        let vertex_shader = super::create_shader(device, &include_shader!("hello_triangle", "vert.out"));
        let fragment_shader = super::create_shader(device, &include_shader!("hello_triangle", "frag.out"));

        HelloTriangleShader {
            vertex_shader,
            fragment_shader,
        }
    }
}

impl GenericShader for HelloTriangleShader {
    fn vertex_id(&self) -> VertexID {
        VertexID(TypeId::of::<()>())
    }

    fn shader_stages(&self) -> SmallVec<[vk::PipelineShaderStageCreateInfo; 8]> {
        smallvec![
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eVertexBit,
                module: self.vertex_shader.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None
            },
            vk::PipelineShaderStageCreateInfo {
                flags: Default::default(),
                stage: vk::ShaderStageFlagBits::eFragmentBit,
                module: self.fragment_shader.as_handle(),
                name: vk::unsized_cstr!("main"),
                specialization_info: None
            },
        ]
    }
}

impl Shader for HelloTriangleShader {
    type Vertex = ();

    fn new(device: &Device) -> Self {
        Self::new(device)
    }
}