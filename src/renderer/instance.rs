use std::collections::HashSet;
use std::ffi::CString;
use positronium_vk as vk;
use positronium_vk::UnsizedCStr;
use smallvec::SmallVec;

use crate::renderer::RendererSettings;

pub struct Instance {
    pub loader: vk::Loader,
    pub instance: vk::Instance,
    pub debug_messenger: Option<vk::DebugUtilsMessengerEXT>,
    pub validation_layers: Vec<UnsizedCStr<'static>>,
}

impl Instance {
    pub fn new(application_name: &str, application_version: vk::Version, mut settings: RendererSettings) -> Instance {
        let loader = vk::Loader::new_auto()
            .expect("Failed to load vulkan library");

        let app_name = CString::new(application_name).expect("Invalid application name");
        let app_name = vk::UnsizedCStr::from(app_name.as_c_str());

        let app_info = vk::ApplicationInfo {
            application_name: Some(app_name),
            application_version,
            engine_name: Some(crate::ENGINE_NAME),
            engine_version: crate::ENGINE_VERSION,
            api_version: vk::API_VERSION_1_2,
        }.into_raw();

        let required_extensions = Self::list_required_instance_extensions(&loader, &mut settings);
        let validation_layers = Self::list_validation_layers(&loader, &settings);

        let mut create_info = vk::InstanceCreateInfoWithNext::<Vec<_>> {
            p_next: vec![],
            flags: Default::default(),
            application_info: Some(&app_info),
            enabled_layer_names: &validation_layers[..],
            enabled_extension_names: &required_extensions[..],
        };

        let mut debug_messenger = settings.use_validation_layers.then(Self::debug_messenger_create_info).map(|s| s.into_raw());
        if let Some(debug_messenger) = &mut debug_messenger {
            create_info.p_next.push(debug_messenger as &mut dyn vk::InstanceCreateInfoNext);
        }

        let instance = loader.create_instance(&create_info.into_raw())
            .expect("Failed to create instance").0;

        let debug_messenger = debug_messenger.as_ref().map(|create_info| {
            println!("Validation layer messenger created");
            instance.create_debug_utils_messenger_ext(create_info).expect("Failed to create debug utils messenger").0
        });

        Instance {
            loader,
            instance,
            debug_messenger,
            validation_layers,
        }
    }

    fn list_required_instance_extensions(loader: &vk::Loader, settings: &mut RendererSettings) -> Vec<vk::UnsizedCStr<'static>> {
        let mut extensions = Vec::new();

        let available_extensions = loader.enumerate_instance_extension_properties::<SmallVec<[_; 32]>>(None)
            .expect("Failed to enumerate instance extensions").0;
        let available_extensions: Vec<_> = available_extensions.iter().map(|e| e.extension_name.as_cstr().unwrap()).collect();

        if settings.use_validation_layers {
            if !available_extensions.contains(&vk::EXT_DEBUG_UTILS_EXTENSION_NAME_UNSIZED_CSTR.as_cstr()) {
                settings.use_validation_layers = false;
                eprintln!("WARNING: Validation layers where requested, but the required extension: {:?} is not available", vk::EXT_DEBUG_UTILS_EXTENSION_NAME);
            } else {
                extensions.push(vk::EXT_DEBUG_UTILS_EXTENSION_NAME_UNSIZED_CSTR);
            }
        }

        if settings.support_windowing {
            let options = vk::get_platform_surface_extension_options().expect("Unsupported platform");
            let mut combined_selected = HashSet::new();

            for option in options {
                let supported = option.iter().all(|r| available_extensions.contains(&r.as_cstr()));

                if supported {
                    combined_selected.extend(option);
                }
            }

            extensions.extend(combined_selected);
        }

        extensions
    }

    fn list_validation_layers(loader: &vk::Loader, settings: &RendererSettings) -> Vec<vk::UnsizedCStr<'static>> {
        if !settings.use_validation_layers {
            return Vec::new();
        }

        const VALIDATION_LAYERS: &'static [vk::UnsizedCStr<'static>] = &[vk::unsized_cstr!("VK_LAYER_KHRONOS_validation")];

        let available_layers = loader.enumerate_instance_layer_properties::<SmallVec<[_; 32]>>()
            .expect("Failed to enumerate available validation layers").0;
        let available_layers: Vec<_> = available_layers.iter().map(|l| l.layer_name.as_cstr().unwrap()).collect();

        VALIDATION_LAYERS.iter().copied().filter(|l| {
            let available = available_layers.contains(&l.as_cstr());
            if !available {
                eprintln!("WARNING: Requested validation layer: {:?} is not available", l.as_cstr());
            }
            available
        }).collect()
    }

    extern "C" fn debug_callback(message_severity: vk::raw_bit_fields::RawDebugUtilsMessageSeverityFlagBitsEXT, message_types: vk::DebugUtilsMessageTypeFlagsEXT, callback_data: Option<&vk::raw_structs::RawDebugUtilsMessengerCallbackDataEXT>, _user_data: Option<&'static ()>) -> vk::Bool32 {
        if let Some(callback_data) = callback_data {
            let message_severity = message_severity.normalise();

            if message_severity != vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit {
                let msg = callback_data.message.as_str().unwrap();
                if !msg.contains("wrong ELF class") && !msg.contains("Failed to open dynamic library") {
                    println!("Validation layer: {:?} - {:?}\n\tP{}", message_severity, message_types, msg);
                }
            }
        }

        vk::Bool32::False
    }

    fn debug_messenger_create_info() -> vk::DebugUtilsMessengerCreateInfoEXT {
        vk::DebugUtilsMessengerCreateInfoEXT {
            flags: Default::default(),
            message_severity: vk::DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit | vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit,
            message_type: vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidationBit | vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit,
            pfn_user_callback: Self::debug_callback,
            user_data: None,
        }
    }
}