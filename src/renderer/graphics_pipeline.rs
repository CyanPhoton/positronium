use positronium_vk as vk;
use smallvec::SmallVec;
use crate::renderer::shaders::{GenericShader, GenericVertex, Vertex};
use crate::renderer::swapchain::Swapchain;
use super::shaders::Shader;
use super::{Device, RenderPass};

pub struct GraphicsPipeline {
    pub layout: vk::PipelineLayout,
    pub pipeline: vk::Pipeline,
}

impl GraphicsPipeline {
    pub fn new(device: &Device, swapchain: &Swapchain, render_pass: &RenderPass, shader: &dyn GenericShader, vertex_info: &dyn GenericVertex) -> GraphicsPipeline {
        debug_assert_eq!(shader.vertex_id(), vertex_info.id());

        let shader_stages = shader.shader_stages().into_iter().map(|s| s.into_raw()).collect::<SmallVec<[_; 4]>>();

        let vertex_binding_description = vertex_info.binding_description().map(|s| s.into_raw());
        let vertex_binding_descriptions = vertex_binding_description.as_ref().map_or(&[][..], |b| std::slice::from_ref(b));

        let vertex_attribute_descriptions = vertex_info.attribute_descriptions().into_iter().map(|v| v.into_raw()).collect::<SmallVec<[_; 4]>>();

        let vertex_input_info = vk::PipelineVertexInputStateCreateInfo {
            flags: Default::default(),
            vertex_binding_descriptions,
            vertex_attribute_descriptions: vertex_attribute_descriptions.as_slice()
        }.into_raw();

        let input_assembly_info = vk::PipelineInputAssemblyStateCreateInfo {
            flags: Default::default(),
            topology: vk::PrimitiveTopology::eTriangleList,
            primitive_restart_enable: false
        }.into_raw();

        let viewports = [vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: swapchain.extent.width as f32,
            height: swapchain.extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0
        }.into_raw()];

        let scissors = [vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: swapchain.extent
        }.into_raw()];

        let viewport_state_info = vk::PipelineViewportStateCreateInfo {
            flags: Default::default(),
            viewports: &viewports[..],
            scissors: &scissors[..]
        }.into_raw();

        let rasterizer_info = vk::PipelineRasterizationStateCreateInfo {
            flags: Default::default(),
            depth_clamp_enable: false,
            rasterizer_discard_enable: false,
            polygon_mode: vk::PolygonMode::eFill,
            cull_mode: vk::CullModeFlags::eBackBit,
            front_face: vk::FrontFace::eCounterClockwise,
            depth_bias_enable: false,
            depth_bias_constant_factor: 0.0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
            line_width: 1.0
        }.into_raw();

        let multisample_info = vk::PipelineMultisampleStateCreateInfo {
            flags: Default::default(),
            rasterization_samples: vk::SampleCountFlagBits::e1Bit,
            sample_shading_enable: false,
            min_sample_shading: 1.0,
            sample_mask: &[],
            alpha_to_coverage_enable: false,
            alpha_to_one_enable: false
        }.into_raw();

        let colour_blend_attachments = [vk::PipelineColourBlendAttachmentState {
            blend_enable: false,
            src_colour_blend_factor: vk::BlendFactor::eZero,
            dst_colour_blend_factor: vk::BlendFactor::eZero,
            colour_blend_op: vk::BlendOp::eAdd,
            src_alpha_blend_factor: vk::BlendFactor::eZero,
            dst_alpha_blend_factor: vk::BlendFactor::eZero,
            alpha_blend_op: vk::BlendOp::eAdd,
            colour_write_mask: vk::ColourComponentFlags::eRBit | vk::ColourComponentFlags::eGBit | vk::ColourComponentFlags::eBBit | vk::ColourComponentFlags::eABit,
        }.into_raw()];

        let colour_blending = vk::PipelineColourBlendStateCreateInfo {
            flags: Default::default(),
            logic_op_enable: false,
            logic_op: vk::LogicOp::eClear,
            attachments: &colour_blend_attachments[..],
            blend_constants: [0.0, 0.0, 0.0, 0.0]
        }.into_raw();

        let layout_create_info = vk::PipelineLayoutCreateInfo {
            flags: Default::default(),
            set_layouts: &[],
            push_constant_ranges: &[]
        }.into_raw();

        let layout = device.device.create_pipeline_layout(&layout_create_info)
            .expect("Failed to create pipeline layout").0;

        let pipeline_create_infos = [vk::GraphicsPipelineCreateInfo {
            flags: Default::default(),
            stages: &shader_stages[..],
            vertex_input_state: Some(&vertex_input_info),
            input_assembly_state: Some(&input_assembly_info),
            tessellation_state: None,
            viewport_state: Some(&viewport_state_info),
            rasterization_state: Some(&rasterizer_info),
            multisample_state: Some(&multisample_info),
            depth_stencil_state: None,
            colour_blend_state: Some(&colour_blending),
            dynamic_state: None,
            layout: layout.as_handle(),
            render_pass: render_pass.render_pass.as_handle(),
            subpass: 0,
            base_pipeline_handle: vk::Handle::null(),
            base_pipeline_index: 0
        }.into_raw()];

        let pipeline = device.device.create_graphics_pipelines::<SmallVec<[_; 1]>>(None, &pipeline_create_infos[..])
            .expect("Failed to create graphics pipeline").0.into_iter().next().unwrap();

        GraphicsPipeline {
            layout,
            pipeline
        }
    }
}